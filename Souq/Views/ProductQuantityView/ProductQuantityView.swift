//
//  ProductQuantityView.swift
//  Souq
//
//  Created by Pawan Ramteke on 27/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import ASJCollectionViewFillLayout
class ProductQuantityView: UIView,UICollectionViewDataSource,UICollectionViewDelegate {
    
    var _collectionView: UICollectionView!
    
    var closeClosure : ((Int)->())?
    var quantityLimit = 10
    var selIndex : Int! {
        didSet {
            selIndex = Preferences.getAppLanguage() == ENGLISH ? selIndex : quantityLimit - 1 - selIndex
            _collectionView.reloadData()
            _collectionView.scrollToItem(at: IndexPath(item: selIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        let lblTitle = UILabel(frame: CGRect(x: 10, y: 0, width: self.frame.size.width - 20, height: 40))
        lblTitle.font = UIFont.appBoldFont(size: 18)
        lblTitle.text = localize(string: "Quantity")
        self.addSubview(lblTitle)
        
        let btnClose = UIButton(frame: CGRect(x: Preferences.getAppLanguage() == ENGLISH ? self.frame.maxX - 40 : 10 , y: lblTitle.frame.midY - 15, width: 30, height: 30))
        btnClose.setImage(UIImage(named: "ic_cross"), for: .normal)
        btnClose.addTarget(self, action: #selector(close), for: .touchUpInside)
        self.addSubview(btnClose)
        
        let layout = ASJCollectionViewFillLayout()
        layout.direction = .horizontal
        layout.itemSpacing = 5
        layout.itemLength = 60

        _collectionView = UICollectionView(frame: CGRect(x: 0, y: lblTitle.frame.maxY+10, width: self.frame.size.width, height: 70), collectionViewLayout: layout)
        _collectionView.dataSource = self
        _collectionView.delegate = self
        _collectionView.backgroundColor = UIColor.clear
        _collectionView.register(QuantityCell.self, forCellWithReuseIdentifier: "Cell")
        self.addSubview(_collectionView)
        
        selIndex = 0
        
        _collectionView.reloadData()
        if Preferences.getAppLanguage() == ARABIC {
            _collectionView.scrollToItem(at: IndexPath(item: quantityLimit - 1, section: 0), at: .right, animated: false)
        }
        else {
            _collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: false)
        }
    }
    
    func show()
    {
        self.frame.origin.y = SCREEN_HEIGHT
        UIView.animate(withDuration: 0.4) {
            self.frame.origin.y = self.frame.origin.y + 10 - (self.frame.size.height * 2)
        }
    }
    
    func hide() {
        UIView.animate(withDuration: 0.4, animations: {
            self.frame.origin.y = SCREEN_HEIGHT
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    @objc func close()
    {
        if closeClosure != nil {
            closeClosure!(selIndex + 1)
        }
    }
    
    func onCloseClicked(closure:@escaping (Int)->())
    {
        closeClosure = closure
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    //    fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return quantityLimit
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! QuantityCell
        
        cell.layer.borderColor = indexPath.row == selIndex ? UIColor.appThemeColor.cgColor : UIColor.controllerBGColor.cgColor

        cell.lblQuantity.text = Preferences.getAppLanguage() == ENGLISH ? "\(indexPath.row + 1)" : "\(quantityLimit - indexPath.row)"
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selIndex = indexPath.row
        collectionView.reloadData()
        close()
    }
}


class QuantityCell: UICollectionViewCell {
    var lblQuantity : UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.borderWidth = 1.0
        
        lblQuantity = UILabel()
        lblQuantity.font = UIFont.appBoldFont(size: 18)
        self.addSubview(lblQuantity)
        lblQuantity.enableAutoLayout()
        lblQuantity.centerX()
        lblQuantity.centerY()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
}
