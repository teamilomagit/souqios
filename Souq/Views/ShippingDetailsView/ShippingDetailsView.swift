//
//  ShippingDetailsView.swift
//  Souq
//
//  Created by Pawan Ramteke on 25/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class ShippingDetailsView: UIView,UITableViewDataSource {
    
    var arrData = NSArray()
    init(frame: CGRect,shippingData:NSArray) {
        super.init(frame: frame)
        
        let btnBG = UIButton(frame: frame)
        self.addSubview(btnBG)
        btnBG.addTarget(self, action: #selector(btnBGClicked), for: .touchUpInside)
        arrData = shippingData
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let baseView = UIView()
        baseView.backgroundColor = UIColor.white
        self.addSubview(baseView)
        baseView.enableAutoLayout()
        baseView.leadingMargin(pixels: 20)
        baseView.trailingMargin(pixels: 20)
        baseView.centerY()
        baseView.fixedHeight(pixels: SCREEN_HEIGHT * 0.6)
        
        let viewTitle = UIView()
        viewTitle.backgroundColor = UIColor.appThemeColor
        baseView.addSubview(viewTitle)
        viewTitle.enableAutoLayout()
        viewTitle.leadingMargin(pixels: 0)
        viewTitle.trailingMargin(pixels: 0)
        viewTitle.topMargin(pixels: 0)
        viewTitle.fixedHeight(pixels: 50)
        
        let lblTitle = UILabel()
        lblTitle.font = UIFont.appBoldFont(size: 18)
        lblTitle.text = localize(string: "Shipping & Delivery")
        lblTitle.textColor = .white
        lblTitle.backgroundColor = UIColor.appThemeColor
        viewTitle.addSubview(lblTitle)
        lblTitle.enableAutoLayout()
        lblTitle.leadingMargin(pixels: 10)
        lblTitle.trailingMargin(pixels: 10)
        lblTitle.topMargin(pixels: 0)
        lblTitle.bottomMargin(pixels: 0)
        
        let btnClose = UIButton()
        btnClose.setImage(UIImage(named: "ic_close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnClose.tintColor = .white
        btnClose.addTarget(self, action: #selector(btnBGClicked), for: .touchUpInside)
        viewTitle.addSubview(btnClose)
        btnClose.enableAutoLayout()
        btnClose.trailingMargin(pixels: 10)
        btnClose.centerY()
        btnClose.fixedWidth(pixels: 30)
        btnClose.fixedHeight(pixels: 30)
        
        let tblView = UITableView()
        tblView.dataSource = self
        baseView.addSubview(tblView)
        tblView.enableAutoLayout()
        tblView.leadingMargin(pixels: 0)
        tblView.trailingMargin(pixels: 0)
        tblView.belowToView(view: viewTitle, pixels: 0)
        tblView.bottomMargin(pixels: 0)
        
        self.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? ShippingDetailsCell
        if cell == nil {
            cell = ShippingDetailsCell(style: .default, reuseIdentifier: "Cell")
        }
        let model = arrData[indexPath.row] as! ZoneShippingModel
        cell?.lblName.text = model.name
        cell?.lblPrice.text = "$\(model.shippingFlatCost!)"
        return cell!
    }
    
    @objc func btnBGClicked()
    {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       // fatalError("init(coder:) has not been implemented")
    }
    
}

class ShippingDetailsCell: UITableViewCell {
    var lblName : UILabel!
    var lblPrice : UILabel!
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        lblPrice = UILabel()
        lblPrice.font = UIFont.appRegularFont(size: 16)
        self.addSubview(lblPrice)
        lblPrice.enableAutoLayout()
        lblPrice.trailingMargin(pixels: 10)
        lblPrice.topMargin(pixels: 0)
        lblPrice.bottomMargin(pixels: 0)
        
        lblName = UILabel()
        lblName.font = UIFont.appRegularFont(size: 16)
        self.addSubview(lblName)
        lblName.enableAutoLayout()
        lblName.leadingMargin(pixels: 10)
        lblName.topMargin(pixels: 10)
        lblName.bottomMargin(pixels: 10)
       // lblName.addToLeftToView(view: lblPrice, pixels: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
}
