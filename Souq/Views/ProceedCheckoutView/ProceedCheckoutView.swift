//
//  ProceedCheckoutView.swift
//  Souq
//
//  Created by Pawan Ramteke on 11/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class ProceedCheckoutView: UIView {

    var proceedClosure : (()->())?
    var continueClosure : (()->())?

    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var viewBaseTopConstr: NSLayoutConstraint!
    @IBOutlet weak var imgViewProduct: UIImageView!
    
    @IBOutlet weak var lblProductBrand: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnProceed: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnContinue.layer.borderColor = UIColor.appThemeColor.cgColor
        btnContinue.setTitleColor(UIColor.appThemeColor, for: .normal)
        
        self.viewBaseTopConstr.constant = 0
        UIView.animate(withDuration: 0.4) {
            self.layoutIfNeeded()
            self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
    }
    
    @IBAction func btnProceedClicked(_ sender: Any) {
        if proceedClosure != nil {
            proceedClosure!()
        }
        
        btnBGClicked(nil)
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        if continueClosure != nil {
            continueClosure!()
        }
        btnBGClicked(nil)
    }
    
    
    func onProceedToCheckout(closure : @escaping  ()->())
    {
        proceedClosure = closure
    }
    
    func onContinueToShopping(closure : @escaping  ()->())
    {
        continueClosure = closure
    }
    
    @IBAction func btnBGClicked(_ sender: Any?) {
        self.viewBaseTopConstr.constant = -309
        UIView.animate(withDuration: 0.4, animations: {
            self.layoutIfNeeded()
            self.backgroundColor = UIColor.black.withAlphaComponent(0)
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
}
