//
//  CancelOrderView.swift
//  Souq
//
//  Created by Pawan Ramteke on 25/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class CancelOrderView: UIView {
    
    var cancelClosure : (()->())?
    @IBOutlet weak var lblOrderID: UILabel!
    
    @IBOutlet weak var txtViewComment: UITextView!
    var orderId :String! {
        didSet {
            lblOrderID.text = "\(localize(string:"Order Id")) : \(orderId!)"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    func cancelOrderAPI()
    {
        let param = [
            "order_status_id" : orderId,
            "notify": 1,
            "comment":txtViewComment.text!
        ] as [String : Any]
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        
        let api = "orderhistory/\(orderId!)"

        RemoteAPI().callPOSTApi(apiName: api, params: param as [String : AnyObject], apiType: .put, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(localize(string: "Your order has been cancelled successfully"))
            if self.cancelClosure != nil {
                self.cancelClosure!()
            }
            self.btnBGClicked(nil)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    @IBAction func btnCancelOrderClicked(_ sender: Any) {
        self.endEditing(true)
        if isValidate(){
        cancelOrderAPI()
        }
    }
    
    func isValidate() -> Bool{
        
        if txtViewComment.text.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please mention your reason for cancelling order."))
            return false
        }
        else if txtViewComment.text.trimString().count < 3{
            VIEWMANAGER.showToast(localize(string: "Please mention your reason for cancelling order upto 100 charactors."))
            return false
        }
        
        return true
    }
    

    @IBAction func btnBGClicked(_ sender: Any?) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    func onCancelOrderSuccess(closure:@escaping ()->()) {
        cancelClosure = closure
    }
}
