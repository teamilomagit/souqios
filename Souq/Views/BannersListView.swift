//
//  BannersListView.swift
//  Odito
//
//  Created by Pawan Ramteke on 30/01/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit
import ASJCollectionViewFillLayout
class BannersListView: UIView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    var contentModeForBanner: UIView.ContentMode = .scaleToFill
    
    
    var SelectClosure : ((Int)->())?
    var _collectionView : UICollectionView!
    private var pageControl : UIPageControl!
    private var index = 1
    var timer : Timer!
    var bannerData = NSArray() {
        didSet {
            if bannerData.count > 0 {
                pageControl.isHidden = false
                startTimer()
                pageControl.numberOfPages = bannerData.count
            }
            _collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.white
        
        let layout = ASJCollectionViewFillLayout()
        layout.direction = .horizontal
        layout.itemSpacing = 0
        layout.itemLength = SCREEN_WIDTH
        layout.stretchesLastItems = false
        _collectionView = UICollectionView(frame: self.frame, collectionViewLayout: layout)
        _collectionView.dataSource = self
        _collectionView.delegate = self
        _collectionView.backgroundColor = UIColor.clear
        _collectionView.isPagingEnabled = true
        _collectionView.showsHorizontalScrollIndicator = false
        self.addSubview(_collectionView)
        
        _collectionView.enableAutoLayout()
        _collectionView.leadingMargin(pixels: 0)
        _collectionView.trailingMargin(pixels: 0)
        _collectionView.topMargin(pixels: 0)
        _collectionView.bottomMargin(pixels: 20)

        _collectionView.register(BannerCollectionCell.self, forCellWithReuseIdentifier: "CellID")
        
        pageControl = UIPageControl()
        pageControl.currentPage = 0
        pageControl.currentPageIndicatorTintColor = UIColor.appThemeColor
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.isHidden = true
        self.addSubview(pageControl)
        pageControl.enableAutoLayout()
        pageControl.centerX()
        pageControl.belowToView(view: _collectionView, pixels: 0)
        pageControl.fixedHeight(pixels: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        collectionView.backgroundView = bannerData.count == 0 ? collectionBGView() : nil
        return bannerData.count
    }
    
    func collectionBGView()->UIActivityIndicatorView
    {
        let loader = UIActivityIndicatorView(frame: self.bounds)
        loader.color = UIColor.appThemeColor
        loader.startAnimating()
        return loader
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath) as! BannerCollectionCell
        let model = bannerData[indexPath.row] as! BannerImagesModel
        
        
        
        cell.imgView.contentMode = contentModeForBanner
        
        
        
        cell.imgView.sd_setImage(with: URL(string:model.image.replacingOccurrences(of: " ", with: "%20")), placeholderImage: UIImage(named:"placeholder_img"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if SelectClosure != nil {
            SelectClosure!(indexPath.row)
        }
    }
    
    func startTimer()
    {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector:#selector(timerEvent) , userInfo: nil, repeats: true)
    }
    
    @objc func timerEvent()
    {
        if index < bannerData.count {
            _collectionView.setContentOffset(CGPoint(x:SCREEN_WIDTH * CGFloat(index),y:0), animated: true)
            index = index + 1
        }
        else {
            index = 1
            _collectionView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        }
        pageControl.currentPage = index - 1
    }
    
    func onSelectImage(closure : @escaping (Int)->())
    {
        SelectClosure = closure
    }
}

class BannerCollectionCell: UICollectionViewCell {
    
    var imgView : UIImageView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
        imgView = UIImageView()
        imgView.image = UIImage(named: "img_institute_banner")
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        self.addSubview(imgView)
        imgView.enableAutoLayout()
        imgView.leadingMargin(pixels: 0)
        imgView.trailingMargin(pixels: 0)
        imgView.topMargin(pixels: 0)
        imgView.bottomMargin(pixels: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
