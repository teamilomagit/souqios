//
//  AppButton.swift
//  Souq
//
//  Created by Pawan Ramteke on 11/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class AppButton: UIButton {

    private var loader : UIActivityIndicatorView!
    private var title : String!
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       // fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.setTitleColor(UIColor.white, for: .normal)
        self.backgroundColor = UIColor.appThemeColor
        self.titleLabel?.font = UIFont.appMediumFont(size: 16)
        
        loader = UIActivityIndicatorView(activityIndicatorStyle: .white)
        loader.hidesWhenStopped = true
        self.addSubview(loader)
        loader.enableAutoLayout()
        loader.centerX()
        loader.centerY()
        title = self.titleLabel?.text
    }

    func showLoader() {
        loader.startAnimating()
        self.isUserInteractionEnabled = false
        self.setTitle("", for: .normal)
    }
    
    func hideLoader() {
        loader.stopAnimating()
        self.isUserInteractionEnabled = true
        self.setTitle(localize(string: title), for: .normal)
    }
    
}
