//
//  ReviewProductView.swift
//  Souq
//
//  Created by Pawan Ramteke on 25/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import HCSStarRatingView
class ReviewProductView: UIView {

    @IBOutlet weak var txtFieldName: CustomTextField!
    @IBOutlet weak var txtViewReview: UITextView!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    var review : CGFloat = 0.0
    var productID : String!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.alpha = 0
        txtFieldName.leftViewMode = .always
        txtFieldName.leftView = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 10, height: 40)))
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
        ratingView.addTarget(self, action: #selector(reviewChange), for: .valueChanged)
    }
    
    func setYourReviewAPI()
    {
        let param = [
            "name":txtFieldName.text!,
            "text":txtViewReview.text!,
            "rating":review
            ] as [String : Any]
        
        let api = "\(GET_PRODUCT_LISTING_API)/\(productID!)/review"
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: api, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.btnBGClicked(nil)
            VIEWMANAGER.showToast(localize(string: "Rated Successfully"))
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    @objc func reviewChange() {
        review = ratingView.value
    }

    @IBAction func btnSubmitClicked(_ sender: Any) {
        if validate() {
            setYourReviewAPI()
        }
    }
    
    @IBAction func btnBGClicked(_ sender: Any?) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    func validate() -> Bool {
        if txtFieldName.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter name"))
            return false
        }
       
        if txtViewReview.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please write your review"))
            return false
        }
        if review == 0 {
            VIEWMANAGER.showToast(localize(string: "Please give rating"))
            return false
        }
        return true
        
    }
}
