//
//  CustomTextField.swift
//  Odito
//
//  Created by Pawan Ramteke on 31/01/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class CustomTextField: UITextField,UITextFieldDelegate {

    enum TextFieldTag : Int {
        case TAG_MOBILE = 100
        case TAG_DATE_PICKER = 101
        case TAG_TIME_PICKER =  104
        case TAG_ACTION_SHEET = 102
        case TAG_DROP_DOWN   = 103
        case TAG_PASSWORD    = 105
    }
    
    @IBInspectable var leftImage : UIImage? = nil  {
        didSet{
            let imgView = UIImageView(frame: CGRect(x: 0, y:0, width: 40, height: 40))
            imgView.image = leftImage
            //imgView.tintColor = UIColor.white
            imgView.contentMode = .center
            
            if Preferences.getAppLanguage() == ENGLISH {
                self.leftViewMode = .always
                self.leftView = imgView
            }
            else {
                self.rightViewMode = .always
                self.rightView = imgView
            }
        }
    }
    
    @IBInspectable var rightImage : UIImage? = nil  {
        didSet{
            self.rightViewMode = .always
            let imgView = UIImageView(frame: CGRect(x: 0, y:0, width: 40, height: 40))
            imgView.image = rightImage?.withRenderingMode(.alwaysTemplate)
            imgView.tintColor = UIColor.white
            imgView.contentMode = .center
            self.rightView = imgView
        }
    }
    
    var actionSheetData : NSArray!
    var dropDownData : NSArray!

    var onDataSelect : ((String)->())?
    var onDropDownDataSelect : ((String)->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont.appRegularFont(size: 18)
        self.borderStyle = .none
        self.delegate = self
    }
    func setTextFieldTag(tag:TextFieldTag)
    {
        self.tag = tag.rawValue
        if tag == .TAG_MOBILE{
            self.keyboardType = .phonePad
        }
        
        if tag == .TAG_ACTION_SHEET || tag == .TAG_DROP_DOWN {
            let imgView = UIImageView(frame: CGRect(x: 0, y:0, width: 40, height: 20))
            imgView.image = UIImage(named: "ic_dropdown")?.withRenderingMode(.alwaysTemplate)
            imgView.tintColor = UIColor.black
            imgView.contentMode = .center
            if Preferences.getAppLanguage() == ENGLISH {
                self.rightViewMode = .always
                self.rightView = imgView
            }
            else{
                self.leftViewMode = .always
                self.leftView = imgView
            }
        }
        
        if tag == .TAG_PASSWORD {
            let btnEye = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            btnEye.setImage(UIImage(named: "ic_eye_open")?.withRenderingMode(.alwaysTemplate), for: .normal)
            btnEye.setImage(UIImage(named: "ic_eye_close")?.withRenderingMode(.alwaysTemplate), for: .selected)
            btnEye.tintColor = UIColor.gray
            btnEye.addTarget(self, action: #selector(btnEyeClicked(_:)), for: .touchUpInside)
            
            if Preferences.getAppLanguage() == ENGLISH {
                self.rightViewMode = .always
                self.rightView = btnEye
            }
            else{
                self.leftViewMode = .always
                self.leftView = btnEye
            }
        }
    }
    
    @objc func btnEyeClicked(_ sender:UIButton)
    {
        sender.isSelected = !sender.isSelected
        self.isSecureTextEntry = !self.isSecureTextEntry
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,let textRange = Range(range, in: text)
        {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            
//            if self.tag == TextFieldTag.TAG_MOBILE.rawValue
//            {
//                return updatedText.count <= 10
//            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == TextFieldTag.TAG_DATE_PICKER.rawValue {
            self.superview?.endEditing(true)
            showDatePicker(mode: .date)
            return false
        }
        if textField.tag == TextFieldTag.TAG_TIME_PICKER.rawValue {
            self.superview?.endEditing(true)
            showDatePicker(mode: .time)
            return false
        }
        
        if textField.tag == TextFieldTag.TAG_ACTION_SHEET.rawValue {
            self.superview?.endEditing(true)
            showActionSheet()
            return false
        }
        if textField.tag == TextFieldTag.TAG_DROP_DOWN.rawValue {
            self.superview?.endEditing(true)
            showDropDown()
            return false
        }
        return true
    }
    
    func showActionSheet()
    {
        if actionSheetData == nil || actionSheetData.count == 0 {
            return
        }
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: localize(string: "Cancel"), style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        for i in actionSheetData {
            let alertAction = UIAlertAction(title: i as? String, style: .default, handler: { (action) in
                self.text = action.title
                if self.onDataSelect != nil{
                    self.onDataSelect!(action.title!)
                }
            })
            alert.addAction(alertAction)
            
        }
        VIEWMANAGER.topMostController().present(alert, animated: true, completion: nil)
    }
    
    func showDropDown() {
        if dropDownData == nil  {
            return
        }
        let dropDown = DropDownListView(frame: UIScreen.main.bounds, title: self.placeholder, data: dropDownData as! [String])
        UIApplication.shared.keyWindow?.addSubview(dropDown)
        dropDown.onDoneClicked { (string) in
            self.text = string
            if self.onDropDownDataSelect != nil {
                self.onDropDownDataSelect!(string)
            }
        }
    }
    
    func showDatePicker(mode:UIDatePickerMode)
    {
        let datePicker = DatePickerView(frame: UIScreen.main.bounds, pickerMode: mode)
        VIEWMANAGER.topMostController().view.addSubview(datePicker)
        datePicker.onDoneClicked { (date) in
            self.text = self.formattedDate(selDate: date, mode: mode)
        }
    }
    
    
    func formattedDate(selDate:Date!,mode:UIDatePickerMode) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = mode == .time ? "hh:mm a" : "dd/MM/yyyy"
        var dateToPass: String? = nil
        
        dateToPass = dateFormat.string(from: selDate)
        
        return dateToPass!
    }
    
    func onDataSelectionSuccess(success:@escaping (String)->()){
        onDataSelect = success
    }
    

    func onDropDownSelectionSuccess(success:@escaping (String)->()){
        onDropDownDataSelect = success
    }
    
}
