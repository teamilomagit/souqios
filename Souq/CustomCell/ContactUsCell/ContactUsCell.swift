//
//  ContactUsCell.swift
//  Souq
//
//  Created by iLoma Technology on 23/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import MapKit
class ContactUsCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var mapVIew: MKMapView!
    @IBOutlet weak var btnCall: UIButton!
    
    var location : CLLocationCoordinate2D? {
        didSet {
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegionMake(location!, span)
            mapVIew.region = region
            let annotation = MKPointAnnotation()
            annotation.title = lblTitle.text
            annotation.coordinate = location!
            mapVIew.addAnnotation(annotation)
            btnCall.addTarget(self, action: #selector(btnCallClicked), for: .touchUpInside)
        }
    }
    
    @objc func btnCallClicked() {
        let url = URL(string: "telprompt://+917736950877")
        if url != nil{
            UIApplication.shared.open(url!)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
