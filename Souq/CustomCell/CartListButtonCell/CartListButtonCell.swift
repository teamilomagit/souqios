//
//  CartListButtonCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 11/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class CartListButtonCell: UITableViewCell {

    var btnClosure : (()->())?
    @IBOutlet weak var btnContinue: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnContinue.layer.borderWidth = 2.0
        btnContinue.layer.borderColor = UIColor.appThemeColor.cgColor
        btnContinue.titleLabel?.font = UIFont.appBoldFont(size: 16)
        btnContinue.setTitleColor(UIColor.appThemeColor, for: .normal)
        btnContinue.addTarget(self, action: #selector(btnContinueClicked), for: .touchUpInside)
    }

    @objc func btnContinueClicked(){
        btnClosure!()
    }
    
    func onContineShoppingClicked(closure:@escaping ()->()){
        btnClosure = closure
    }
    
}
