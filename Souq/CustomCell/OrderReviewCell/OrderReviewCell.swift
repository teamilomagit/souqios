//
//  OrderReviewCell.swift
//  Souq
//
//  Created by iLoma on 19/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class OrderReviewCell: UITableViewCell {
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblProductModel: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var imgViewProduct: UIImageView!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblDeleveryPeriod: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        baseView.layer.borderWidth = 0.5
        baseView.layer.borderColor = UIColor.lightGray.cgColor
    }

}
