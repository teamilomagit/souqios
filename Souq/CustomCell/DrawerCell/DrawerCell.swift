//
//  DrawerCell.swift
//  Souq
//
//  Created by iLoma on 18/01/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class DrawerCell: UITableViewCell {
    @IBOutlet weak var lblMenuTitle: UILabel!
    @IBOutlet weak var btnIcon: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnIconWtConstr: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblMenuTitle.font = UIFont.appRegularFont(size: 14)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
