//
//  HomeProductsListCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 04/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import ASJCollectionViewFillLayout
class HomeProductsListCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate {

    var viewAllClosure : ((IndexPath) -> ())?
    var selectProductClosure : ((ProductListingModel) -> ())?

    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var _collectionView: UICollectionView!
    @IBOutlet weak var btnViewAll: UIButton!
    
    var indexPath : IndexPath!
    
    var arrProducts : NSArray! = NSArray(){
        didSet{
            _collectionView.reloadData()
            if arrProducts == nil {
                arrProducts = NSArray()
            }
            if arrProducts.count > 0{
                if Preferences.getAppLanguage() == ARABIC {
                    _collectionView.scrollToItem(at: IndexPath(item: arrProducts.count - 1, section: 0), at: .right, animated: false)
                }
                else {
                    _collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .right, animated: false)
                }
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.white
        _collectionView.dataSource = self
        _collectionView.delegate = self
        
        let layout = ASJCollectionViewFillLayout()
        layout.direction = .horizontal
        layout.itemSpacing = 5
        layout.itemLength = 170
        
        _collectionView.setCollectionViewLayout(layout, animated: true)
        _collectionView.register(UINib(nibName: "ProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionCell")
        
        lblCategoryTitle.font = UIFont.appMediumFont(size: 16)
        btnViewAll.titleLabel?.font = UIFont.appBoldFont(size: 13)
        btnViewAll.layer.borderWidth = 1.0
        btnViewAll.layer.borderColor = UIColor.appThemeColor.cgColor
        btnViewAll.setTitleColor(UIColor.white, for: .normal)
        btnViewAll.addTarget(self, action: #selector(btnViewAllClicked), for: .touchUpInside)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionCell
        let model = Preferences.getAppLanguage() == ENGLISH ?  arrProducts[indexPath.row] as! ProductListingModel : arrProducts[arrProducts.count - 1 - indexPath.row] as! ProductListingModel
        cell.lblProductName.text = model.name
        cell.imageViewProduct.sd_setImage(with: URL(string:VIEWMANAGER.getProperImageUrl(strUrl: model.thumb) ?? VIEWMANAGER.getProperImageUrl(strUrl: model.originalImage) ?? ""), placeholderImage: UIImage(named:"placeholder_img"))
        cell.lblProductPrice.text = model.priceFormated
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = Preferences.getAppLanguage() == ENGLISH ?  arrProducts[indexPath.row] as! ProductListingModel : arrProducts[arrProducts.count - 1 - indexPath.row] as! ProductListingModel
        if selectProductClosure != nil {
            selectProductClosure!(model)
        }
    }

    @objc func btnViewAllClicked()
    {
        if viewAllClosure != nil {
            viewAllClosure!(indexPath)
        }
    }
    
    func onViewAllProducts(closure :((IndexPath) -> ())?) {
        viewAllClosure = closure!
    }
    
    func onSelectProducts(closure :((ProductListingModel) -> ())?) {
        selectProductClosure = closure!
    }
    
}
