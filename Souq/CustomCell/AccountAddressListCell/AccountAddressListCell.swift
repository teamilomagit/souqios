//
//  AccountAddressListCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 16/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class AccountAddressListCell: UITableViewCell {
    
    var editClosure : ((IndexPath)->())?
    var deleteClosure : ((IndexPath)->())?
    
    @IBOutlet weak var lblZone: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    
    var indexPath : IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnEdit.addTarget(self, action: #selector(btnEditClicked), for: .touchUpInside)
        btnDelete.addTarget(self, action: #selector(btnDeleteClicked), for: .touchUpInside)
    }
    
    @objc func btnEditClicked() {
        if editClosure != nil {
            editClosure!(indexPath)
        }
    }
    
    @objc func btnDeleteClicked() {
        if deleteClosure != nil {
            deleteClosure!(indexPath)
        }
    }

    func onEditAddressClicked(closure:@escaping (IndexPath)->()) {
        editClosure = closure
    }
    
    func onDeleteAddressClicked(closure:@escaping (IndexPath)->()) {
        deleteClosure = closure
    }
}
