//
//  OrderHHistoryDetailCell.swift
//  Souq
//
//  Created by iLoma on 11/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class OrderHHistoryDetailCell: UITableViewCell {
    
    var reviewClosure : ((IndexPath)->())?
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var btnWriteReview: UIButton!
    var indexPath : IndexPath!
    
    @IBOutlet weak var viewOrdered: UIView!
    @IBOutlet weak var viewProcessing: UIView!
    @IBOutlet weak var viewDelivered: UIView!
    @IBOutlet weak var viewShipped: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        baseView.layer.borderWidth = 0.5
        baseView.layer.borderColor = UIColor.lightGray.cgColor
        btnWriteReview.addTarget(self, action: #selector(btnWriteReviewClicked), for: .touchUpInside)
    }

    @objc func btnWriteReviewClicked()
    {
        if reviewClosure != nil {
            reviewClosure!(indexPath)
        }
    }
    
    func onWriteReview(closure:@escaping (IndexPath)->()) {
        reviewClosure = closure
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
