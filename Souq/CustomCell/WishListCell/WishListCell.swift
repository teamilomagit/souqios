//
//  WishListCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 12/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class WishListCell: UITableViewCell {

    @IBOutlet weak var baseView: UIView!
    var addToCartClosure : ((IndexPath)->())?
    var removeClosure : ((IndexPath)->())?
    @IBOutlet weak var lblModel: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblStock: UILabel!
    
    @IBOutlet weak var btnAddtoCart: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    var indexPath : IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        baseView.layer.borderWidth = 0.5
        baseView.layer.borderColor = UIColor.lightGray.cgColor
        btnAddtoCart.addTarget(self, action: #selector(btnAddToCartClicked), for: .touchUpInside)
        btnRemove.addTarget(self, action: #selector(btnRemoveClicked), for: .touchUpInside)

    }
    
    @objc func btnAddToCartClicked()
    {
        addToCartClosure!(indexPath)
    }

    @objc func btnRemoveClicked()
    {
        removeClosure!(indexPath)
    }
    
    func onAddToCartClicked(closure:@escaping (IndexPath)->()) {
        addToCartClosure = closure
    }
    
    func onRemoveClicked(closure:@escaping (IndexPath)->()) {
        removeClosure = closure
    }
}
