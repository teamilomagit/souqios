//
//  ProductCollectionCell.swift
//  Souq
//
//  Created by iLoma on 04/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class ProductCollectionCell: UICollectionViewCell {
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        baseView.layer.borderWidth = 0.5
        baseView.layer.borderColor = UIColor.lightGray.cgColor
        lblProductName.font = UIFont.appRegularFont(size: 12)
        lblProductPrice.font = UIFont.appBoldFont(size: 18)
    }

}
