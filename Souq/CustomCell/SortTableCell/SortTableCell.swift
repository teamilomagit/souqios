//
//  SortTableCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 12/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class SortTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnRadio: UIButton!
    var indexPath : IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
