//
//  CartListCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 11/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import ASJCollectionViewFillLayout
class CartListCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var removeCloser : ((IndexPath) -> ())?
    var qtyDropdownClickCloser : ((IndexPath) -> ())?
    var productQuantityCloser : ((IndexPath,Int) -> ())?

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductModel: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var imgViewProduct: UIImageView!
    @IBOutlet weak var btnProductQuantity: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var lblOutofStock: UILabel!
    
    let arrQuantity = NSMutableArray()
    
    @IBOutlet weak var collectionQuantiy: UICollectionView!
    var indexpath : IndexPath!
    var quantity : Int!{
        didSet {
            collectionQuantiy.reloadData()
        }
    }
    @IBOutlet weak var collectionViewHtConstr: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        baseView.layer.borderWidth = 0.5
        baseView.layer.borderColor = UIColor.lightGray.cgColor
        btnRemove.addTarget(self, action: #selector(btnRemoveClicked), for: .touchUpInside)
        btnProductQuantity.addTarget(self, action: #selector(btnProductQuantityClicked), for: .touchUpInside)
        btnProductQuantity.layer.borderWidth = 1.0
        btnProductQuantity.layer.borderColor = UIColor.appThemeColor.cgColor
        if Preferences.getAppLanguage() == ENGLISH {
            btnRemove.imageEdgeInsets = UIEdgeInsets(top:0 , left: 0, bottom: 0, right: 10)
            btnProductQuantity.semanticContentAttribute = .forceRightToLeft
            btnProductQuantity.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
            btnProductQuantity.imageEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)

        }
        else {
            btnRemove.imageEdgeInsets = UIEdgeInsets(top:0 , left: 10, bottom: 0, right: 0)
            btnProductQuantity.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            btnProductQuantity.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
            btnProductQuantity.semanticContentAttribute = .forceLeftToRight
        }
        
        let layout = ASJCollectionViewFillLayout()
        layout.direction = .horizontal
        layout.itemSpacing = 5
        layout.itemLength = 50

        collectionQuantiy.register(QuantityCell.self, forCellWithReuseIdentifier: "CellID")
        collectionQuantiy.delegate = self
        collectionQuantiy.dataSource = self
        collectionQuantiy.setCollectionViewLayout(layout, animated: true)
        
        collectionViewHtConstr.constant = 0
    }
    @objc func btnRemoveClicked(_ sender:UIButton)
    {
        if removeCloser != nil{
            removeCloser!(indexpath)
        }
    }
    func onRemoveClicked(closure:@escaping (IndexPath) -> ())
    {
        removeCloser = closure
    }
    func onDropdownClicked(closure:@escaping (IndexPath) -> ())
    {
        qtyDropdownClickCloser = closure
    }
    
    @objc func btnProductQuantityClicked(_ sender:UIButton)
    {
        if qtyDropdownClickCloser != nil {
            self.qtyDropdownClickCloser!(self.indexpath)
        }
        //setShowHideQuantity()
        
        
    }
    func onQuantityClicked(closure:@escaping (IndexPath,Int) -> ())
    {
        productQuantityCloser = closure
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath) as! QuantityCell
        cell.lblQuantity.text = "\(indexPath.row + 1)"
        if indexPath.row == quantity - 1  {
            cell.backgroundColor = UIColor.appThemeColor
            cell.lblQuantity.textColor = UIColor.white
            cell.layer.borderColor = UIColor.clear.cgColor
        }
        else {
            cell.backgroundColor = UIColor.clear
            cell.lblQuantity.textColor = UIColor.black
            cell.layer.borderColor = UIColor.controllerBGColor.cgColor

        }
      return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indxPath: IndexPath) {
        if productQuantityCloser != nil{
            productQuantityCloser!(self.indexpath,indxPath.row + 1)
        }
    }
    
}
