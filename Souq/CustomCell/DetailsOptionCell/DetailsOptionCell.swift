//
//  DetailsOptionCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 12/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class DetailsOptionCell: UITableViewCell {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var imgRightArrow: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        baseView.layer.borderWidth = 1.0
        baseView.layer.borderColor = UIColor.controllerBGColor.cgColor
       // baseView.layer.cornerRadius = 2
        lblValue.textColor = UIColor.appThemeColor
        
        if Preferences.getAppLanguage() == ENGLISH {
            imgRightArrow.transform = CGAffineTransform.identity
        }
        else{
            imgRightArrow.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
