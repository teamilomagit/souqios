//
//  ReviewsCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 13/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class ReviewsCell: UITableViewCell {

    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnRating.setTitleColor(UIColor.ratingGoldenColor, for: .normal)
        btnRating.setImage(UIImage(named: "ic_star")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnRating.tintColor = UIColor.ratingGoldenColor
        
        btnRating.titleEdgeInsets = UIEdgeInsetsMake(0, Preferences.getAppLanguage() == ENGLISH ? 5 : 0, 0, Preferences.getAppLanguage() == ENGLISH ? 0 : 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
