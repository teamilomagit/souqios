//
//  OrderHistoryCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 14/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblPlacedDate: UILabel!
    @IBOutlet weak var viewOrdered: UIView!
    @IBOutlet weak var viewProcessing: UIView!
    @IBOutlet weak var viewDelivered: UIView!
    @IBOutlet weak var viewShipped: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
