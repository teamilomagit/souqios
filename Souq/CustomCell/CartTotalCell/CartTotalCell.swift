//
//  CartTotalCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 11/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class CartTotalCell: UITableViewCell {
    
    @IBOutlet weak var viewEnterCoupon: UIView!
    @IBOutlet weak var txtFieldCouponCard: UITextField!
    @IBOutlet weak var btnApply : UIButton!
    
    @IBOutlet weak var viewSubTotal: UIView!
    @IBOutlet weak var lblSubTotalTitle: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    
    @IBOutlet weak var viewCoupon: UIView!
    @IBOutlet weak var lblCouponTitle: UILabel!
    @IBOutlet weak var lblCoupon: UILabel!
    
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    @IBOutlet weak var btnRemoveCoupon: UIButton!

    var applyClosure :((String)->())?
    var DeleteClosure :(()->())?

    var arrTotals = [Total]() {
        didSet {
            if arrTotals.count == 3 {
                viewCoupon.isHidden = false
                let subTotal = arrTotals[0]
                lblSubTotalTitle.text = subTotal.title
                lblSubTotal.text = subTotal.text
                
                let coupon = arrTotals[1]
                lblCouponTitle.text = coupon.title
                lblCoupon.text = coupon.text

                let total = arrTotals[2]
                lblTotalTitle.text = total.title
                lblTotal.text = total.text

            }
            else if arrTotals.count == 2 {
                viewCoupon.isHidden = true
                
                let subTotal = arrTotals[0]
                lblSubTotalTitle.text = subTotal.title
                lblSubTotal.text = subTotal.text
                
                let total = arrTotals[1]
                lblTotalTitle.text = total.title
                lblTotal.text = total.text
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.white
        
        viewEnterCoupon.layer.borderWidth = 0.5
        viewEnterCoupon.layer.borderColor = UIColor.lightGray.cgColor
        
        btnApply.addTarget(self, action: #selector(btnApplyCouponClicked(_:)), for: .touchUpInside)
        btnRemoveCoupon.addTarget(self, action: #selector(btnRemoveCouponClicked), for: .touchUpInside)

        viewCoupon.isHidden = true
    }
    
    @objc func btnApplyCouponClicked(_ sender: Any) {
        self.superview?.endEditing(true)
        if txtFieldCouponCard.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter coupon code"))
            return
        }
        applyClosure!(txtFieldCouponCard.text!)
    }
    
    @objc func btnRemoveCouponClicked() {
        self.DeleteClosure!()
    }
    
    func onApplyCouponClicked(closure:@escaping (String)->()){
        applyClosure = closure
    }
    
    func onRemoveCouponClicked(closure:@escaping ()->()){
        DeleteClosure = closure
    }
}
