//
//  FiltersTableCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 07/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class FiltersTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgRightArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        if Preferences.getAppLanguage() == ENGLISH {
        imgRightArrow.transform = CGAffineTransform.identity
        }
        else{
            imgRightArrow.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
