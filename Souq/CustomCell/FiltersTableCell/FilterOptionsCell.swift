//
//  FilterOptionsCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 07/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class FilterOptionsCell: UITableViewCell {

    var checkBlock : ((IndexPath)->())?
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    var indexPath : IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnCheck.isUserInteractionEnabled = false
        btnCheck.addTarget(self, action: #selector(btnCheckClicked), for: .touchUpInside)
    }

    @objc func btnCheckClicked()
    {
        if checkBlock != nil {
            checkBlock!(indexPath)
        }
    }
    
    func onOptionSelection(closure:@escaping (IndexPath)->()) {
        checkBlock = closure
    }
}
