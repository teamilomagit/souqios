//
//  SelectAddressCell.swift
//  Souq
//
//  Created by Pawan Ramteke on 02/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class SelectAddressCell: UITableViewCell {

    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
