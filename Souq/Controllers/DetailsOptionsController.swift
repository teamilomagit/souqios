//
//  DetailsOptionsController.swift
//  Souq
//
//  Created by Pawan Ramteke on 12/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class DetailsOptionsController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    var selOptionClosure : ((NSArray)->())?
    var arrValues = NSMutableArray()
    @IBOutlet weak var tblViewList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblViewList.register(UINib(nibName: "SortTableCell", bundle: nil), forCellReuseIdentifier: "SortTableCell")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortTableCell") as! SortTableCell
        let model = arrValues[indexPath.row] as! OptionValue
        cell.indexPath = indexPath
        cell.lblTitle.text = model.price == 0 ? model.name : "\(model.name!) (\(model.pricePrefix!)\(model.priceFormated!))"
        cell.btnRadio.isSelected = model.isSelected
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        resetData()
        let model = arrValues[indexPath.row] as! OptionValue
        model.isSelected = true
        arrValues.replaceObject(at: indexPath.row, with: model)
       // tblViewList.reloadData()
        if selOptionClosure != nil {
            selOptionClosure!(arrValues)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func resetData()
    {
        for i in 0 ..< arrValues.count {
            let model = arrValues[i] as! OptionValue
            model.isSelected = false
            arrValues.replaceObject(at: i, with: model)
        }
    }
    
    func onOptionSelecton(closure:@escaping (NSArray)->()) {
        selOptionClosure = closure
    }
}
