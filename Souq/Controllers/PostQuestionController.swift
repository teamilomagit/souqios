//
//  PostQuestionController.swift
//  Souq
//
//  Created by Pawan Ramteke on 15/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class PostQuestionController: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var txtFieldName: CustomTextField!
    @IBOutlet weak var txtFieldEmail: CustomTextField!
    @IBOutlet weak var txtViewQuestion: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if Preferences.getAppLanguage() != ENGLISH {
            btnBack.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        if VIEWMANAGER.currentUser != nil{
        txtFieldName.text = "\(VIEWMANAGER.currentUser!.firstname!) \(VIEWMANAGER.currentUser!.lastname!)"
        txtFieldEmail.text = "\(VIEWMANAGER.currentUser!.email!)"
        }
        txtViewQuestion.text = ""
        
        txtViewQuestion.placeholder = localize(string: "Type Your Question")
    }
    
    func postQuestionAPI()
    {
        let param = [
            "name":txtFieldName.text!,
            "email" : txtFieldEmail.text!,
            "enquiry" : txtViewQuestion.text!
        ]
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: POST_YOUR_QUESTION_API, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(localize(string: "Question sent successfully"))
            self.dismissController(nil)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        self.view.endEditing(true)
        if  validate() {
            postQuestionAPI()
        }
    }
    
    func validate() -> Bool {
        if txtFieldName.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string:"Please enter name"))
            return false
        }
        if txtFieldEmail.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string:"Please enter your Email"))
            return false
        }
        if !txtFieldEmail.text!.isValidEmail() {
            VIEWMANAGER.showToast(localize(string:"Please enter valid email address"))
            return false
        }
        if txtViewQuestion.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string:"Please enter your question"))
            return false
        }
        return true
    }
    
    @IBAction func dismissController(_ sender: Any?) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
