//
//  BaseViewController.swift
//  Souq
//
//  Created by Pawan Ramteke on 04/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookLogin

class BaseViewController: UIViewController {

    var lblCartCount : UILabel!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if lblCartCount != nil {
            if Preferences.getCartCount() != 0{
                addItemToCart()
            }else{
                lblCartCount.isHidden = true
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        let imgView = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        imgView.setImage(UIImage(named: "navigation_logo"), for: .normal)
        imgView.contentMode = .scaleAspectFit
        imgView.addTarget(self, action: #selector(navigationTopLogoClicked), for: .touchUpInside)
        self.navigationItem.titleView = imgView

        if !VIEWMANAGER.isObserverd {
            NotificationCenter.default.addObserver(self, selector: #selector(drawerSelection(notification:)), name: NSNotification.Name(rawValue: DRAWER_SELECTION_NOTIFICATION), object: nil)
            VIEWMANAGER.isObserverd = true
        }
    }
    
    @objc func navigationTopLogoClicked()
    {
        if let topVC = UIApplication.getTopMostViewController() {
            print(topVC)
            
            if topVC is KYDrawerController{
                let drawer = topVC as! KYDrawerController
                print(drawer.mainViewController)
                if drawer.mainViewController is UINavigationController{
                    let nav = drawer.mainViewController as! UINavigationController
                    nav.popToRootViewController(animated: true)
                }
            }
            
        }
    }

    @objc func drawerSelection(notification:NSNotification)
    {
        let indexPath = notification.object as! IndexPath
        print(indexPath)
        
        if indexPath.section == 0 {
            let  categoriesData = VIEWMANAGER.arrCategoriesData.copy() as! NSArray
            let model = categoriesData[indexPath.row] as! CategoriesModel
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let productListingVC = storyboard.instantiateViewController(withIdentifier: "ProductListingController") as! ProductListingController
            let dataDict = model.toDictionary()
            productListingVC.selCategoryModel = CategoriesModel(fromDictionary: dataDict)
            VIEWMANAGER.topMostController().navigationController?.pushViewController(productListingVC, animated: true)
            
        } else if indexPath.section == 1 {
            if indexPath.row == 1 {
                let contactUsVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "ContactUsController") as! ContactUsController
                VIEWMANAGER.topMostController().navigationController?.pushViewController(contactUsVC, animated: true)
            }
            else if indexPath.row == 2 {
                let helpVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "HelpController") as! HelpController
                VIEWMANAGER.topMostController().navigationController?.pushViewController(helpVC, animated: true)
            }
            else if indexPath.row == 3 {
                VIEWMANAGER.showToast(localize(string:"Comming soon"))
            }
            
            else if indexPath.row == 4 {
                if VIEWMANAGER.currentUser != nil {
                    if VIEWMANAGER.currentUser != nil {
                        CustomAlertView.showAlertWithYesNo(title: localize(string: "LOGOUT"), messsage: localize(string: "Are you sure you want to logout?")) {
                            self.logoutService()
                        }
                    }
                }
                else {
                    let sellOnVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "SellOnController") as! SellOnController
                    VIEWMANAGER.topMostController().navigationController?.pushViewController(sellOnVC, animated: true)
                }
            }
            
            else if indexPath.row == 5 {
                let sellOnVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "SellOnController") as! SellOnController
                VIEWMANAGER.topMostController().navigationController?.pushViewController(sellOnVC, animated: true)
            }
            
        }
            
    
            
        else  {
            if indexPath.row == 3 {
                if VIEWMANAGER.currentUser != nil {
                    CustomAlertView.showAlertWithYesNo(title: localize(string: "Logout"), messsage: localize(string: "Are you sure, you want to logout?")) {
                        self.logoutService()
                    }
                }
            }
        }
        
        
    }
    
    func logoutService()
    {
        let param = ["X-Oc-Session" : Preferences.getSessionKey()
                     ] as [String : AnyObject]
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName:LOGOUT_URL, params: param, completion: { (responseData) in
            Preferences.clearLoginData()
            let loginManager = LoginManager()
            loginManager.logOut()
            GIDSignIn.sharedInstance().signOut()
            VIEWMANAGER.currentUser = nil
            if self.lblCartCount != nil {
                self.lblCartCount.isHidden = true
            }
            VIEWMANAGER.showToast(localize(string: "You are successfully logout"))
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func setDrawerButtonToNavigation()
    {
        let btnDrawer = UIButton(type: .custom)
        btnDrawer.setImage(UIImage(named: "ic_drawer"), for: .normal)
        btnDrawer.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnDrawer.addTarget(self, action: #selector(btnDrawerClicked), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btnDrawer)
        self.navigationItem.leftBarButtonItem = item
    }
    
    func setSearchAndCartButtonsToNavigation()
    {
        let btnSearch = UIButton(type: .custom)
        btnSearch.setImage(UIImage(named: "ic_search"), for: .normal)
        btnSearch.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnSearch.addTarget(self, action: #selector(btnSearchClicked), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btnSearch)
        
        let btnCart = UIButton(type: .custom)
        btnCart.setImage(UIImage(named: "shopping_cart"), for: .normal)
        btnCart.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnCart.addTarget(self, action: #selector(btnCartClicked), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btnCart)
        
        lblCartCount = UILabel(frame: CGRect(x: 15, y: -5, width: 20, height: 20))
        lblCartCount.layer.borderColor = UIColor.white.cgColor
        lblCartCount.layer.borderWidth = 1
        lblCartCount.layer.cornerRadius = lblCartCount.bounds.size.height / 2
        lblCartCount.textAlignment = .center
        lblCartCount.layer.masksToBounds = true
        lblCartCount.font = UIFont.appBoldFont(size: 13)
        lblCartCount.textColor = .white
        lblCartCount.backgroundColor = .red
        if Preferences.getCartCount() != 0{
           addItemToCart()
        }else{
            lblCartCount.isHidden = true
        }
        btnCart.addSubview(lblCartCount)
        
        self.navigationItem.rightBarButtonItems = [item2,item1]
    }
    
    func setCartButtonToNavigation()
    {
        let btnCart = UIButton(type: .custom)
        btnCart.setImage(UIImage(named: "shopping_cart"), for: .normal)
        btnCart.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnCart.addTarget(self, action: #selector(btnCartClicked), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btnCart)
        
        lblCartCount = UILabel(frame: CGRect(x: 15, y: -5, width: 20, height: 20))
        lblCartCount.layer.borderColor = UIColor.white.cgColor
        lblCartCount.layer.borderWidth = 1
        lblCartCount.layer.cornerRadius = lblCartCount.bounds.size.height / 2
        lblCartCount.textAlignment = .center
        lblCartCount.layer.masksToBounds = true
        lblCartCount.font = UIFont.appBoldFont(size: 13)
        lblCartCount.textColor = .white
        lblCartCount.backgroundColor = .red
        if Preferences.getCartCount() != 0{
            addItemToCart()
        }else{
            lblCartCount.isHidden = true
        }
        btnCart.addSubview(lblCartCount)
        self.navigationItem.rightBarButtonItem = item1
    }
    
    @objc func btnDrawerClicked()
    {
        var parentVC = self.parent
        while parentVC != nil {
            if let drawerVC = parentVC as? KYDrawerController
            {
                drawerVC.setDrawerState(.opened, animated: true)
            }
            parentVC = parentVC?.parent
        }
    }
    
    @objc func btnSearchClicked()
    {
        let searchVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVC.modalTransitionStyle = .crossDissolve
        let navVC = UINavigationController(rootViewController: searchVC)
        self.present(navVC, animated: true, completion: nil)
    }
    
    @objc func btnCartClicked()
    {
        let cartVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        VIEWMANAGER.topMostController().navigationController?.pushViewController(cartVC, animated: true)
    }
    
    
    func addItemToCart() {
        let cartCount = Preferences.getCartCount()
        lblCartCount.text = "\(cartCount!)"
        lblCartCount.isHidden = false
    }
}
