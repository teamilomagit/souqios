//
//  OrderHistoryController.swift
//  Souq
//
//  Created by Pawan Ramteke on 14/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class OrderHistoryController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblViewList: UITableView!
    let LIMIT = 10
    var pageNo = 1
    var loadStarted : Bool!
    var arrList = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = localize(string: "ORDER HISTORY")
        
        tblViewList.register(UINib(nibName: "OrderHistoryCell", bundle: nil), forCellReuseIdentifier: "OrderHistoryCell")
        
        getOrderHistoryList(pageNo: pageNo)
    }
    
    func getOrderHistoryList(pageNo:Int)
    {
        VIEWMANAGER.showActivityIndicator()
       let url = "\(ORDER_HISTORY_API)/limit/\(LIMIT)/page/\(pageNo)"
        RemoteAPI().callPOSTApi(apiName: url, params: nil, apiType: .get, completion: { (response) in
            let arrData = response as! NSArray
            
            self.arrList.addObjects(from: arrData as! [Any])
            self.tblViewList.reloadData()
            
            self.tblViewList.backgroundView = self.arrList.count > 0 ? nil : self.tableBGView()
            
            if arrData.count == self.LIMIT
            {
                self.pageNo = self.pageNo + 1
                self.loadStarted = true
                //     self.loader.startAnimating()
            }
            else{
                self.loadStarted = false
                //    self.loader.stopAnimating()
            }
            
            VIEWMANAGER.hideActivityIndicator()
            self.tblViewList.reloadData()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryCell") as! OrderHistoryCell
        let model = arrList[indexPath.row] as! OrderHistoryModel
        cell.lblOrderNumber.text = "\(localize(string: "ORDER")) \(model.orderId!)"
        cell.lblPlacedDate.text = "\(localize(string: "Placed on")) \(formattedDate(strDate: model.dateAdded!))"
        
        cell.viewOrdered.backgroundColor = UIColor.lightGray
        cell.viewProcessing.backgroundColor = UIColor.lightGray
        cell.viewShipped.backgroundColor = UIColor.lightGray
        cell.viewDelivered.backgroundColor = UIColor.lightGray
        if model.status == "Pending" {
            cell.viewOrdered.backgroundColor = UIColor.statusGreenColor
        }
        else if model.status == "Processing" {
            cell.viewOrdered.backgroundColor = UIColor.statusGreenColor
            cell.viewProcessing.backgroundColor = UIColor.statusGreenColor
        }
        else if model.status == "Shipped" {
            cell.viewOrdered.backgroundColor = UIColor.statusGreenColor
            cell.viewProcessing.backgroundColor = UIColor.statusGreenColor
            cell.viewShipped.backgroundColor = UIColor.statusGreenColor
        }
        else if model.status == "Complete" {
            cell.viewOrdered.backgroundColor = UIColor.statusGreenColor
            cell.viewProcessing.backgroundColor = UIColor.statusGreenColor
            cell.viewShipped.backgroundColor = UIColor.statusGreenColor
            cell.viewDelivered.backgroundColor = UIColor.statusGreenColor
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let historyDetailVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "OrderHistoryDetailsController") as! OrderHistoryDetailsController
        let model = arrList[indexPath.row] as! OrderHistoryModel
        historyDetailVC.strOrderId = model.orderId
        self.navigationController?.pushViewController(historyDetailVC, animated: true)
        historyDetailVC.onCancelOrderSuccess {
            self.arrList.removeAllObjects()
            self.pageNo = 1
            self.getOrderHistoryList(pageNo: self.pageNo)
        }
        
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.item == arrList.count - 5{
            if loadStarted {
                getOrderHistoryList(pageNo: pageNo)
                loadStarted = false
            }
        }
    }
    
    func tableBGView() -> UIView
    {
        let lblEmptyTitle = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 40))
        lblEmptyTitle.font = UIFont.appBoldFont(size: 16)
        lblEmptyTitle.text = localize(string: localize(string: "You have not placed any order"))
        lblEmptyTitle.textAlignment = .center

        return lblEmptyTitle
    }
    
    func formattedDate(strDate:String) -> String {
        let formatter = DateFormatter()
        if Preferences.getAppLanguage() == ENGLISH {
             formatter.dateFormat = "dd/MM/yyyy"
        }else{
             formatter.dateFormat = "yyyy/MM/dd"
        }
       
        let date = formatter.date(from:strDate)!
        formatter.dateFormat = "MMM dd, yyyy"
        let formDate = formatter.string(from:date)
        return formDate
    }
}
