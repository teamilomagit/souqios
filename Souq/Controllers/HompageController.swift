//
//  HompageController.swift
//  Souq
//
//  Created by iLoma on 31/01/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import SDWebImage
class HompageController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UITextFieldDelegate {
    var arrBanners = NSArray()
    
    @IBOutlet weak var baseScroll: UIScrollView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtFieldSearch: UITextField!
    
    @IBOutlet weak var bannerView: BannersListView!
    
    var pageControl : UIPageControl!
    @IBOutlet weak var tblViewList: UITableView!
    @IBOutlet weak var tblViewHtConstr: NSLayoutConstraint!
    var currentOffset : CGFloat!
    
    let arrProducts = NSMutableArray()
    var arrDealsOfTheDay = NSArray()
    var refreshControl : UIRefreshControl!
    var currentIdx : Int!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setDrawerButtonToNavigation()
        setCartButtonToNavigation()
        self.view.backgroundColor = UIColor.appThemeColor
        self.title = APP_NAME
        viewSearch.backgroundColor = UIColor.appThemeColor
        
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
        imgView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)
        imgView.contentMode = .scaleAspectFit
        imgView.tintColor = UIColor.gray
        
        if Preferences.getAppLanguage() == ENGLISH {
            txtFieldSearch.leftViewMode = .always
            txtFieldSearch.leftView = imgView
        }
        else {
            txtFieldSearch.rightViewMode = .always
            txtFieldSearch.rightView = imgView
        }
        
        txtFieldSearch.placeholder = localize(string: "What are you looking for?")
        tblViewList.register(UINib(nibName: "HomeProductsListCell", bundle: nil), forCellReuseIdentifier: "HomeProductsListCell")
        tblViewList.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        tblViewList.estimatedRowHeight = 250
        tblViewList.rowHeight = UITableViewAutomaticDimension
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.white
        baseScroll.addSubview(refreshControl)
        
        
        callAPIs()
    }
    
    @objc func refresh(sender:Any) {
        callAPIs()
    }
    
    func callAPIs()
    {
        getBannersDataService()
        getDealsOfTheDayAPI()
        if VIEWMANAGER.arrCategoriesData.count == 0 {
            app_del.getProductCategoriesAPI(completion: {
                for _ in VIEWMANAGER.arrCategoriesData {
                    self.arrProducts.add(NSNull())
                }
            })
        }
        else {
            for _ in VIEWMANAGER.arrCategoriesData {
                arrProducts.add(NSNull())
            }
        }
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblViewHtConstr.constant = tblViewList.contentSize.height
    }
    
    func getBannersDataService() {
        RemoteAPI().callPOSTApi(apiName: GET_BANNER_API, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.arrBannersData = responseData as! NSArray
            self.getBannerHomeScreenImages()
        }) { (errMsg) in
            // self.view.makeToast(errMsg)
        }
    }
    
    
    func getBannerHomeScreenImages()
    {
        let arr = VIEWMANAGER.arrBannersData.filter {
            (($0 as! BannerTypeModel).name.contains("Home Page Slideshow"))
        }
        
        if arr.count == 0 {
            return
        }
        
        let model = arr[0] as! BannerTypeModel
        let api = "\(GET_BANNER_IMAGES_API)\(model.bannerId!)"
        RemoteAPI().callPOSTApi(apiName: api, params: nil, apiType: .get, completion: { (responseData) in
            self.arrBanners = responseData as! NSArray
            self.bannerView.bannerData = self.arrBanners
        }) { (errMsg) in
            
        }
    }
    
    func getDealsOfTheDayAPI()
    {
        RemoteAPI().callPOSTApi(apiName:"\(DEALS_OF_THE_DAY_API)/limit/5/page/1" , params: nil, apiType: .get, completion: { (responseData) in
            self.arrDealsOfTheDay = responseData as! NSArray
            self.tblViewList.reloadData()
            self.refreshControl.endRefreshing()
        }) { (errMsg) in
            
        }
    }
   
    func getDataByCategory(categoryId:Int,index:IndexPath)
    {
        let api = "\(GET_PRODUCT_LISTING_API)/category/\(categoryId)/limit/5/page/1"
        RemoteAPI().callPOSTApi(apiName:api , params: nil, apiType: .get, completion: { (responseData) in
            self.arrProducts.replaceObject(at: index.row, with: responseData as! NSArray)
            //self.tblViewList.reloadData()
            self.tblViewList.reloadRows(at: [index], with: .none)
        }) { (errMsg) in
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : VIEWMANAGER.arrCategoriesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeProductsListCell") as! HomeProductsListCell
        cell.indexPath = indexPath
        if indexPath.section == 0 {
            cell.arrProducts = arrDealsOfTheDay
            cell.lblCategoryTitle.text = localize(string: "DEALS OF THE DAY")
        }
        else {
            
            let data = arrProducts[indexPath.row] as? NSArray
            if data == nil {
                let model = VIEWMANAGER.arrCategoriesData[indexPath.row] as! CategoriesModel
                getDataByCategory(categoryId: model.categoryId, index: indexPath)
            }
            cell.arrProducts = data
            let model = VIEWMANAGER.arrCategoriesData[indexPath.row] as! CategoriesModel
            cell.lblCategoryTitle.text = model.name
        }
        cell.onViewAllProducts { (indexPath) in
            let model = VIEWMANAGER.arrCategoriesData[indexPath.row] as! CategoriesModel
            self.pushToProductListingController(isDealsOfTheday: indexPath.section == 0, model: model)
        }
        
        cell.onSelectProducts { (productModel) in
            self.pushToProductDetailsController(productModel: productModel)
        }
        return cell
    }
    
    @objc override func btnDrawerClicked()
    {
        var parentVC = self.parent?.parent
        while parentVC != nil {
            if let drawerVC = parentVC as? KYDrawerController
            {
                drawerVC.setDrawerState(.opened, animated: true)
            }
            parentVC = parentVC?.parent
        }
    }
    
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView == baseScroll {
//            if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
//                navigationController?.setNavigationBarHidden(true, animated: true)
//            } else {
//                navigationController?.setNavigationBarHidden(false, animated: true)
//            }
//        }
//    }
//    
    func pushToProductListingController(isDealsOfTheday:Bool = false,model:CategoriesModel) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let productListingVC = storyboard.instantiateViewController(withIdentifier: "ProductListingController") as! ProductListingController
        productListingVC.selCategoryModel = model
        productListingVC.isDealsOfTheDay = isDealsOfTheday
        self.navigationController?.pushViewController(productListingVC, animated: true)
    }
    
    func pushToProductDetailsController(productModel:ProductListingModel)
    {
        let details = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ProductDetailsController") as! ProductDetailsController
        details.productModel = productModel
        self.navigationController?.pushViewController(details, animated: true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let searchVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        searchVC.modalTransitionStyle = .crossDissolve
        let navVC = UINavigationController(rootViewController: searchVC)

        self.present(navVC, animated: true, completion: nil)
        return false
    }
    
    deinit {
        tblViewList.removeObserver(self, forKeyPath: "contentSize")
    }
    
}
