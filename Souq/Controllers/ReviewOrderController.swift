//
//  ReviewOrderController.swift
//  Souq
//
//  Created by iLoma on 19/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class ReviewOrderController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var baseScroll: UIScrollView!
    @IBOutlet weak var tblViewReview: UITableView!
    @IBOutlet weak var btnCheckPayCard: UIButton!
    @IBOutlet weak var btnCheckPayCash: UIButton!
    @IBOutlet weak var viewFreeShipping: UIView!
    @IBOutlet weak var btnFreeShip: UIButton!
    @IBOutlet weak var lblFreeAmount: UILabel!
    
    @IBOutlet weak var viewFlatShipping: UIView!
    @IBOutlet weak var btnFlatShip: UIButton!
    @IBOutlet weak var lblFlatAmount: UILabel!
    
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblShippingMethod: UILabel!
    
    @IBOutlet weak var lblShippingFee: UILabel!
    @IBOutlet weak var lblCODFee: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var tblViewHeighConstr: NSLayoutConstraint!
    
    @IBOutlet weak var txtViewComment: UITextView!
    @IBOutlet weak var btnPlaceOrder: AppButton!
    
    var cartModel : ViewCartProductModel!
    var selAddrModel : AddressModel!
    var arrProductList = [Product]()
    
    var shippingMethodModel : ShippingMethodModel!
    var paymentMethodModel : PaymentMethodModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = localize(string: "REVIEW YOUR ORDER")
        
        tblViewReview.register(UINib(nibName: "OrderReviewCell", bundle: nil), forCellReuseIdentifier: "OrderReviewCell")
        tblViewReview.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        baseScroll.isHidden = true
        btnPlaceOrder.isHidden = true
        txtViewComment.layer.borderColor = UIColor.lightGray.cgColor
       // txtViewComment.toolbarPlaceholder?.append(contentsOf: localize(string: "Add comments about your order"))
        txtViewComment.placeholder = localize(string: "Add comments about your order")
        txtViewComment.placeholderColor = UIColor.lightGray

        
        if Preferences.getAppLanguage() == ENGLISH {
            btnFreeShip.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            btnFlatShip.titleEdgeInsets = btnFreeShip.titleEdgeInsets
        }
        else {
            btnFreeShip.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
            btnFlatShip.titleEdgeInsets = btnFreeShip.titleEdgeInsets

        }
        getShippingMethodsAPI()
    }
    
    func setupData()
    {
       // btnCheckPayCard.isSelected = true

        arrProductList = cartModel.products
        var tempArray = cartModel.total.components(separatedBy: "- ")
        let str = tempArray[1]
        lblSubtotal.text = str
        lblTotal.text = str
        
        lblShippingAddress.text = "\(selAddrModel.address1!), \(selAddrModel.address2!), \(selAddrModel.city!), \(selAddrModel.zone!), \(selAddrModel.country!)"
        lblMobileNo.text = VIEWMANAGER.currentUser?.telephone
        
        tblViewReview.reloadData()
    }
    
    
    func getShippingMethodsAPI()
    {
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: GET_SHIPPING_METHODS_API, params: nil, apiType: .get, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.shippingMethodModel = response as? ShippingMethodModel
            self.baseScroll.isHidden = false
            self.btnPlaceOrder.isHidden = false
            self.setupData()

            
            self.setupShippingMethodData()
            
          //  self.setShippingMethodAPI(shippingCode: self.shippingMethodModel.flat.code)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func setupShippingMethodData() {
        
        btnFreeShip.isSelected = true
        btnShippingMethodClicked(btnFlatShip)
        if shippingMethodModel.flat != nil {
            btnFlatShip.setTitle(shippingMethodModel.flat.title, for: .normal)
            lblFlatAmount.text = shippingMethodModel.flat.text
        }
        else {
            viewFlatShipping.isHidden = true
        }
        
        viewFreeShipping.isHidden = true

        if shippingMethodModel.free != nil {
            btnFreeShip.setTitle(shippingMethodModel.free.title, for: .normal)
            lblFreeAmount.text = shippingMethodModel.free.text
        }
        else {
            viewFreeShipping.isHidden = true
        }
    }
    
    func setShippingMethodAPI(shippingCode:String) {
        let param = [
            "shipping_method": shippingCode,
            "comment": ""
        ]
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: GET_SHIPPING_METHODS_API, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.btnPayCashClicked(self.btnCheckPayCash)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func getPaymentMethodsAPI()
    {
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: GET_PAYMENT_METHODS_API, params: nil, apiType: .get, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.paymentMethodModel = response as? PaymentMethodModel
            self.setPaymentMethodAPI(paymentCode: self.paymentMethodModel.cod.code)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func setPaymentMethodAPI(paymentCode:String) {
        let param = [
            "payment_method": paymentCode,
            "agree": "1",
            "comment": "string"
        ]
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: GET_PAYMENT_METHODS_API, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func confirmOrderAPI()
    {
        let param = [
            "payment_method": paymentMethodModel.cod.code,
            "agree": "1",
            "comment": txtViewComment.text!
        ]
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: PLACE_ORDER_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.saveOrderAPI()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    func saveOrderAPI()
    {
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: PLACE_ORDER_API, params: nil, apiType: .put, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            Preferences.saveCartCount(count: 0)
    
            CustomAlertView.showAlert(withTitle: localize(string: "Success"), messsage:"\(localize(string: "Your order with ID")) \(responseData) \(localize(string: "is confirmed successfully!"))", completion: {
                self.navigationController?.popToRootViewController(animated: true)
            })
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblViewHeighConstr.constant = tblViewReview.contentSize.height
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProductList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderReviewCell") as! OrderReviewCell
        let model = arrProductList[indexPath.row]
        cell.lblProductModel.text = model.model
        cell.lblProductName.text = model.name
        cell.lblProductPrice.text = model.price
        cell.lblQuantity.text = "\(localize(string: "Qty")) \(model.quantity!)"
        cell.imgViewProduct.sd_setImage(with: URL(string:VIEWMANAGER.getProperImageUrl(strUrl: model.thumb)!), placeholderImage: UIImage(named:"placeholder_img"))

        return cell
    }
    
    @IBAction func btnShippingMethodClicked(_ sender: UIButton) {
        btnFlatShip.isSelected = false
        btnFreeShip.isSelected = false
        sender.isSelected = true
        
        if btnFlatShip.isSelected {
            setShippingMethodAPI(shippingCode: shippingMethodModel.flat.code)
            lblShippingMethod.text = shippingMethodModel.flat.title
            lblShippingFee.text = shippingMethodModel.flat.text
        
            let flatCost = Float(shippingMethodModel.flat.cost)
            let total = cartModel.totalRaw + flatCost!
            lblTotal.text = "$\(total)"
        }
        else {
            setShippingMethodAPI(shippingCode: shippingMethodModel.free.code)
            lblShippingMethod.text = shippingMethodModel.free.title
            lblShippingFee.text = shippingMethodModel.free.text
            
            let freeCost = Float(shippingMethodModel.free.cost)
            let total = cartModel.totalRaw + freeCost
            lblTotal.text = "$\(total)"
        }
        
    }
    
    
    @IBAction func btnPlaceOrderClicked(_ sender: Any) {
        if !btnCheckPayCash.isSelected {
            self.view.makeToast(localize(string: "Please select payment method"))
            return
        }
        if !btnFreeShip.isSelected && !btnFlatShip.isSelected {
            self.view.makeToast(localize(string: "Please select shipping method"))
            return
        }
        
        confirmOrderAPI()
        
        
    }
    
    @IBAction func btnPayCardClicked(_ sender: UIButton) {
        sender.isSelected = true
        btnCheckPayCash.isSelected = false
    }
    
    @IBAction func btnPayCashClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            getPaymentMethodsAPI()
        }
       // btnCheckPayCard.isSelected = false
    }
    
    
    @IBAction func btnChangeAddrClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit {
        tblViewReview.removeObserver(self, forKeyPath: "contentSize")
    }
}
