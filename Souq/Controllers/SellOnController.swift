//
//  SellOnController.swift
//  Souq
//
//  Created by iLoma Technology on 24/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import MessageUI

class SellOnController: BaseViewController , MFMailComposeViewControllerDelegate {


    @IBOutlet weak var viewWebsite: UIView!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var viewGmail: UIView!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var btnMobile: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
    }
    

    @IBAction func call(_ sender: Any) {

        let url = URL(string: "telprompt://\(CONTACT_NO)")
        if url != nil{
            UIApplication.shared.open(url!)
        }
        
        
    }
    
    
    @IBAction func sendMail(_ sender: Any) {
        let email = "sell@souq366.com"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    @IBAction func btnWebsite(_ sender: Any) {
        guard let url = URL(string: "https://souq366.com/") else { return }
        UIApplication.shared.open(url)
    }
}
