//
//  AccountViewController.swift
//  Souq
//
//  Created by Pawan Ramteke on 11/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class AccountViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblViewMenus: UITableView!
    let arrMenu = ["EDIT ACCOUNT","PASSWORD","WISHLIST","ORDER HISTORY","ADDRESS BOOK"]
    let menuImgs = ["ic_account","ic_password","ic_wishlist","ic_order_history","ic_address_book"]
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         lblUserName.text = "\(localize(string: "Hello")) \(VIEWMANAGER.currentUser!.firstname!) \(VIEWMANAGER.currentUser!.lastname!)"
      //  lblUserName.text = "\(VIEWMANAGER.currentUser!.firstname!) \(VIEWMANAGER.currentUser!.lastname!)"
        lblUserEmail.text = "\(VIEWMANAGER.currentUser!.email!)"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = localize(string: "MY ACCOUNT")
        
        tblViewMenus.register(UITableViewCell.self, forCellReuseIdentifier: "CellId")
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? arrMenu.count : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellId")
            cell?.textLabel?.font = UIFont.appBoldFont(size: 16)
            cell?.textLabel?.text = localize(string: arrMenu[indexPath.row])
            cell?.accessoryType = .disclosureIndicator
            cell?.imageView?.image = UIImage(named: menuImgs[indexPath.row])
            tableView.separatorStyle = .singleLine
            return cell!
        }
        var cell = tableView.dequeueReusableCell(withIdentifier: "SignoutCell")
        if cell == nil {
            cell = SignoutCell(style: .default, reuseIdentifier: "SignoutCell")
        }
        tableView.separatorStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 60 : UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        header.backgroundColor = UIColor.white
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 {
            CustomAlertView.showAlertWithYesNo(title: "Logout", messsage: "Are you sure, you want to logout?") {
                self.logoutService()
                self.navigationController?.popToRootViewController(animated: true)
            }
            return
        }
        
        switch indexPath.row {
        case 0:
            pushToEditAccountController()
        break
        case 1:
            pushToChangePasswordController()
        break
        case 2:
            pushToMyWishlistController()
        break
        case 3:
            pushToOrderHistoryController()
        break
        case 4:
            pushToAccountAddressListController()
            break
        default:
            break
        }
    }
    
    func pushToEditAccountController()
    {
        let editProfileVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "EditAccountController") as! EditAccountController
        self.navigationController?.pushViewController(editProfileVC, animated: true)
    }
    
    func pushToChangePasswordController()
    {
        let changePassVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "ChangePasswordController") as! ChangePasswordController
        self.navigationController?.pushViewController(changePassVC, animated: true)
    }
    
    func pushToMyWishlistController()
    {
        let wishlistVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "MyWishlistController") as! MyWishlistController
        self.navigationController?.pushViewController(wishlistVC, animated: true)
    }
    
    func pushToOrderHistoryController()
    {
        let orderHistoryVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "OrderHistoryController") as! OrderHistoryController
        self.navigationController?.pushViewController(orderHistoryVC, animated: true)
    }
    func pushToAccountAddressListController()
    {
        let accountAddressVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "AccoundAddressListController") as! AddressListController
        self.navigationController?.pushViewController(accountAddressVC, animated: true)
    }
}

class SignoutCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.white
        
        let baseView = UIView()
        self.addSubview(baseView)
        baseView.enableAutoLayout()
        baseView.centerX()
        baseView.topMargin(pixels: 0)
        baseView.bottomMargin(pixels: 0)
        
        let imgView = UIImageView()
        imgView.image = UIImage(named: "ic_signout")
        imgView.contentMode = .scaleAspectFit
        baseView.addSubview(imgView)
        imgView.enableAutoLayout()
        imgView.leadingMargin(pixels: 10)
        imgView.centerY()
        imgView.fixedWidth(pixels: 30)
        imgView.fixedHeight(pixels: 30)
        
        let lblTitle = UILabel()
        lblTitle.font = UIFont.appBoldFont(size: 17)
        lblTitle.text = localize(string: "SIGN OUT")
        baseView.addSubview(lblTitle)
        lblTitle.enableAutoLayout()
        lblTitle.addToRightToView(view: imgView, pixels: 10)
        lblTitle.topMargin(pixels: 0)
        lblTitle.bottomMargin(pixels: 0)
        lblTitle.trailingMargin(pixels: 10)
        lblTitle.fixedHeight(pixels: 50)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
