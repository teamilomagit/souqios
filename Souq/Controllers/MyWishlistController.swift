//
//  MyWishlistController.swift
//  Souq
//
//  Created by Pawan Ramteke on 12/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class MyWishlistController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblViewList: UITableView!
    var arrWishlist = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = localize(string: "MY WISHLIST")
        
        
        tblViewList.register(UINib(nibName: "WishListCell", bundle: nil), forCellReuseIdentifier: "WishListCell")
        
        getWishlistAPI()
    }
    
    func getWishlistAPI()
    {
        VIEWMANAGER.showActivityIndicator(self)
        RemoteAPI().callPOSTApi(apiName: MY_WISHLIST_API, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrWishlist = NSMutableArray(array: responseData as! NSArray)
            self.tblViewList.reloadData()
            self.tblViewList.backgroundView = self.arrWishlist.count > 0 ? nil : self.tableBGView()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func tableBGView() -> UIView
    {
        let viewBG = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: tblViewList.frame.size.height))
        viewBG.backgroundColor = UIColor.clear
        
        
        let imgView = UIImageView()
        imgView.image = UIImage(named: "")
        viewBG.addSubview(imgView)
        imgView.enableAutoLayout()
        imgView.centerX()
        imgView.topMargin(pixels: 10)
        imgView.fixedWidth(pixels: SCREEN_HEIGHT * 0.4)
        imgView.fixedHeight(pixels: SCREEN_HEIGHT * 0.4)
        
        let lblEmptyTitle = UILabel()
        lblEmptyTitle.font = UIFont.appBoldFont(size: 16)
        lblEmptyTitle.text = localize(string: localize(string: "Your wishlist looks empty"))
        lblEmptyTitle.textAlignment = .center
        viewBG.addSubview(lblEmptyTitle)
        lblEmptyTitle.enableAutoLayout()
        lblEmptyTitle.leadingMargin(pixels: 10)
        lblEmptyTitle.trailingMargin(pixels: 10)
        lblEmptyTitle.belowToView(view: imgView, pixels: 10)
        
        let lblSubTitle = UILabel()
        lblSubTitle.font = UIFont.appRegularFont(size: 14)
        lblSubTitle.textColor = UIColor.lightGray
        lblSubTitle.text = localize(string: localize(string: localize(string: "")))
        lblSubTitle.textAlignment = .center
        viewBG.addSubview(lblSubTitle)
        lblSubTitle.enableAutoLayout()
        lblSubTitle.leadingMargin(pixels: 10)
        lblSubTitle.trailingMargin(pixels: 10)
        lblSubTitle.belowToView(view: lblEmptyTitle, pixels: 10)
        
        return viewBG
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrWishlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishListCell") as! WishListCell
        let model = arrWishlist[indexPath.row] as! WishlistModel
        cell.imgView?.sd_setImage(with: URL(string: model.thumb), placeholderImage: UIImage(named: "placeholder_img"))
        cell.lblModel.text = model.model
        cell.lblName.text = model.name
        cell.lblPrice.text = model.price
        cell.lblStock.text = model.stock
        cell.indexPath = indexPath
        
        cell.onAddToCartClicked { (indexPath) in
            let model = self.arrWishlist[indexPath.row] as! WishlistModel
            self.pushToProductDetailsController(model: model)
        }
        
        cell.onRemoveClicked { (indexPath) in
            CustomAlertView.showAlertWithYesNo(title: "", messsage: localize(string:"Are you sure you want to remove the product from wishlist?"), completion: {
                self.removeFromWishlistAPI(indexPath)
            })
        }
        return cell
    }
    
    
    func removeFromWishlistAPI(_ indexPath : IndexPath) {
        let model = arrWishlist[indexPath.row] as! WishlistModel
        let api = "\(MY_WISHLIST_API)/\(model.productId!)"
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: api, params: nil, apiType: .delete, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrWishlist.removeObject(at: indexPath.row)
            self.tblViewList.deleteRows(at: [indexPath], with: .fade)
            DispatchQueue.main.async{
                self.tblViewList.reloadData()
            }
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func pushToProductDetailsController(model : WishlistModel) {
        
        let productModel = ProductListingModel()
        productModel.name = model.name
        productModel.stockStatus = model.stock
        productModel.priceFormated = model.price
        productModel.productId = Int(model.productId)
        let details = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ProductDetailsController") as! ProductDetailsController
        details.productModel = productModel
        self.navigationController?.pushViewController(details, animated: true)
    }

}
