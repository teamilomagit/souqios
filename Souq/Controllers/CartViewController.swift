//
//  CartViewController.swift
//  Souq
//
//  Created by Pawan Ramteke on 11/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class CartViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var viewBaseCheckout: UIView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblItemCount: UILabel!
    @IBOutlet weak var tblViewList: UITableView!
    var model : ViewCartProductModel!
    
    var arrPList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = localize(string: "VIEWCART")
        
        viewBaseCheckout.isHidden = true
        tblViewList.register(UINib(nibName: "CartListCell", bundle: nil), forCellReuseIdentifier: "CartListCell")
        tblViewList.register(UINib(nibName: "CartTotalCell", bundle: nil), forCellReuseIdentifier: "CartTotalCell")
        tblViewList.register(UINib(nibName: "CartListButtonCell", bundle: nil), forCellReuseIdentifier: "CartListButtonCell")
        //tblViewList.estimatedRowHeight = 100
        //tblViewList.rowHeight = UITableViewAutomaticDimension
        getProductCartList()
    }
    
    func getProductCartList(isHardLoader:Bool = false)
    {
        VIEWMANAGER.showActivityIndicator(hardLoader : isHardLoader)
        RemoteAPI().callPOSTApi(apiName: GET_CART_PRODUCTS, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
        
            self.model = responseData as? ViewCartProductModel
            
            self.tblViewList.backgroundView = self.model.products.count > 0 ? nil : self.tableBGView()

            if self.model.products.count > 0 {
                self.viewBaseCheckout.isHidden = false
                self.arrPList = NSMutableArray(array: self.model.products! as NSArray)
                Preferences.saveCartCount(count: self.model.totalProductCount)
                var tempArray = self.model.total.components(separatedBy: "- ")
                let str = tempArray[1]
                self.lblTotal.text! = str
                self.lblItemCount.text! = "\(String(describing: self.model!.totalProductCount!))\(localize(string: " Item"))"
                self.tblViewList.reloadData()
            }
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            
        }
    }
    
    func updateCartItemQuantity(productKey:String, quantity:String) {
        let param = [
                        "key": productKey,
                        "quantity": quantity
                    ]
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: ADD_PRODUCT_TO_CART, params: param as [String : AnyObject], apiType: .put, completion: { (response) in
            self.getProductCartList()
        }) { (errMsg) in
            VIEWMANAGER.showToast(errMsg!)
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    func applyCouponAPI(couponCode:String) {
        let param = ["coupon":couponCode]
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: APPLY_COUPON_API, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.getProductCartList(isHardLoader: true)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func removeCouponAPI() {
        
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: APPLY_COUPON_API, params: nil, apiType: .delete, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.getProductCartList(isHardLoader: true)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func tableBGView() -> UIView
    {
        let viewBG = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: tblViewList.frame.size.height))
        viewBG.backgroundColor = UIColor.clear
        
        
        let lblEmptyTitle = UILabel()
        lblEmptyTitle.font = UIFont.appBoldFont(size: 16)
        lblEmptyTitle.text = localize(string: localize(string: "Your shopping cart is empty"))
        lblEmptyTitle.textAlignment = .center
        viewBG.addSubview(lblEmptyTitle)
        lblEmptyTitle.enableAutoLayout()
        lblEmptyTitle.leadingMargin(pixels: 10)
        lblEmptyTitle.trailingMargin(pixels: 10)
        lblEmptyTitle.topMargin(pixels: 0)
        
        let lblSubTitle = UILabel()
        lblSubTitle.font = UIFont.appRegularFont(size: 14)
        lblSubTitle.textColor = UIColor.lightGray
        lblSubTitle.text = localize(string: localize(string: localize(string: "What are you waiting for?")))
        lblSubTitle.textAlignment = .center
        viewBG.addSubview(lblSubTitle)
        lblSubTitle.enableAutoLayout()
        lblSubTitle.leadingMargin(pixels: 10)
        lblSubTitle.trailingMargin(pixels: 10)
        lblSubTitle.belowToView(view: lblEmptyTitle, pixels: 10)
        
        
        let imgView = UIImageView()
       
        if Preferences.getAppLanguage() == ENGLISH {
            imgView.image = UIImage(named: "empty_cart_logo")
        }else {
             imgView.image = UIImage(named: "empty_cart_arabic")
        }
        
        
        viewBG.addSubview(imgView)
        imgView.enableAutoLayout()
        imgView.centerX()
        imgView.belowToView(view: lblSubTitle, pixels: 20)
        imgView.fixedWidth(pixels: SCREEN_HEIGHT * 0.4)
        imgView.fixedHeight(pixels: SCREEN_HEIGHT * 0.4)
        
        let btnStartShopping = AppButton(frame: CGRect.zero)
        btnStartShopping.setTitle(localize(string: "Start Shopping"), for: .normal)
        btnStartShopping.addTarget(self, action: #selector(btnStartShoppingClicked), for: .touchUpInside)
        viewBG.addSubview(btnStartShopping)
        btnStartShopping.enableAutoLayout()
        btnStartShopping.centerX()
        btnStartShopping.belowToView(view: imgView, pixels: 40)
        btnStartShopping.fixedWidth(pixels: 200
        )
        btnStartShopping.fixedHeight(pixels: 50)
        return viewBG
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrPList.count > 0 ? 3 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? arrPList.count : (self.model == nil ? 0 : 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartListCell") as! CartListCell
            let pmodel = arrPList[indexPath.row] as! Product
            
            cell.lblProductModel.text! = pmodel.model
            cell.lblProductName.text! = pmodel.name
            cell.lblProductPrice.text! = pmodel.price
            cell.btnProductQuantity.setTitle(pmodel.quantity, for: .normal)
            cell.quantity = Int(pmodel.quantity)
            cell.lblOutofStock.isHidden = pmodel.stock
            if pmodel.isSelected {
                cell.collectionViewHtConstr.constant = 50
                cell.collectionQuantiy.reloadData()
            }
            else {
                cell.collectionViewHtConstr.constant = 0
            }
            
            cell.imgViewProduct.sd_setImage(with: URL(string:VIEWMANAGER.getProperImageUrl(strUrl: pmodel.thumb)!), placeholderImage: UIImage(named:"placeholder_img"))
            
            cell.indexpath = indexPath
            cell.onRemoveClicked(closure: { (indexPath) in
                CustomAlertView.showAlertWithYesNo(title: localize(string: "Remove!"), messsage: localize(string: "Are you sure you want to remove?"), completion: {
                    self.removeProductFromCartList(indexPath)
                })
            })
            
            
            cell.onQuantityClicked(closure: { (indexPath,quantity) in
                let pmodelTemp = self.arrPList[indexPath.row] as! Product
                pmodelTemp.quantity = "\(quantity)"
                pmodelTemp.isSelected = !pmodelTemp.isSelected
                self.arrPList.replaceObject(at: indexPath.row, with: pmodel)
                tableView.reloadRows(at: [indexPath], with: .automatic)
                self.updateCartItemQuantity(productKey:pmodelTemp.key ,quantity: pmodelTemp.quantity)
            })
            
            cell.onDropdownClicked(closure: { (indexPath) in
                let pmodelTemp = self.arrPList[indexPath.row] as! Product
                pmodelTemp.isSelected = !pmodel.isSelected
                self.arrPList.replaceObject(at: indexPath.row, with: pmodel)
                tableView.reloadRows(at: [indexPath], with: .automatic)
            })
            
           return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartTotalCell") as! CartTotalCell
            cell.arrTotals = model.totals
            cell.txtFieldCouponCard.text = ""
            cell.onApplyCouponClicked {(coupon) in 
                self.applyCouponAPI(couponCode: coupon)
            }
            cell.onRemoveCouponClicked {
                CustomAlertView.showAlertWithYesNo(title: "", messsage: localize(string: "Are you sure, you want to remove coupon?"), completion: {
                    self.removeCouponAPI()
                })
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartListButtonCell") as! CartListButtonCell
        cell.onContineShoppingClicked {
            self.navigationController?.popViewController(animated: true)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        }
        let header = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        header.backgroundColor = UIColor.white
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 : 10
    }
    
    func removeProductFromCartList(_ indexPath:IndexPath)
    {
        let pmodelTemp = self.arrPList[indexPath.row] as! Product

        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: REMOVE_ITEM_FROM_CART, params: ["key":pmodelTemp.key as AnyObject], apiType: .delete, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrPList.removeObject(at: indexPath.row)
            self.tblViewList.reloadData()
            
            if self.arrPList.count == 0 {
                self.viewBaseCheckout.isHidden = true
                self.tblViewList.backgroundView = self.arrPList.count > 0 ? nil : self.tableBGView()
            }
            var cartCount = Preferences.getCartCount()!
            cartCount = cartCount - Int(pmodelTemp.quantity)!
            Preferences.saveCartCount(count: cartCount)
            
            self.getProductCartList()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()

        }
    }
    
    func setShowHideQuantity(cell:CartListCell,model:Product,withAnimation:Bool = false){

        if withAnimation {
            UICollectionView.animate(withDuration: 0.5) {
                cell.collectionViewHtConstr.constant = model.isSelected ?  55 : 0
                cell.layoutIfNeeded()
            }
        }
        else {
            cell.collectionViewHtConstr.constant = model.isSelected ?  55 : 0
            cell.layoutIfNeeded()
        }
    }
    
    @objc func btnStartShoppingClicked() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnCheckoutClicked(_ sender: Any) {
        if isProductOutOfStock()  {
            CustomAlertView.showAlert(withTitle: "", messsage: localize(string:  "\(arrPList.count == 1 ? "The" : "One of the") item in your cart is out of stock. Please remove the item to continue with checkout."), completion: nil)
            return
        }
        if VIEWMANAGER.currentUser == nil {
            pushToGuestLoginController()
        }
        else {
            pushToSelectAddressController()
        }
    }
    
    func pushToGuestLoginController() {
        let guestLoginVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "GuestLoginController") as! GuestLoginController
        self.navigationController?.pushViewController(guestLoginVC, animated: true)
        guestLoginVC.onGuestRegistrationSuccess { (addressModel) in
            self.pushToReviewOrderController(addrModel: addressModel)
        }
    }
    
    func pushToSelectAddressController()
    {
        let selectAddrVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "SelectAddressController") as! SelectAddressController
        selectAddrVC.cartModel = model
        self.navigationController?.pushViewController(selectAddrVC, animated: true)
    }
    
    func pushToReviewOrderController(addrModel : AddressModel) {
        let reviewOrderVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ReviewOrderController") as! ReviewOrderController
        reviewOrderVC.cartModel = model
        reviewOrderVC.selAddrModel = addrModel
        self.navigationController?.pushViewController(reviewOrderVC, animated: true)
    }
    
    func isProductOutOfStock() -> Bool {
        let arr = arrPList.filter {($0 as! Product).stock == false }
        return arr.count > 0
    }
}
