//
//  EditAccountController.swift
//  Souq
//
//  Created by Pawan Ramteke on 12/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class EditAccountController: BaseViewController,UITextFieldDelegate {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldMobile: CustomTextField!
    @IBOutlet weak var txtFieldGender: CustomTextField!
    var model : AccountDetailModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = localize(string: "EDIT ACCOUNT")
        txtFieldMobile.setTextFieldTag(tag: .TAG_MOBILE)
        
        lblUserName.text = "\(VIEWMANAGER.currentUser!.firstname!) \(VIEWMANAGER.currentUser!.lastname!)"
        lblUserEmail.text = "\(VIEWMANAGER.currentUser!.email!)"
        txtFieldGender.setTextFieldTag(tag: .TAG_ACTION_SHEET)
        txtFieldGender.actionSheetData = [localize(string: "Male"),localize(string: "Female")]
        getAccountDataAPI()
        
     //   txtFieldMobile.delegate = self
    }
    
    
    
    func getAccountDataAPI()
    {
    
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: UPDATE_ACCOUNT, params: nil, apiType: .get, completion: { (response) in
            self.model = response as? AccountDetailModel
            self.txtFieldFirstName.text = self.model.firstname ?? VIEWMANAGER.currentUser!.firstname
            self.txtFieldLastName.text = self.model.lastname ?? VIEWMANAGER.currentUser!.lastname
            self.txtFieldEmail.text = self.model.email ?? VIEWMANAGER.currentUser!.email
            self.txtFieldMobile.text = self.model.telephone ?? VIEWMANAGER.currentUser!.telephone
            self.lblUserName.text = "\(self.model.firstname!) \(self.model.lastname!)"
            self.lblUserEmail.text = "\(self.model.email!)"
            VIEWMANAGER.currentUser?.firstname = self.model.firstname
            VIEWMANAGER.currentUser?.lastname = self.model.lastname
            VIEWMANAGER.currentUser?.email = self.model.email
            if self.model.accountCustomField != nil {
                let genderModel = self.model.accountCustomField as AccountCustomField
                self.txtFieldGender.text = genderModel.one == "1" ? "Male" : "Female"
            }
           
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFieldMobile{
            let allowingChars = "0123456789"
            let numberOnly = NSCharacterSet.init(charactersIn: allowingChars).inverted
            let validString = string.rangeOfCharacter(from: numberOnly) == nil
            if validString{
                return validString
            }
            else{
                self.view.makeToast("Please enter only digits.")
                return false
            }
            
        }
        return true
    }
    
    func editAccontDataAPI()
    {
        var strGender = String()
        if txtFieldGender.text == "Male"{
            strGender = "1"
        }else{
            strGender = "2"
        }

        let customFields = ["account":["1":strGender]]
        let param = [
            "firstname": txtFieldFirstName.text!,
            "lastname": txtFieldLastName.text!,
            "email": txtFieldEmail.text!,
            "telephone": txtFieldMobile.text!,
            "custom_field": customFields
            ] as [String : Any]
        print(param)
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: UPDATE_ACCOUNT, params: param as [String : AnyObject], apiType: .put, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.view.makeToast(localize(string:"Account updated successfully"))
            self.getAccountDataAPI()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    @IBAction func btnUpdateClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            editAccontDataAPI()
        }
    }
    
    
    func validate()-> Bool {
        if txtFieldFirstName.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter your first name"))
            return false
        }
       else if txtFieldFirstName.text!.count > 32 {
            self.view.makeToast(localize(string:"First Name must be between 1 and 32 characters!"))
            return false
        }
        
        if txtFieldLastName.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter your last name"))
            return false
        }
        
        if txtFieldLastName.text!.count > 32 {
            self.view.makeToast(localize(string:"Last Name must be between 1 and 32 characters!"))
            return false
        }
        
        
        if txtFieldEmail.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter your email"))
            return false
        }
        if !txtFieldEmail.text!.isValidEmail() {
            VIEWMANAGER.showToast(localize(string: "Please enter valid email address"))
            return false
        }
        if txtFieldMobile.text!.count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter your mobile number"))
            return false
        }
        else if txtFieldMobile.text!.count < 10 {
            self.view.makeToast(localize(string:"Please enter valid mobile"))
            return false
        }
        if txtFieldGender.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter gender"))
            return false
        }

        return true
    }
}
