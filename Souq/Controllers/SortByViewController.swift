//
//  SortByViewController.swift
//  Souq
//
//  Created by Pawan Ramteke on 07/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class SortByViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    var sortClosure : ((String,Int)->())?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblViewList: UITableView!
    var arrData = ["DEFAULT","NAME (A-Z)","NAME (Z-A)","PRICE (LOW > HIGH)","PRICE (HIGH > LOW)","RATING (HIGHEST)","RATING (LOWEST)","MODEL (A-Z)","MODEL (Z-A)"]
    var arrOrder = ["","name&order=asc","name&order=desc","price&order=asc","price&order=desc","rating&order=asc","rating&order=desc","model&order=asc","model&order=desc"]
    var selIdx : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.textColor = UIColor.appThemeColor
        lblTitle.text = localize(string: arrData[selIdx])
        
        tblViewList.register(UINib(nibName: "SortTableCell", bundle: nil), forCellReuseIdentifier: "SortTableCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortTableCell") as! SortTableCell
        cell.indexPath = indexPath
        cell.lblTitle.text = localize(string: arrData[indexPath.row])
        cell.btnRadio.isSelected = selIdx == indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selIdx = indexPath.row
        tableView.reloadData()
    }
    
    @IBAction func btnApplyClicked(_ sender: Any) {
        if sortClosure != nil {
            sortClosure!(arrOrder[selIdx],selIdx)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func onSortSelection(closure:@escaping (String,Int)->()) {
        sortClosure = closure
    }
}

