//
//  GuestLoginController.swift
//  Souq
//
//  Created by Pawan Ramteke on 18/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import GoogleSignIn
import FacebookLogin
import FacebookCore
import FBSDKCoreKit

class GuestLoginController: BaseViewController,GIDSignInDelegate,GIDSignInUIDelegate {

    var RegistrationClosure : ((AddressModel)->())?

    @IBOutlet weak var btnRegisterAcc: UIButton!
    @IBOutlet weak var btnGuestCheckout: UIButton!

    @IBOutlet weak var lblCheckoutOptions: UILabel!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnGuestCheckout.isSelected = true
        lblCheckoutOptions.text = localize(string: "Checkout options")
        
        if Preferences.getAppLanguage() == ENGLISH {
            btnRegisterAcc.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            btnRegisterAcc.contentHorizontalAlignment = .left
            btnGuestCheckout.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            btnGuestCheckout.contentHorizontalAlignment = .left
        }
        else{
            btnRegisterAcc.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
            btnRegisterAcc.contentHorizontalAlignment = .right
            btnGuestCheckout.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
            btnGuestCheckout.contentHorizontalAlignment = .right
        }

    }
    
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            loginService()
        }
    }
    
    func loginService() {
        let param = ["email": txtFieldEmail.text,
                     "password": txtFieldPassword.text,
                     ] as [String : AnyObject]
        VIEWMANAGER.showActivityIndicator(hardLoader: true)
        RemoteAPI().callPOSTApi(apiName:LOGIN_URL, params: param, completion: { (responseData) in
            let model: LoginModel? = responseData as? LoginModel
            VIEWMANAGER.currentUser = model
            Preferences.saveCartCount(count: model!.cartCountProducts)
            VIEWMANAGER.hideActivityIndicator()
            self.navigationController?.popViewController(animated: true)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    @IBAction func btnGoogleLoginClicked(_ sender: Any) {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error{
            print("We have error sign in user == \(error.localizedDescription)")
            VIEWMANAGER.showToast(error.localizedDescription)
        }else{
            if let gmailUser = user {
                let fullName = user.profile.name
                let components = fullName!.components(separatedBy: " ")
                let param = ["access_token": user.authentication.idToken,
                         "email": gmailUser.profile.email,
                         "provider": "google",
                         "firstname": components.first!,
                         "lastname": components.last!,
                         ] as [String : AnyObject]
                self.socialLoginService(param: param)
            }
        }
    }
    
    
    @IBAction func btnFacebookLoginClicked(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.loginBehavior = .native
        loginManager.logIn(readPermissions: [.publicProfile,.email], viewController: self) { (result) in
            switch result {
            case .failed(let error):
                VIEWMANAGER.showToast(error.localizedDescription)
            case .cancelled:
                print("User cancelled login.")
            case .success(_, _, let accessToken):
                let strFbAccessToken = accessToken.authenticationToken
                self.getFBUserInfo(accessToken: strFbAccessToken)
            }
        }
    }
    
    func getFBUserInfo(accessToken:String) {
        
        VIEWMANAGER.showActivityIndicator(hardLoader: true)
        let request = GraphRequest(graphPath: "me", parameters: ["fields":"email,first_name,last_name"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: FacebookCore.GraphAPIVersion.defaultVersion)
        request.start { (response, result) in
            VIEWMANAGER.hideActivityIndicator()
            switch result {
            case .success(let value):
                let email = (value.dictionaryValue!["email"] as! String)
                let fName = (value.dictionaryValue!["first_name"] as! String)
                let lName = (value.dictionaryValue!["last_name"] as! String)
                
                let param = ["access_token": accessToken,
                             "email": email,
                             "provider": "facebook",
                             "firstname": fName,
                             "lastname": lName,
                             ] as [String : AnyObject]
                self.socialLoginService(param: param)
            case .failed(let error):
                print(error)
                VIEWMANAGER.showToast(error.localizedDescription)
            }
        }
    }

    
    func socialLoginService(param : [String : Any]) {
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName:SOCIAL_LOGIN_URL, params: param as [String : AnyObject], completion: { (responseData) in
            let model: LoginModel? = responseData as? LoginModel
            VIEWMANAGER.currentUser = model
            VIEWMANAGER.hideActivityIndicator()
            self.navigationController?.popViewController(animated: true)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    @IBAction func btnRadioClicked(_ sender: UIButton) {
        btnRegisterAcc.isSelected = false
        btnGuestCheckout.isSelected = false
        sender.isSelected = true
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        btnRegisterAcc.isSelected ? pushToRegistrationVC() : pushToGuestRegistraionVC()
    }
    
    func pushToRegistrationVC() {
        let signUP = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "SignUpController") as! SignUpController
        self.present(signUP, animated: true, completion: nil)
        signUP.onSignUpSuccess {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func pushToGuestRegistraionVC() {
        let guestRegVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "GuestRegistrationController") as! GuestRegistrationController
        guestRegVC.modalTransitionStyle = .crossDissolve
        self.present(guestRegVC, animated: true, completion: nil)
        guestRegVC.onGuestRegistrationSuccess { (addressModel) in
            self.navigationController?.popViewController(animated: false)
            if self.RegistrationClosure != nil {
                self.RegistrationClosure!(addressModel)
            }
        }

    }
    
    
    func validate() -> Bool
    {
        if txtFieldEmail.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter your email id."))
            return false
        }
        if !txtFieldEmail.text!.isValidEmail() {
            VIEWMANAGER.showToast(localize(string: "Please enter valid email id."))
            return false
        }
        if txtFieldPassword.text!.count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter your password."))
            return false
        }
        return true
    }
    
    func onGuestRegistrationSuccess(closure : @escaping (AddressModel)->()) {
        RegistrationClosure = closure
    }
}
