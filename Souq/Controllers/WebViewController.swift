//
//  WebViewController.swift
//  Souq
//
//  Created by Pawan Ramteke on 14/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class WebViewController: BaseViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
       // webView.loadRequest(URLRequest(url: URL(string: "www.google.com")!) )

        
        //let url = URL(string: "https://www.google.co.in")
        //let requestObj = URLRequest(url: url! as URL)
       // webView.loadRequest(requestObj)
        
        getInformationTypes()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
    }
    
    func getID(name: String,arrlist: NSMutableArray){
        var matchingId  = 0
        
        for model in arrlist {
            let  tempModel = model as! InformationType
            if tempModel.title == localize(string: name){
                matchingId = tempModel.id
                break
            }
        }
        
        if matchingId != 0{
            
            getInformationDetail(infomationId: matchingId)
        }
        
        
    }
    
    func getInformationTypes(){
        
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: GET_INFORMATION_TYPES_API, params: nil, apiType: .get, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
          
            print(response)

            self.getID(name: self.title ?? "", arrlist: response as! NSMutableArray)
            //   self.setShippingMethodAPI(shippingCode: self.shippingMethodModel.free.code)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func getInformationDetail(infomationId : Int){
        
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: "\(GET_INFORMATION_TYPES_API)/\(infomationId)", params: nil, apiType: .get, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            
            print(response)
            
            let model = response as! InformationType
            
            
            
            var finalString = model.descriptionField
            
            finalString = finalString!.replacingOccurrences(of: "&lt;", with: "<", options: .literal, range: nil)
            finalString = finalString!.replacingOccurrences(of: "&gt;", with: ">", options: .literal, range: nil)
            finalString = finalString!.replacingOccurrences(of: "&amp;", with: "&", options: .literal, range: nil)
            
            let htmlBody = "<html><body><h3>\(model.title!)</h3>\(finalString!)</body></html>"

            
//            String replaced = url.replace("&lt;","<");
//            String replaced2 = replaced.replace("&gt;", ">");
//            replaced2 = replaced2.replace("&amp;", "&");
//            String newBody = "<html><body>" +  "<h3>" + screen_title + "</h3>" +  replaced2 + "</body></html>";
//            mWebView.loadDataWithBaseURL(null, newBody, "text/html", "utf-8", null);

            
            
            self.webView.loadHTMLString(htmlBody , baseURL: nil)

            
            //   self.setShippingMethodAPI(shippingCode: self.shippingMethodModel.free.code)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }

}
