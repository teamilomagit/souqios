//
//  OrderHistoryDetailsController.swift
//  Souq
//
//  Created by iLoma on 11/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class OrderHistoryDetailsController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
   
    var cancelClosure : (()->())?
    @IBOutlet weak var baseScroll: UIScrollView!

    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblPlaceDate: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var tblviewOrderDetail: UITableView!
    @IBOutlet weak var lblPaymentMethod: UILabel!
    
    @IBOutlet weak var viewSubTotal: UIView!
    @IBOutlet weak var lblSubTotalTitle: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    
    @IBOutlet weak var viewShipping: UIView!
    @IBOutlet weak var lblShippingTitle: UILabel!
    @IBOutlet weak var lblShipping: UILabel!
    
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblTotal: UILabel!

    @IBOutlet weak var viewOrdered: UIView!
    @IBOutlet weak var viewProcessing: UIView!
    @IBOutlet weak var viewDelivered: UIView!
    @IBOutlet weak var viewShipped: UIView!

    @IBOutlet weak var tblViewHeighConstr: NSLayoutConstraint!
    var arrProduct = NSArray()
    
    var model : OrderHistoryDetailsModel!
    var strOrderId : String!

    override func viewDidLoad() {
        super.viewDidLoad()

        tblviewOrderDetail.register(UINib(nibName: "OrderHHistoryDetailCell", bundle: nil), forCellReuseIdentifier: "OrderHHistoryDetailCell")
        tblviewOrderDetail.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        baseScroll.isHidden = true
        getOrderHistoryDetailService(strOrderId: strOrderId)
    }
    
    func getOrderHistoryDetailService(strOrderId:String)
    {
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        let url = ORDER_HISTORY_DETAILS_API + strOrderId
        RemoteAPI().callPOSTApi(apiName: url, params: nil, apiType: .get, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.baseScroll.isHidden = false
            self.model = response as? OrderHistoryDetailsModel
            self.setupDetailsData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func setupDetailsData()
    {
        lblOrderNo.text = "\(localize(string: "ORDER")) \(model.orderId!)"
        lblPlaceDate.text = "\(localize(string: "Placed on")) \(formattedDate(strDate: model.dateAdded!))"
        
        lblShippingAddress.text = "\(self.model.shippingAddress1!) \(self.model.shippingAddress2!) \(self.model.shippingCity!) \(self.model.shippingZone!) \(self.model.shippingCountry!)-\(self.model.shippingPostcode!)"
        
        lblPhoneNumber.text! = self.model.telephone
        lblPaymentMethod.text! = self.model.paymentMethod
        
        if model.totals.count == 3 {
            viewShipping.isHidden = false
            let subTotal = model.totals[0]
            lblSubTotalTitle.text = subTotal.title
            lblSubTotal.text = "$\(subTotal.strValue!)"
            
            let shipping = model.totals[1]
            lblShippingTitle.text = shipping.title
            lblShipping.text = "$\(shipping.strValue!)"
            
            let total = model.totals[2]
            lblTotalTitle.text = total.title
            lblTotal.text = "$\(total.strValue!)"
        }
        else if model.totals.count == 2 {
            viewShipping.isHidden = true
            
            let subTotal = model.totals[0]
            lblSubTotalTitle.text = subTotal.title
            lblSubTotal.text = "$\(subTotal.strValue!)"
            
            let total = model.totals[1]
            lblTotalTitle.text = total.title
            lblTotal.text = "$\(total.strValue!)"
        }
        self.arrProduct = self.model.products! as NSArray
        self.tblviewOrderDetail.reloadData()
        
        if model.orderStatusId == "1" {
            viewOrdered.backgroundColor = UIColor.statusGreenColor
        }
        else if model.orderStatusId == "2" {
            viewOrdered.backgroundColor = UIColor.statusGreenColor
            viewProcessing.backgroundColor = UIColor.statusGreenColor
        }
        else if model.orderStatusId == "3" {
            viewOrdered.backgroundColor = UIColor.statusGreenColor
            viewProcessing.backgroundColor = UIColor.statusGreenColor
            viewShipped.backgroundColor = UIColor.statusGreenColor
        }
        else if model.orderStatusId == "5" {
            viewOrdered.backgroundColor = UIColor.statusGreenColor
            viewProcessing.backgroundColor = UIColor.statusGreenColor
            viewShipped.backgroundColor = UIColor.statusGreenColor
            viewDelivered.backgroundColor = UIColor.statusGreenColor
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProduct.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHHistoryDetailCell") as! OrderHHistoryDetailCell
        cell.indexPath = indexPath
        let pModel = arrProduct[indexPath.row] as! Product
        cell.lblName.text = pModel.name
        cell.lblPrice.text = pModel.price
        cell.imgView!.sd_setImage(with: URL(string:VIEWMANAGER.getProperImageUrl(strUrl: pModel.image)!), placeholderImage: UIImage(named:"placeholder_img"))
        cell.onWriteReview { (indexPath) in
            let model = self.arrProduct[indexPath.row] as! Product
            self.showRatingView(productId : model.productId)
        }

        return cell
    }
    
    
    @IBAction func btnCancelOrderClicked(_ sender: Any) {
        let cancelOrderView = Bundle.main.loadNibNamed("CancelOrderView", owner: self, options: nil)?[0] as! CancelOrderView
        self.view.addSubview(cancelOrderView)
        cancelOrderView.orderId = model.orderId
        
        cancelOrderView.enableAutoLayout()
        cancelOrderView.leadingMargin(pixels: 0)
        cancelOrderView.trailingMargin(pixels: 0)
        cancelOrderView.topMargin(pixels: 0)
        cancelOrderView.bottomMargin(pixels: 0)
        
        cancelOrderView.onCancelOrderSuccess {
            if self.cancelClosure != nil {
                self.cancelClosure!()
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblViewHeighConstr.constant = tblviewOrderDetail.contentSize.height
    }
    
    func showRatingView(productId:String)
    {
        let reviewView = Bundle.main.loadNibNamed("ReviewProductView", owner: self, options: nil)?[0] as! ReviewProductView
        reviewView.productID = productId
        self.view.addSubview(reviewView)
        reviewView.enableAutoLayout()
        reviewView.leadingMargin(pixels: 0)
        reviewView.trailingMargin(pixels: 0)
        reviewView.topMargin(pixels: 0)
        reviewView.bottomMargin(pixels: 0)
    }
    
    func formattedDate(strDate:String) -> String {
        let formatter = DateFormatter()
        if Preferences.getAppLanguage() == ENGLISH {
            formatter.dateFormat = "dd/MM/yyyy"
        }else{
            formatter.dateFormat = "yyyy/MM/dd"
        }
        let date = formatter.date(from:strDate)!
        formatter.dateFormat = "MMM dd, yyyy"
        let formDate = formatter.string(from:date)
        return formDate
    }
    
    func onCancelOrderSuccess(closure:@escaping ()->()) {
        cancelClosure = closure
    }

    deinit {
        tblviewOrderDetail.removeObserver(self, forKeyPath: "contentSize")
    }
  
}
