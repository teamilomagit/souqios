//
//  SearchViewController.swift
//  Souq
//
//  Created by Pawan Ramteke on 14/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var viewNavigation: UIView!
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtFieldSearch: UITextField!
    @IBOutlet weak var tblViewList: UITableView!
    var arrSearchList = NSArray()
    var timer : Timer!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        viewNavigation.backgroundColor = UIColor.appThemeColor
        viewSearch.backgroundColor = UIColor.appThemeColor

        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
        imgView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)
        imgView.contentMode = .scaleAspectFit
        imgView.tintColor = UIColor.gray
        
        if Preferences.getAppLanguage() == ENGLISH {
            txtFieldSearch.leftViewMode = .always
            txtFieldSearch.leftView = imgView
        }
        else {
            txtFieldSearch.rightViewMode = .always
            txtFieldSearch.rightView = imgView
        }
        txtFieldSearch.becomeFirstResponder()
        txtFieldSearch.placeholder = localize(string: "What are you looking for?")

        if Preferences.getAppLanguage() != ENGLISH {
            btnBack.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        
        tblViewList.keyboardDismissMode = .interactive
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableCell") as! SearchTableCell
        let model = arrSearchList[indexPath.row] as! ProductListingModel
        cell.lblSearchVal.text = model.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrSearchList[indexPath.row] as! ProductListingModel
        let details = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ProductDetailsController") as! ProductDetailsController
        details.productModel = model
        self.navigationController?.pushViewController(details, animated: true)
    }

    @IBAction func btnBackClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let nsString = textField.text! as NSString
        let resultingString = nsString.replacingCharacters(in: range, with: string)
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        if resultingString.count > 0 {
            timer = Timer.scheduledTimer(timeInterval: 0.7, target: self, selector: #selector(searchForKeyword(timer:)), userInfo: resultingString, repeats: false)
        }
        else {
             arrSearchList = NSArray()
            tblViewList.reloadData()
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    @objc func searchForKeyword(timer:Timer) {
        let keyword = timer.userInfo as! String
        print(keyword)
        searchProductAPI(searchStr: keyword)
    }
    
    func searchProductAPI(searchStr:String) {
        let api = "\(SEARCH_PRODUCT_API)\(searchStr)"
        loader.startAnimating()
        RemoteAPI().callPOSTApi(apiName: api, params: nil, apiType: .get, completion: { (responseData) in
            self.loader.stopAnimating()
            self.arrSearchList = responseData as! NSArray
             self.tblViewList.backgroundView = self.arrSearchList.count > 0 ? nil : self.tableBGView()
            self.tblViewList.reloadData()
        }) { (errMsg) in
            self.loader.stopAnimating()
        }
    }
    func tableBGView() -> UIView
    {
        let bgView = UIView(frame: self.view.frame)
        
        let lblEmptyTitle = UILabel(frame: CGRect(x: 0, y: 20, width: SCREEN_WIDTH, height: 40))
        lblEmptyTitle.font = UIFont.appBoldFont(size: 16)
        lblEmptyTitle.text = localize(string: localize(string: "No data found"))
        lblEmptyTitle.textAlignment = .center
        bgView.addSubview(lblEmptyTitle)
        
        return bgView
    }
}




class SearchTableCell: UITableViewCell {
    
    @IBOutlet weak var lblSearchVal: UILabel!
    
    @IBOutlet weak var btnSearch: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
