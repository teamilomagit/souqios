//
//  ImageViewerController.swift
//  Souq
//
//  Created by Pawan Ramteke on 13/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import ASJCollectionViewFillLayout
class ImageViewerController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var _collectionView: UICollectionView!
    
    @IBOutlet weak var _collectionViewList: UICollectionView!
    var imgData : NSMutableArray!
    var selIdx : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 80, height: 100)
        layout.scrollDirection = .horizontal
        _collectionView.setCollectionViewLayout(layout, animated: true)
        _collectionView.register(ImageViewerCell.self, forCellWithReuseIdentifier: "CellId")
        
        let layoutList = ASJCollectionViewFillLayout()
        layoutList.itemSpacing = 0
        layoutList.numberOfItemsInSide = 1
        layoutList.stretchesLastItems = false
        layoutList.itemLength = SCREEN_WIDTH
        layoutList.direction = .horizontal
        _collectionViewList.setCollectionViewLayout(layoutList, animated: true)
        _collectionViewList.register(ImageViewerListCell.self, forCellWithReuseIdentifier: "CellListId")
        _collectionViewList.isPagingEnabled = true
        
        resetData()
        let model = imgData[selIdx] as! BannerImagesModel
        model.isSelected = true
        imgData.replaceObject(at: selIdx, with: model)
        _collectionViewList.setContentOffset(CGPoint(x:SCREEN_WIDTH * CGFloat(selIdx),y:0), animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == _collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellId", for: indexPath) as! ImageViewerCell
            let model = imgData[indexPath.row] as! BannerImagesModel
            cell.imgView.sd_setImage(with: URL(string:model.image.replacingOccurrences(of: " ", with: "%20")), placeholderImage: UIImage(named:""))
            cell.layer.borderColor = model.isSelected ? UIColor.appThemeColor.cgColor : UIColor.clear.cgColor
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellListId", for: indexPath) as! ImageViewerListCell
        let model = imgData[indexPath.row] as! BannerImagesModel
        cell.imgView.sd_setImage(with: URL(string:model.image.replacingOccurrences(of: " ", with: "%20")), placeholderImage: UIImage(named:""))
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == _collectionView {
            _collectionViewList.setContentOffset(CGPoint(x:SCREEN_WIDTH * CGFloat(indexPath.row),y:0), animated: true)
            resetData()
            let model = imgData[indexPath.row] as! BannerImagesModel
            model.isSelected = true
            imgData.replaceObject(at: indexPath.row, with: model)
            _collectionView.reloadData()
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == _collectionViewList {
            let width = scrollView.frame.size.width
            let page : Int = Int((scrollView.contentOffset.x + (0.5 * width)) / width)
            resetData()
            let model = imgData[page] as! BannerImagesModel
            model.isSelected = true
            imgData.replaceObject(at: page, with: model)
            _collectionView.reloadData()
        }
    }
    
    func resetData()
    {
        for i in 0 ..< imgData.count {
            let model = imgData[i] as! BannerImagesModel
            model.isSelected = false
            imgData.replaceObject(at: i, with: model)
        }
    }
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

class ImageViewerCell: UICollectionViewCell {
    var imgView : UIImageView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.backgroundColor = UIColor.white
        self.layer.borderWidth = 1.0
        imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        self.addSubview(imgView)
        imgView.enableAutoLayout()
        imgView.leadingMargin(pixels: 0)
        imgView.trailingMargin(pixels: 0)
        imgView.topMargin(pixels: 0)
        imgView.bottomMargin(pixels: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ImageViewerListCell: UICollectionViewCell {
    var imgView : UIImageView!
    var scrollView : UIScrollView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.borderWidth = 1.0
        
        imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.isUserInteractionEnabled = true
        self.addSubview(imgView)
        imgView.enableAutoLayout()
        imgView.leadingMargin(pixels: 0)
        imgView.trailingMargin(pixels: 0)
        imgView.topMargin(pixels: 0)
        imgView.bottomMargin(pixels: 0)
        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinch(sender:)))
        imgView.addGestureRecognizer(pinch)

       // let doubleTap = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(_:)))
      //  doubleTap.numberOfTapsRequired = 2
    }
    
    @objc func pinch(sender:UIPinchGestureRecognizer) {
        if sender.state == .began || sender.state == .changed {
            let currentScale = self.imgView.frame.size.width / self.imgView.bounds.size.width
            if currentScale > 0{
                let newScale = currentScale*sender.scale
                let transform = CGAffineTransform(scaleX: newScale, y: newScale)
                self.imgView.transform = transform
                sender.scale = 1
            }
        }
    }
    
//    @objc func handleDoubleTap(_ gestureRecognizer:UIGestureRecognizer)
//    {
//        var newScale = Float(imgView.zoomScale * ZOOM_STEP)
//
//        if CGFloat(newScale) > imageScrollView.maximumZoomScale {
//            newScale = Float(imageScrollView.minimumZoomScale)
//            var zoomR: CGRect = zoomRect(forScale: CGFloat(newScale), withCenter: gestureRecognizer.location(in: gestureRecognizer.view))
//
//            imageScrollView.zoom(to: zoomR, animated: true)
//        } else {
//
//            newScale = Float(imageScrollView.maximumZoomScale)
//            let zoomR: CGRect = zoomRect(forScale: CGFloat(newScale), withCenter: gestureRecognizer.location(in: gestureRecognizer.view))
//
//            imageScrollView.zoom(to: zoomR, animated: true)
//        }
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

