//
//  FiltersViewController.swift
//  Souq
//
//  Created by Pawan Ramteke on 07/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class FiltersViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    var filterClosure : ((String)->())?
    var strTitle : String!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblViewList: UITableView!
    var arrFilters = NSMutableArray()
    var selOptionIds : String = ""
    @IBOutlet weak var btnReset: AppButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.textColor = UIColor.appThemeColor
        lblTitle.text = strTitle
        
        btnReset.setTitleColor(UIColor.appThemeColor, for: .normal)
        btnReset.backgroundColor = UIColor.clear
        btnReset.layer.borderWidth = 1.0
        btnReset.layer.borderColor = UIColor.appThemeColor.cgColor
        tblViewList.register(UINib(nibName: "FiltersTableCell", bundle: nil), forCellReuseIdentifier: "FiltersTableCell")
        
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FiltersTableCell") as! FiltersTableCell
        let model = arrFilters[indexPath.row] as! FilterGroup
        cell.lblTitle.text = model.name
        cell.lblSubTitle.text = selOptionIds.count > 0 ? getSubTitle(model: model) : ""
        return cell
    }
    
    func getSubTitle(model:FilterGroup) -> String {
        var str = ""
        let arrIds = selOptionIds.components(separatedBy: ",")
        for ids in arrIds {
            let arrData = model.filterOptions.filter{$0.filterId == ids }
            if arrData.count > 0 {
                let mod = arrData[0]
                if str == "" {
                    str = mod.name
                }
                else {
                    str = str + "," + mod.name
                }
            }
        }
        return str
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrFilters[indexPath.row] as! FilterGroup
        let filterOptionsVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "FilterOptionsController") as! FilterOptionsController
        filterOptionsVC.arrFilterOptions = NSMutableArray(array: model.filterOptions)
        filterOptionsVC.arrSelIds = NSMutableArray(array: selOptionIds.count > 0 ? selOptionIds.components(separatedBy: ",") : [])
        self.navigationController?.pushViewController(filterOptionsVC, animated: true)
        
        filterOptionsVC.onOptionsSelection { (selIds) in
            self.selOptionIds = selIds
            self.tblViewList.reloadData()
        }
    }
    
    
    @IBAction func btnResetClicked(_ sender: Any) {
        self.selOptionIds = ""
        if filterClosure != nil {
            filterClosure!(self.selOptionIds)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnApplyClicked(_ sender: Any) {
        if filterClosure != nil {
            filterClosure!(self.selOptionIds)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func onApplyFilter(closure:@escaping (String)->()) {
        filterClosure = closure
    }
}
