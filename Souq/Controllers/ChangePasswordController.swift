//
//  ChangePasswordController.swift
//  Souq
//
//  Created by Pawan Ramteke on 12/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class ChangePasswordController: BaseViewController {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!

    @IBOutlet weak var txtFieldConfirmPass: CustomTextField!
    @IBOutlet weak var txtFieldNewPass: CustomTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = localize(string: "PASSWORD")
        lblUserName.text = "\(VIEWMANAGER.currentUser!.firstname!) \(VIEWMANAGER.currentUser!.lastname!)"
        lblUserEmail.text = "\(VIEWMANAGER.currentUser!.email!)"
        
        txtFieldConfirmPass.setTextFieldTag(tag: .TAG_PASSWORD)
        txtFieldNewPass.setTextFieldTag(tag: .TAG_PASSWORD)
    }
    
    func changePasswordAPI()
    {
        let param = [
                        "password": txtFieldNewPass.text!,
                        "confirm": txtFieldConfirmPass.text!
                    ]
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: CHANGE_PASSWORD_API, params: param as [String : AnyObject], apiType: .put, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(localize(string: "Password changed successfully"))
            self.navigationController?.popToRootViewController(animated: true)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    @IBAction func btnChangePasswordClicked(_ sender: Any) {
        if validate() {
            changePasswordAPI()
        }
    }
    
    func validate() -> Bool {
        if txtFieldNewPass.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter new password"))
            return false
        }
        else if txtFieldNewPass.text!.count < 4 {
            self.view.makeToast(localize(string:"Password must be between 4 to 20 characters!"))
            return false
        }
        
        
       
        if txtFieldConfirmPass.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter confirm password"))
            return false
        }
    
        return true
    }
}
