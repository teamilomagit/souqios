//
//  ReviewsViewController.swift
//  Souq
//
//  Created by Pawan Ramteke on 13/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class ReviewsViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblViewList: UITableView!
    var arrList = [Reviews]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblViewList.register(UINib(nibName: "ReviewsCell", bundle: nil), forCellReuseIdentifier: "ReviewsCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewsCell") as! ReviewsCell
        let model = arrList[indexPath.row]
        cell.btnRating.setTitle("\(model.rating!)", for: .normal)
        cell.lblAuthorName.text = model.author
        cell.lblDate.text = model.dateAdded
        cell.lblDescription.text = model.text
        return cell
    }
}
