//
//  ListingController.swift
//  Souq
//
//  Created by iLoma on 01/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import ASJCollectionViewFillLayout
class ProductListingController: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegate {
   
    @IBOutlet weak var viewFilterHtConstr: NSLayoutConstraint!
    let LIMIT = 10
    var pageNo = 1
    var loadStarted : Bool!
    var selCategoryModel : CategoriesModel!
    var isDealsOfTheDay : Bool = false
    var selOptionIds : String = ""
    var selSortIndex : Int = 0
    var selSort : String = ""
    
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnSortBy: UIButton!
    @IBOutlet weak var btnView: UIButton!
    
    @IBOutlet weak var collectionListing: UICollectionView!
    var arrList = NSMutableArray()
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setSearchAndCartButtonsToNavigation()
        self.title = localize(string: "LISTING")

        viewFilterHtConstr.constant = isDealsOfTheDay ? 0 : 50
        getProductListService(pageNo: pageNo)
        setupCollectionView(noOfItems: 2)
        collectionListing.register(UINib(nibName: "ProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionCell")
        if Preferences.getAppLanguage() == ENGLISH {
            btnFilter.titleEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        }
        else {
            btnFilter.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
        }
        btnSortBy.titleEdgeInsets = btnFilter.titleEdgeInsets
        btnView.titleEdgeInsets = btnFilter.titleEdgeInsets
    }
    
    func setupCollectionView(noOfItems:Int)
    {
        let layout = ASJCollectionViewFillLayout()
        layout.numberOfItemsInSide = noOfItems
        layout.itemSpacing = 2
        layout.stretchesLastItems = false
        layout.itemLength = 190
        collectionListing.setCollectionViewLayout(layout, animated: true)
    }
    
    func getProductListService(pageNo:Int,filter:String = "",sort:String = ""){
       //index.php?route=feed/rest_api/products&category=20&limit=10&page=1&filters=2&sort=name&order=asc
        
      // index.php?route=feed/rest_api/products&category=20&limit=10&start=1&sort=name&order=asc
        var url = ""
        if isDealsOfTheDay {
            url = "\(DEALS_OF_THE_DAY_API)"
        }
        else {
            if sort == "" && filter == "" {
                url = "\(GET_PRODUCT_LISTING_API)/category/\(String(describing: selCategoryModel.categoryId!))/limit/\(LIMIT)/page/\(pageNo)"
            }
            else if filter != "" && sort == "" {
                url = "\(GET_PRODUCT_LISTING_API)/category/\(String(describing: selCategoryModel.categoryId!))/filters/\(filter)/limit/\(LIMIT)/page/\(pageNo)"
            }
            else if filter == "" && sort != "" {
                url = "\(FILTER_PRODUCT_LISTING_API)&category=\(String(describing: selCategoryModel.categoryId!))&limit=\(LIMIT)&page=\(pageNo)&sort=\(sort)"
            }
            else {
                url = "\(FILTER_PRODUCT_LISTING_API)&category=\(String(describing: selCategoryModel.categoryId!))&limit=\(LIMIT)&page=\(pageNo)&filters=\(filter)&sort=\(sort)"
            }
        }
        
        VIEWMANAGER.showActivityIndicator(self)
        RemoteAPI().callPOSTApi(apiName: url, params: nil, apiType: .get, completion: { (responseData) in
            
            let arrData = responseData as! NSArray
            
            self.arrList.addObjects(from: arrData as! [Any])
            self.collectionListing.reloadData()
            
            self.collectionListing.backgroundView = self.arrList.count > 0 ? nil : self.tableBGView()
            if arrData.count == self.LIMIT
            {
                self.pageNo = self.pageNo + 1
                self.loadStarted = true
           //     self.loader.startAnimating()
            }
            else{
                self.loadStarted = false
            //    self.loader.stopAnimating()
            }

            VIEWMANAGER.hideActivityIndicator()
            self.collectionListing.reloadData()

        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : ProductCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionCell
        let model = self.arrList[indexPath.row] as! ProductListingModel
        cell.lblProductName.text = model.name
        cell.lblProductPrice.text = String (model.priceFormated)
        
        cell.imageViewProduct.sd_setImage(with: URL(string:VIEWMANAGER.getProperImageUrl(strUrl: model.thumb) ?? VIEWMANAGER.getProperImageUrl(strUrl: model.originalImage) ?? ""), placeholderImage: UIImage(named:"placeholder_img"))

        return cell
   }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.arrList[indexPath.row] as! ProductListingModel
        
        let details = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ProductDetailsController") as! ProductDetailsController
        details.productModel = model
        self.navigationController?.pushViewController(details, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !isDealsOfTheDay && indexPath.item == arrList.count - 5{
            if loadStarted {
                getProductListService(pageNo: pageNo, filter:selOptionIds , sort: selSort)
                loadStarted = false
            }
        }
    }
    
    func tableBGView() -> UIView
    {
        let lblEmptyTitle = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 40))
        lblEmptyTitle.font = UIFont.appBoldFont(size: 16)
        lblEmptyTitle.text = localize(string: localize(string: "No data available"))
        lblEmptyTitle.textAlignment = .center
        
        return lblEmptyTitle
    }
    
    @IBAction func btnMenuClicked(_ sender: Any) {
        var parentVC = self.parent
        while parentVC != nil {
            if let drawerVC = parentVC as? KYDrawerController {
                drawerVC.setDrawerState(.opened, animated: true)
            }
            parentVC = parentVC?.parent
        }
    }
    
    
    @IBAction func btnFilterClicked(_ sender: Any) {
        let filtersVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
        filtersVC.arrFilters = NSMutableArray(array: selCategoryModel.filters.filterGroups)
        filtersVC.strTitle = selCategoryModel.name
        filtersVC.selOptionIds = selOptionIds
        self.navigationController?.pushViewController(filtersVC, animated: true)
        
        filtersVC.onApplyFilter { (selIds) in
            self.selOptionIds = selIds
            self.pageNo = 1
            self.getProductListService(pageNo: self.pageNo, filter: selIds)
            self.arrList.removeAllObjects()
            self.collectionListing.reloadData()
        }
    }
    
    @IBAction func btnSortByClicked(_ sender: Any) {
        let sortByVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "SortByViewController") as! SortByViewController
        sortByVC.selIdx = selSortIndex
        self.navigationController?.pushViewController(sortByVC, animated: true)
        
        sortByVC.onSortSelection { (selSort,selIdx) in
            self.selSortIndex = selIdx
            self.selSort = selSort
            self.pageNo = 1
            self.getProductListService(pageNo: self.pageNo, filter:  self.selOptionIds, sort: selSort)
            self.arrList.removeAllObjects()
            self.collectionListing.reloadData()
        }
    }
    
    @IBAction func btnViewTypeClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        setupCollectionView(noOfItems: sender.isSelected ? 1 : 2)
    }
    
}
