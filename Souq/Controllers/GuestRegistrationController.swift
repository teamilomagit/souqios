//
//  GuestRegistrationController.swift
//  Souq
//
//  Created by Pawan Ramteke on 18/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class GuestRegistrationController: BaseViewController {

    var RegistrationClosure : ((AddressModel)->())?
    
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldMobile: CustomTextField!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var txtFieldCountry: CustomTextField!
    @IBOutlet weak var txtFieldZone: CustomTextField!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var txtFieldDistrict: UITextField!
    @IBOutlet weak var txtFieldStreetNo: UITextField!
    @IBOutlet weak var txtFieldHouseNo: UITextField!
    @IBOutlet weak var txtFieldAddr1: UITextField!
    @IBOutlet weak var txtFieldAddr2: UITextField!
    @IBOutlet weak var txtFieldPasscode: UITextField!
    @IBOutlet weak var lblAddressLine2: UILabel!
    
    var arrCountries = NSMutableArray()
    var selCountryId = 0
    
    var arrZones = NSMutableArray()
    var selZoneId = "0"
    override func viewDidLoad() {
        super.viewDidLoad()

        lblAddressLine2.text = localize(string: "Address Line 2")
        txtFieldCountry.setTextFieldTag(tag: .TAG_ACTION_SHEET)
        txtFieldZone.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtFieldMobile.setTextFieldTag(tag: .TAG_MOBILE)
        
        
        if Preferences.getAppLanguage() == ENGLISH {
            btnMale.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            btnMale.contentHorizontalAlignment = .left
            btnFemale.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            btnFemale.contentHorizontalAlignment = .left
        }
        else{
            btnFemale.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
            btnFemale.contentHorizontalAlignment = .right
            btnMale.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
            btnMale.contentHorizontalAlignment = .right
        }

        getCountries()
    }
    
    func getCountries(){
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_COUNTRIES, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrCountries = responseData as! NSMutableArray
            if self.arrCountries.count > 0 {
                self.setupCountryData()
            }
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    func setupCountryData(){
        let arrList = NSMutableArray()
        if self.arrCountries.count > 0 {
            for str in self.arrCountries as [AnyObject]{
                let model  = str as! CountriesModel
                arrList.add(model.name)
            }
        }
        self.txtFieldCountry.actionSheetData = arrList
        txtFieldCountry.onDataSelectionSuccess { (data) in
            let arr = self.arrCountries.filter {
                (($0 as! CountriesModel).name == data)
            }
            if arr.count > 0 {
                let model = arr[0] as! CountriesModel
                self.selCountryId = model.countryId
                self.getZoneService(countryId: model.countryId)
            }
        }
    }
    
    func getZoneService(countryId:Int){
        VIEWMANAGER.showActivityIndicator()
        let api = "\(GET_ZONE)\(countryId)"
        RemoteAPI().callPOSTApi(apiName: api, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            let model = responseData as! ZoneModel
            for zModel in model.zone{
                self.arrZones.add(zModel.name)
            }
            self.txtFieldZone.dropDownData = self.arrZones
            
            self.txtFieldZone.onDropDownSelectionSuccess(success: { (data) in
                let arr = model.zone.filter {
                    (($0).name == data)
                }
                if arr.count > 0 {
                    let model = arr[0]
                    self.selZoneId = model.zoneId
                }
            })
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    
    func guestRegistrationAPI()
    {
        let param = [
                "firstname":txtFieldFirstName.text!,
                "lastname":txtFieldLastName.text!,
                "email":txtFieldEmail.text!,
                "telephone":txtFieldMobile.text!,
                "company":"string",
                "city":txtFieldCity.text!,
                "address_1":txtFieldAddr1.text!,
                "address_2":txtFieldAddr2.text!,
                "country_id":selCountryId,
                "zone_id":selZoneId,
                "postcode":txtFieldPasscode.text!,
                "custom_field":[
                    "account":["1":btnMale.isSelected ? "1" : "2"],
                    "address":[
                        "2":txtFieldCity.text!,
                        "3":txtFieldDistrict.text!,
                        "4":txtFieldStreetNo.text!,
                        "5":txtFieldHouseNo.text!,
                        "6":"Nearest Landmark"
                    ]
            ]
        ] as [String : Any]
        
        VIEWMANAGER.showActivityIndicator(hardLoader: true)
        RemoteAPI().callPOSTApi(apiName: GUEST_LOGIN_URL, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.guestShippingAPI(param: param as [String : AnyObject])
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func guestShippingAPI(param : [String : AnyObject]) {
        VIEWMANAGER.showActivityIndicator(hardLoader: true)
        RemoteAPI().callPOSTApi(apiName: GUEST_SHIPPING_URL, params: param, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            let addrModel = AddressModel()
            addrModel.address1 = self.txtFieldAddr1.text!
            addrModel.address2 = self.txtFieldAddr2.text!
            addrModel.city = self.txtFieldCity.text!
            addrModel.zone = self.txtFieldZone.text!
            addrModel.country = self.txtFieldCountry.text!
            
            self.dismiss(animated: true, completion: {
                if self.RegistrationClosure != nil {
                    self.RegistrationClosure!(addrModel)
                }
            })
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    @IBAction func btnContinueClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            guestRegistrationAPI()
        }
    }
    
    @IBAction func btnGenderClicked(_ sender: UIButton) {
        btnMale.isSelected = false
        btnFemale.isSelected = false
        sender.isSelected = true
    }
    
    
    func validate() -> Bool {
        if txtFieldFirstName.text!.count == 0 {
            self.view.makeToast(localize(string:"Please enter first name"))
            return false
        }
        if txtFieldFirstName.text!.count > 32 {
            self.view.makeToast(localize(string:"First Name must be between 1 and 32 characters!"))
            return false
        }
        
        if txtFieldLastName.text!.count == 0 {
            self.view.makeToast(localize(string:"Please enter last name"))
            return false
        }
        
        if txtFieldLastName.text!.count > 32 {
            self.view.makeToast(localize(string:"Last Name must be between 1 and 32 characters!"))
            return false
        }
        if txtFieldEmail.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter registered email address"))
            return false
        }
        if !txtFieldEmail.text!.isValidEmail() {
            VIEWMANAGER.showToast(localize(string: "Please enter valid email address"))
            return false
        }
        if txtFieldMobile.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter valid mobile number"))
            return false
        }
        if txtFieldCountry.text!.trimString().count == 0 {
            self.view.makeToast(localize(string:"Please select country"))
            return false
        }
        if txtFieldZone.text!.trimString().count == 0 {
            self.view.makeToast(localize(string:"Please select zone"))
            return false
        }
        if txtFieldCity.text!.trimString().count == 0 {
            self.view.makeToast(localize(string:"Please enter your city"))
            return false
        }
        if txtFieldDistrict.text!.trimString().count == 0 {
            self.view.makeToast(localize(string:"Please enter your district"))
            return false
        }
        if txtFieldStreetNo.text!.trimString().count == 0 {
            self.view.makeToast(localize(string:"Please enter your street no"))
            return false
        }
        if txtFieldHouseNo.text!.trimString().count == 0 {
            self.view.makeToast(localize(string:"Please enter house no."))
            return false
        }
        if txtFieldAddr1.text!.trimString().count == 0 {
            self.view.makeToast(localize(string:"Please enter address line 1"))
            return false
        }
//        if txtFieldAddr2.text!.trimString().count == 0 {
//            self.view.makeToast(localize(string:"Please enter address line 2"))
//            return false
//        }
//        if txtFieldAddr2.text!.trimString().count == 0 {
//            self.view.makeToast(localize(string:"Please enter address line 2"))
//            return false
//        }
//        if txtFieldPasscode.text!.trimString().count == 0 {
//            self.view.makeToast(localize(string:"Please enter postcode"))
//            return false
//        }
        
        return true
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
  
    func onGuestRegistrationSuccess(closure : @escaping (AddressModel)->()) {
        RegistrationClosure = closure
    }
}
