//
//  OnboardingController.swift
//  Odito
//
//  Created by Pawan Ramteke on 28/01/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class OnboardingController: UIViewController,UIScrollViewDelegate {

    let arrImages = ["onboard_feature","onboard_sell","onboard_cod","onboard_category","onboard_market"]
    let arrTitles = [
        "WHAT'S NEW",
        "SELL ON SOUQ366",
        "CASH ON DELIVERY",
        "MILLIONS OF ATTRACTIVE PRODUCTS",
        "A VAST MARKETPLACE"
    ]
    
    let arrDescr  = [
        "Amazing features for an unmatched User Experience",
        "Become a brand! Sell your products to millions of buyers",
        "Get all orders shipped within 48 hours and pay easily with cash",
        "A diverse range of handpicked and original products for all shopaholics!",
        "Scale up your business with an all-in-one virtual marketplace solution"
    ]
    var pageControl : UIPageControl!
    var btnStart : UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imgViewBG = UIImageView(frame: self.view.bounds)
        imgViewBG.image = UIImage(named: "bg_souq")
        self.view.addSubview(imgViewBG)

        
        let baseScroll = UIScrollView(frame: self.view.bounds)
        baseScroll.isPagingEnabled = true
        baseScroll.contentSize = CGSize(width: SCREEN_WIDTH * CGFloat(arrImages.count), height: 0)
        baseScroll.delegate = self
        self.view.addSubview(baseScroll)
        for i in 0...arrImages.count - 1 {
            
            let baseView = UIView(frame: CGRect(x:baseScroll.frame.size.width * CGFloat(i), y: baseScroll.frame.midY - 200, width: baseScroll.frame.size.width, height: 400))
            baseScroll.addSubview(baseView)
            
            let topImgHt : CGFloat = IS_IPHONE_5S ? 200 : 200
            let imgViewTop = UIImageView(frame: CGRect(x: 0, y: 0, width: baseView.frame.size.width, height: topImgHt))
            imgViewTop.image = UIImage(named: arrImages[i])
            imgViewTop.contentMode = .scaleAspectFit
            baseView.addSubview(imgViewTop)
            
            let lblTitle = UILabel(frame: CGRect(x: 20, y: imgViewTop.frame.maxY+10, width: imgViewBG.frame.size.width - 40, height: 50))
            lblTitle.numberOfLines = 0
            lblTitle.font = UIFont.appBoldFont(size: 20)
            lblTitle.text = arrTitles[i]
            lblTitle.textColor = .white
            lblTitle.textAlignment = .center
            baseView.addSubview(lblTitle)
            
            let lblDescr = UILabel(frame: CGRect(x: 20, y: lblTitle.frame.maxY, width: imgViewBG.frame.size.width - 40, height: 70))
            lblDescr.numberOfLines = 0
            lblDescr.font = UIFont.appBoldFont(size: 16)
            lblDescr.text = arrDescr[i]
            lblDescr.textColor = .white
            lblDescr.textAlignment = .center
            baseView.addSubview(lblDescr)
            
        }
        
        pageControl = UIPageControl(frame: CGRect(x: (SCREEN_WIDTH/2) - 50, y: SCREEN_HEIGHT - 180, width: 100, height: 50))
        pageControl.pageIndicatorTintColor = UIColor.blue
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.currentPage = 0
        pageControl.numberOfPages = arrImages.count
        pageControl.isUserInteractionEnabled = false
        self.view.addSubview(pageControl)
        
        btnStart = UIButton(frame: CGRect(x: self.view.frame.midX - 35, y: pageControl.frame.maxY, width: 70, height: 40))
        btnStart.setTitle(localize(string: "SKIP"), for: .normal)
        btnStart.setTitleColor(UIColor.appThemeColor, for: .normal)
        btnStart.backgroundColor = UIColor.white
        btnStart.layer.cornerRadius = 4
        btnStart.titleLabel?.font = UIFont.appRegularFont(size: 15)
        btnStart.addTarget(self, action: #selector(btnStartClicked), for: .touchUpInside)
        self.view.addSubview(btnStart)
        
        let viewLogin = UIView()
        viewLogin.backgroundColor = .white
        self.view.addSubview(viewLogin)
        viewLogin.enableAutoLayout()
        viewLogin.leadingMargin(pixels: 0)
        viewLogin.trailingMargin(pixels: 0)
        viewLogin.bottomMargin(pixels: 0)
        viewLogin.fixedHeight(pixels: 50)
        
        let btnLogin = UIButton()
        btnLogin.setTitle(localize(string: "Login"), for: .normal)
        btnLogin.setTitleColor(UIColor.appThemeColor, for: .normal)
        btnLogin.titleLabel?.font = UIFont.appRegularFont(size: 16)
        btnLogin.addTarget(self, action: #selector(btnSignInClicked), for: .touchUpInside)
        
        viewLogin.addSubview(btnLogin)
        btnLogin.enableAutoLayout()
        btnLogin.trailingMargin(pixels: 20)
        btnLogin.topMargin(pixels: 0)
        btnLogin.bottomMargin(pixels: 0)
        
        let lblTitle = UILabel()
        lblTitle.font = UIFont.appRegularFont(size: 16)
        lblTitle.text = localize(string: "New to Souq366? ")
        viewLogin.addSubview(lblTitle)
        lblTitle.enableAutoLayout()
        lblTitle.leadingMargin(pixels: 20)
        lblTitle.topMargin(pixels: 0)
        lblTitle.bottomMargin(pixels: 0)
        
        
        let btnSignup = UIButton()
        btnSignup.setTitle(localize(string: "Sign Up"), for: .normal)
        btnSignup.setTitleColor(UIColor.appThemeColor, for: .normal)
        btnSignup.titleLabel?.font = UIFont.appRegularFont(size: 16)
        btnSignup.addTarget(self, action: #selector(btnSignupClicked), for: .touchUpInside)
        viewLogin.addSubview(btnSignup)
        btnSignup.enableAutoLayout()
        btnSignup.addToRightToView(view: lblTitle, pixels: 0)
        btnSignup.topMargin(pixels: 0)
        btnSignup.bottomMargin(pixels: 0)
    }
    
    @objc func btnStartClicked()
    {
        let response = Preferences.getLoginResponse()
        if response != nil {
            let model = LoginModel(fromDictionary: response as! [String : Any])
            VIEWMANAGER.currentUser = model
        }
        app_del.setDashboardAsRoot()
    }
    
    @objc func btnSignInClicked() {
        let loginVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        VIEWMANAGER.topMostController().present(loginVC, animated: true, completion: nil)
        
        loginVC.onLoginSuccess {
            app_del.setDashboardAsRoot()
        }
        
    }
    
    @objc func btnSignupClicked() {
        let signupVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "SignUpController") as! SignUpController
        let navVC = UINavigationController(rootViewController: signupVC)
        VIEWMANAGER.topMostController().present(navVC, animated: true, completion: nil)
        signupVC.onSignUpSuccess {
            app_del.setDashboardAsRoot()
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }

    override func viewDidLayoutSubviews() {
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }
    }

}
