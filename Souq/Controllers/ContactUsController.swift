//
//  ContactUsController.swift
//  Souq
//
//  Created by iLoma Technology on 23/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import MapKit
class ContactUsController: BaseViewController,UITableViewDataSource , UITableViewDelegate  {
    
    
    @IBOutlet weak var tableviewContactUs: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableviewContactUs.register((UINib(nibName: "ContactUsCell", bundle: nil)), forCellReuseIdentifier: "ContactUsCell")
        tableviewContactUs.delegate = self
        tableviewContactUs.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsCell") as! ContactUsCell
        if indexPath.row == 0 {
            cell.lblTitle.text = "Al-Maghazi Street"
            cell.lblAddress.text = "Al-Maghazi Street, Basrah, Iraq"
            cell.lblMobileNo.text = "+917736950877"
            cell.location = CLLocationCoordinate2D(latitude: 30.397300, longitude:37.719680)
        }
        else if indexPath.row == 1 {
            cell.lblTitle.text = "Bakchah Garden Furniture"
            cell.lblAddress.text = "Bakchah Garden Furniture, Erbil, Iraq"
            cell.lblMobileNo.text = "+917736950877"
            cell.location = CLLocationCoordinate2D(latitude: 36.218767, longitude:44.011028)

        }
        else if indexPath.row == 2 {
            cell.lblTitle.text = "Amirat St"
            cell.lblAddress.text = "Bakchah Garden Furniture, Erbil, Iraq"
            cell.lblMobileNo.text = "+917736950877"
            cell.location = CLLocationCoordinate2D(latitude: 31.085840, longitude:46.246350)

        }
        return cell
    }
    


}
