//
//  HelpController.swift
//  Souq
//
//  Created by iLoma Technology on 24/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class HelpController: BaseViewController {
    

    @IBOutlet weak var lblNeedHelp: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        lblNeedHelp.text = localize(string: "Need Help? We’re here!")
        // Do any additional setup after loading the view.
    }
    


    @IBAction func call(_ sender: Any) {
        let url = URL(string: "telprompt://\(CONTACT_NO)")
        if url != nil{
            UIApplication.shared.open(url!)
        }
        
    }
    
    @IBAction func mailHelpCenter(_ sender: Any) {
        let email = "help@souq366.com"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func performActionSellMail(_ sender: Any) {
        let email = "care@souq366.com"
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func btnGoToMyOrders(_ sender: Any) {
        if VIEWMANAGER.currentUser != nil{
            pushToOrderHistoryController()
        }
        else{
            loginController()
        }
    
    }
    
    @IBAction func btnClickMyAccount(_ sender: Any) {
        if VIEWMANAGER.currentUser != nil{
            pushToMyAccountController()
        }
        else{
            loginController()
        }
        
    }
    func pushToOrderHistoryController()
    {
        let orderHistoryVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "OrderHistoryController") as! OrderHistoryController
        self.navigationController?.pushViewController(orderHistoryVC, animated: true)
    }
    
    func pushToMyAccountController()
    {
        let accountviewVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
        self.navigationController?.pushViewController(accountviewVC, animated: true)
    }
    
    func loginController() {
        let loginVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        VIEWMANAGER.topMostController().present(loginVC, animated: true, completion: nil)
        
        loginVC.onLoginSuccess {
            self.pushToOrderHistoryController()
        }
        
    }
}
