//
//  SignUpController.swift
//  Souq
//
//  Created by iLoma Technology on 16/01/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class SignUpController: UIViewController {
    
    @IBOutlet weak var topLogo: UIImageView!

    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldMobile: CustomTextField!
    @IBOutlet weak var txtFiledPassword: CustomTextField!
    @IBOutlet weak var txtFieldConfirmPassword: CustomTextField!
    @IBOutlet weak var btnCheckTandC: UIButton!
    @IBOutlet weak var btnRadioMale: UIButton!
    @IBOutlet weak var btnRadioFemale: UIButton!
    var strChek = String()
    var strGender = String()
    var loginSuccess : (()->())?

    var fromSignIn : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        topLogo.image = UIImage(named: "logo")?.withRenderingMode(.alwaysTemplate)
        topLogo.tintColor = UIColor.appThemeColor

        txtFieldMobile.placeholder = localize(string: "Enter phone number")
        txtFiledPassword.placeholder = localize(string: "Create a password")
        txtFieldConfirmPassword.placeholder = localize(string: "Enter the password again")
        txtFieldMobile.setTextFieldTag(tag: .TAG_MOBILE)
        txtFiledPassword.setTextFieldTag(tag: .TAG_PASSWORD)
        txtFieldConfirmPassword.setTextFieldTag(tag: .TAG_PASSWORD)
        btnCheckTandC.addTarget(self, action: #selector(btnCheckTandCClicked), for: .touchUpInside)
        btnRadioMale.addTarget(self, action: #selector(btnRadioMaleClicked), for: .touchUpInside)
        btnRadioFemale.addTarget(self, action: #selector(btnRadioFemaleClicked), for: .touchUpInside)
        btnRadioMale.isSelected = true
        if Preferences.getAppLanguage() == ENGLISH {
            btnRadioMale.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            btnRadioMale.contentHorizontalAlignment = .left
        }
        else{
            btnRadioMale.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
            btnRadioMale.contentHorizontalAlignment = .right
        }
        btnRadioFemale.titleEdgeInsets = btnRadioMale.titleEdgeInsets
        btnRadioFemale.contentHorizontalAlignment = btnRadioMale.contentHorizontalAlignment
        btnCheckTandC.isSelected = false
    }
    @objc func btnCheckTandCClicked(_ sender:UIButton){
        sender.isSelected = !sender.isSelected
    }
    @objc func btnRadioMaleClicked(_ sender:UIButton){
        sender.isSelected = true
        btnRadioFemale.isSelected = false
    }
    @objc func btnRadioFemaleClicked(_ sender:UIButton){
        sender.isSelected = true
        btnRadioMale.isSelected = false
    }
    
    
    func signUPService() {
        //let cust = ["custom_field": ["account": ":)"]]
        
        
        let cust: [String:AnyObject] =  (["account":["1":btnRadioMale.isSelected ? "1" : "2"]] as AnyObject) as! [String : AnyObject]
        
        
        let param = ["firstname": txtFieldFirstName.text!,
                     "lastname": txtFieldLastName.text!,
                     "email": txtFieldEmail.text!,
                     "password": txtFiledPassword.text!,
                     "confirm": txtFieldConfirmPassword.text!,
                     "telephone": txtFieldMobile.text!,
                     "customer_group_id": "1",
                     "agree": btnCheckTandC.isSelected ? "1" : "",
                     "custom_field":cust
            
                     ] as [String : AnyObject]
        print(param)
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName:REGISTRATION_API, params: param, completion: { (responseData) in
            let model: LoginModel? = responseData as? LoginModel
            VIEWMANAGER.currentUser = model
            if self.loginSuccess != nil {
                self.loginSuccess!()
            }
            self.btnCloseClicked(nil)
           VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    func validate() -> Bool {
        if txtFieldFirstName.text!.trimString().count == 0 {
            self.view.makeToast(localize(string: "Please enter your first name"))
            return false
        }
        if txtFieldLastName.text!.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter your last name"))
            return false
        }
        if txtFieldEmail.text!.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter your email"))
            return false
        }
        if !txtFieldEmail.text!.isValidEmail() {
            VIEWMANAGER.showToast(localize(string: "Please enter valid email address"))
            return false
        }
        else if txtFieldMobile.text!.count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter your mobile number"))
            return false
        }
        if txtFiledPassword.text!.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter password"))
            return false
        }
        
        else if txtFiledPassword.text!.count < 4 {
            self.view.makeToast(localize(string:"Password must have 4 to 20 character"))
            return false
        }
        
        if txtFieldConfirmPassword.text!.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter confirm password"))
            return false
        }
        if txtFiledPassword.text! != txtFieldConfirmPassword.text! {
            VIEWMANAGER.showToast(localize(string: "Password and confirm password does not matched"))
            return false
        }
        
        if txtFieldMobile.text!.count < 10 {
            VIEWMANAGER.showToast(localize(string: "Please enter valid mobile number"))
            return false
        }
       
        if !btnCheckTandC.isSelected{
            VIEWMANAGER.showToast(localize(string: "Please check privacy policy checkbox"))
            return false
        }
        
        return true
    }
    
    @IBAction func btnSignUpClicked(_ sender: Any) {
        if validate() {
            signUPService()
        }
        
    }

    @IBAction func btnSignInClicked(_ sender: Any) {
        if fromSignIn {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        let loginVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        loginVC.fromSignup = true
        VIEWMANAGER.topMostController().present(loginVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnPrivacyClicked(_ sender: Any) {
        let webVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webVC.title = localize(string: "Privacy Policy")
        VIEWMANAGER.topMostController().navigationController?.pushViewController(webVC, animated: true)
    }
    
    
    @IBAction func btnCloseClicked(_ sender: Any?){
       // self.dismiss(animated: true, completion: nil)
        app_del.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }

    func onSignUpSuccess(closure:@escaping ()->()) {
        loginSuccess = closure
    }
}
