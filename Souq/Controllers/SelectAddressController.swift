//
//  SelectAddressController.swift
//  Souq
//
//  Created by Pawan Ramteke on 02/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class SelectAddressController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    var cartModel : ViewCartProductModel!
    var arrAddressList = NSMutableArray()
    @IBOutlet weak var tblViewList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = localize(string: "SELECT ADDRESS")
        
        tblViewList.register(UINib(nibName: "SelectAddressCell", bundle: nil), forCellReuseIdentifier: "SelectAddressCell")
        
        getAccountAddressListService()
    }
    
    func getAccountAddressListService(){
        
        VIEWMANAGER.showActivityIndicator(self)
        RemoteAPI().callPOSTApi(apiName: GET_ACCOUNT_ADDRESS_LIST, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrAddressList = NSMutableArray(array: responseData as! NSArray)
            self.tblViewList.backgroundView = self.arrAddressList.count > 0 ? nil : self.tableBGView()
            
            self.tblViewList.reloadData()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    func setPaymentAddressToCheckout(addressId:String,completion:@escaping ()->())
    {
        let param = ["address_id" : addressId]
        
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: SET_PAYMENT_ADDRESS_API, params: param as [String : AnyObject], completion: { (response) in
           // VIEWMANAGER.hideActivityIndicator()
            self.setShippingAddressToCheckout(addressId: addressId, completion: { () in
                completion()
            })
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func setShippingAddressToCheckout(addressId:String,completion:@escaping ()->())
    {
        let param = ["address_id" : addressId]
        
       // VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: SET_SHIPPING_ADDRESS_API, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            completion()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    
    func tableBGView() -> UIView
    {
        let viewBG = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: tblViewList.frame.size.height))
        viewBG.backgroundColor = UIColor.clear
        
        let imgView = UIImageView()
        imgView.image = UIImage(named: "")
        viewBG.addSubview(imgView)
        imgView.enableAutoLayout()
        imgView.centerX()
        imgView.topMargin(pixels: 10)
        imgView.fixedWidth(pixels: SCREEN_HEIGHT * 0.4)
        imgView.fixedHeight(pixels: 0)
        
        let lblEmptyTitle = UILabel()
        lblEmptyTitle.font = UIFont.appBoldFont(size: 16)
        lblEmptyTitle.text = localize(string: localize(string: "Your address book looks empty"))
        lblEmptyTitle.textAlignment = .center
        viewBG.addSubview(lblEmptyTitle)
        lblEmptyTitle.enableAutoLayout()
        lblEmptyTitle.leadingMargin(pixels: 10)
        lblEmptyTitle.trailingMargin(pixels: 10)
        lblEmptyTitle.centerY()
        
        let lblSubTitle = UILabel()
        lblSubTitle.font = UIFont.appRegularFont(size: 14)
        lblSubTitle.textColor = UIColor.lightGray
        lblSubTitle.text = localize(string: localize(string: localize(string: "")))
        lblSubTitle.textAlignment = .center
        viewBG.addSubview(lblSubTitle)
        lblSubTitle.enableAutoLayout()
        lblSubTitle.leadingMargin(pixels: 10)
        lblSubTitle.trailingMargin(pixels: 10)
        lblSubTitle.belowToView(view: lblEmptyTitle, pixels: 10)
    
        return viewBG
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectAddressCell") as! SelectAddressCell
        let model = arrAddressList[indexPath.row] as! AddressModel
        cell.radioButton.isSelected = model.isSelected
        cell.lblName.text = "\(model.firstname!) \(model.lastname!)"
        cell.lblAddress.text = "\(model.address1!), \(model.address2!), \(model.city!) - \(model.postcode!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        resetData()
        let model = arrAddressList[indexPath.row] as! AddressModel
        model.isSelected = true
        tableView.reloadData()
    }
    
    func resetData() {
        
        for i in 0..<arrAddressList.count {
            let model = arrAddressList[i] as! AddressModel
            model.isSelected = false
            arrAddressList.replaceObject(at: i, with: model)
        }
    }
    
    func pushToReviewOrderController(addrModel : AddressModel) {
        let reviewOrderVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ReviewOrderController") as! ReviewOrderController
        reviewOrderVC.cartModel = cartModel
        reviewOrderVC.selAddrModel = addrModel
        self.navigationController?.pushViewController(reviewOrderVC, animated: true)
    }
    
    @IBAction func btnAddNewAddressClicked()
    {
        let addAddressVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "AddNewAddressController") as! AddNewAddressController
        self.navigationController?.pushViewController(addAddressVC, animated: true)
        addAddressVC.onAddressAddedSuccess {
            self.getAccountAddressListService()
        }
    }
    
    
    @IBAction func btnNextClicked(_ sender: Any) {
        if arrAddressList.count == 0{
            VIEWMANAGER.showToast(localize(string: "Please add atleast one address"))
        } else{
        
        let arrSelAddr = arrAddressList.filter {($0 as! AddressModel).isSelected == true}
        if arrSelAddr.count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please select atleast one address"))
            return
        }
        
        
        let model = arrSelAddr[0] as! AddressModel

        setPaymentAddressToCheckout(addressId:model.addressId) {
            self.pushToReviewOrderController(addrModel: model)
        }
    }
    }
}
