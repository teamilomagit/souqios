//
//  ForgotPasswordController.swift
//  Souq
//
//  Created by iLoma on 01/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class ForgotPasswordController: UIViewController {
    @IBOutlet weak var txtFieldEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         //txtFieldEmail.setBottomBorder()
    }


    @IBAction func btnSubmitEmailClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            forgotPasswordService()
        }
    }
    
    func validate() -> Bool
    {
        if txtFieldEmail.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter registered email address"))
            return false
        }
        
        if txtFieldEmail.text?.isValidEmail() == false {
            VIEWMANAGER.showToast(localize(string: "Please enter valid email address"))
            return false
        }
        return true
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func forgotPasswordService() {
        let param = ["email":txtFieldEmail.text] as [String : AnyObject]
        
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName:FORGOT_PASSWORD_API, params: param, completion: { (responseData) in
            CustomAlertView.showAlert(withTitle: "", messsage: localize(string: "Thank you, you will receive reset password link in your email address"), completion: {
                self.dismiss(animated: true, completion: nil)
            })
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
}
