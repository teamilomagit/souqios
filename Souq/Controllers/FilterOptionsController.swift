//
//  FilterOptionsController.swift
//  Souq
//
//  Created by Pawan Ramteke on 07/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class FilterOptionsController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    var OptionClosure : ((String)->())?
    
    @IBOutlet weak var txtFieldSearch: CustomTextField!
    @IBOutlet weak var tblViewList: UITableView!
    var arrFilterOptions = NSMutableArray()
    var arrSearch = NSMutableArray()
    var isFiltered = false

    var arrSelIds = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSearchTextField()
        tblViewList.register(UINib(nibName: "FilterOptionsCell", bundle: nil), forCellReuseIdentifier: "FilterOptionsCell")
        let arrData = arrFilterOptions as! [FilterOptions]
        
        arrFilterOptions = NSMutableArray()
        for model in arrData {
            model.isSelected = arrSelIds.contains(model.filterId)
            arrFilterOptions.add(model)
        }
    }
    
    func setupSearchTextField() {
        txtFieldSearch.layer.borderWidth = 1.0
        txtFieldSearch.layer.borderColor = UIColor.appThemeColor.cgColor
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
        imgView.contentMode = .scaleAspectFit
        imgView.image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)
        imgView.tintColor = UIColor.appThemeColor
        
        if Preferences.getAppLanguage() == ENGLISH {
            txtFieldSearch.leftViewMode = .always
            txtFieldSearch.leftView = imgView
        }
        else {
            txtFieldSearch.rightViewMode = .always
            txtFieldSearch.rightView = imgView
        }
        txtFieldSearch.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? arrSearch.count : arrFilterOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterOptionsCell") as! FilterOptionsCell
        cell.indexPath = indexPath
        let model = isFiltered ? arrSearch[indexPath.row] as! FilterOptions : arrFilterOptions[indexPath.row] as! FilterOptions
        cell.lblOption.text = model.name
        cell.btnCheck.isSelected = model.isSelected
        cell.onOptionSelection { (indexPath) in
           
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.isFiltered ? self.arrSearch[indexPath.row] as! FilterOptions : self.arrFilterOptions[indexPath.row] as! FilterOptions
        model.isSelected = !model.isSelected
        model.isSelected ? self.arrSelIds.add(model.filterId) : self.arrSelIds.remove(model.filterId)
        self.isFiltered ? self.arrSearch.replaceObject(at: indexPath.row, with: model) : self.arrFilterOptions.replaceObject(at: indexPath.row, with: model)
        self.tblViewList.reloadData()
    }
    
    @IBAction func btnApplyClicked(_ sender: Any) {
        if self.OptionClosure != nil {
            OptionClosure!(arrSelIds.count > 0 ? arrSelIds.componentsJoined(by: ",") : "")
        }
        self.navigationController?.popViewController(animated: true)
    }
    func onOptionsSelection(closure : @escaping (String)->()) {
        OptionClosure = closure
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let nsString = textField.text! as NSString
        let resultingString = nsString.replacingCharacters(in: range, with: string)
        applyFilter(text: resultingString)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func applyFilter(text:String)
    {
        if text.count == 0
        {
            isFiltered = false
            tblViewList.reloadData()
            return
        }
        else
        {
            isFiltered = true
        }
        
//        let terms = text.components(separatedBy: CharacterSet.whitespaces)
//        let subpredicates = NSMutableArray()
//
//        for term in terms
//        {
//            if term.count == 0
//            {
//                continue
//            }
//            let p = NSPredicate(format:"name contains[c] %@",text)
//            subpredicates.add(p)
//        }
        
      ///  let filter = NSCompoundPredicate(orPredicateWithSubpredicates: subpredicates as! [NSPredicate])
        
//        let predicate = NSPredicate(format:"name contains[c] %@",text)
        let arr = arrFilterOptions.filter {($0 as! FilterOptions).name.localizedCaseInsensitiveContains(text) }
      //  let arr = arrFilterOptions.filtered(using: filter)
        arrSearch.removeAllObjects()
        arrSearch.addObjects(from: arr)
        tblViewList.reloadData()
    }
}
