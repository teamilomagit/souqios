//
//  SplashViewController.swift
//  Souq
//
//  Created by Pawan Ramteke on 04/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var btnRetry: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSessionService()
        btnRetry.isHidden = true

    }
    
    func getSessionService(){
        self.activityIndicator.startAnimating()
        RemoteAPI().callPOSTApi(apiName:GET_SESSION_API, params: nil, apiType: .get, completion: { (responseData) in
            let model = responseData as! SessionKeyModel
            Preferences.saveSessionKey(response: model.session)
            
            app_del.getProductCategoriesAPI {
                self.activityIndicator.stopAnimating()
                let str = Preferences.getOnbording()
                if !str {
                    app_del.setOnboardControllerAsRoot()
                    Preferences.saveOnbording(response: true)
                }else{
                    app_del.setDashboardAsRoot()
                }
            }
            
        }) { (errMsg) in
            self.btnRetry.isHidden = false
            self.activityIndicator.stopAnimating()
        }
    }
    
    @IBAction func btnRetryClicked(_ sender: Any) {
        btnRetry.isHidden = true
        getSessionService()
    }
    
}
