//
//  ProductDetailsController.swift
//  Souq
//
//  Created by iLoma on 05/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import SDWebImage
import ASJCollectionViewFillLayout

class ProductDetailsController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate {
    var arrDeatails = NSMutableArray()
    var arrSimilarProducts = NSArray()
    var productModel: ProductListingModel!
    var detailsModel: ProductDetailsModel!

    @IBOutlet weak var bannerView: BannersListView!
    
    var pageControl : UIPageControl!
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblAvailability: UILabel!
    
    @IBOutlet weak var tblViewOptions: UITableView!
    
    @IBOutlet weak var tblViewOptionsHtConstr: NSLayoutConstraint!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var favoriteLoader: UIActivityIndicatorView!
    @IBOutlet weak var btnShipping: UIButton!
    @IBOutlet weak var btnRatings: UIButton!
    @IBOutlet weak var btnReviews: UIButton!
    
    @IBOutlet weak var btnSelQuantity: UIButton!
    var viewSelQuantity : ProductQuantityView!
    @IBOutlet weak var viewDescription: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var viewSimilarProductHtConstr: NSLayoutConstraint!
    @IBOutlet weak var lblSimilarProdTitle: UILabel!
    @IBOutlet weak var _collectionView: UICollectionView!
    
    @IBOutlet weak var btnPostQuestion: UIButton!
    @IBOutlet weak var btnAddToCart: AppButton!
    
    @IBOutlet weak var btnAddToCartHtConstr: NSLayoutConstraint!
    
    var productId = Int()
    var arrProduct = NSArray()
    var arrOptions = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        setSearchAndCartButtonsToNavigation()
        self.title = localize(string: "DETAIL")
        
        lblProductName.font = UIFont.appMediumFont(size: 20)
        lblProductName.text = productModel.name
        lblAvailability.text = productModel.stockStatus
        lblProductPrice.text = productModel.priceFormated
       // lblDescription.text = productModel.descriptionField
        self.productId = productModel.productId
    
        btnRatings.setTitleColor(UIColor.ratingGoldenColor, for: .normal)
        btnRatings.setImage(UIImage(named: "ic_star")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnRatings.tintColor = UIColor.ratingGoldenColor
        viewDescription.isHidden = true
        
        btnRatings.isHidden = true
        btnReviews.isHidden = true
        bannerView.onSelectImage { (index) in
            self.showImageViewerController(index)
        }
        
        btnRatings.contentHorizontalAlignment = Preferences.getAppLanguage() == ENGLISH ? .left : .right
        btnReviews.contentHorizontalAlignment = Preferences.getAppLanguage() == ENGLISH ? .left : .right

        if Preferences.getAppLanguage() == ENGLISH {
            btnSelQuantity.semanticContentAttribute = .forceRightToLeft
            btnSelQuantity.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
            btnSelQuantity.imageEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0)
            
            btnRatings.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        }
        else {
            btnSelQuantity.semanticContentAttribute = .forceLeftToRight
            btnSelQuantity.titleEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0)
            btnSelQuantity.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
            
            btnRatings.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5)
        }

        tblViewOptions.register(UINib(nibName: "DetailsOptionCell", bundle:nil), forCellReuseIdentifier: "DetailsOptionCell")
        tblViewOptions.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        btnAddToCartHtConstr.constant = 0
        btnPostQuestion.setTitleColor(UIColor.appThemeColor, for: .normal)
        btnPostQuestion.layer.borderColor = UIColor.appThemeColor.cgColor
        viewSimilarProductHtConstr.constant = 0
        setupCollectionView()
        
        getProductDetails()
        getSimilarProductsAPI()
        
        self.btnFavorite.setImage(UIImage(named: "ic_favourite")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.btnFavorite.tintColor = UIColor.lightGray
        
        
        bannerView.contentModeForBanner = .scaleAspectFit
        bannerView.backgroundColor = UIColor.white
    }
    
    func setupCollectionView()
    {
        let layout = ASJCollectionViewFillLayout()
        layout.direction = .horizontal
        layout.itemSpacing = 2
        layout.itemLength = 170
        
        _collectionView.setCollectionViewLayout(layout, animated: true)
        _collectionView.register(UINib(nibName: "ProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionCell")
    }

    func getProductDetails()
    {
       let api = "\(GET_PRODUCT_DETAILS_API)/\(productModel.productId!)"
        RemoteAPI().callPOSTApi(apiName: api, params: nil, apiType: .get, completion: { (responseData) in
            self.viewDescription.isHidden = false
            self.loader.stopAnimating()
            let model = responseData as! ProductDetailsModel
            self.detailsModel = model
            self.bannerView.bannerData = model.images! as NSArray
            self.lblProductName.text = model.name
            self.lblProductPrice.text = model.priceFormated
            self.lblAvailability.text = model.stockStatus
            self.lblDescription.attributedText = self.setHTMLFromString(htmlText: model.descriptionField)
            self.productId = model.productId
            
            self.btnRatings.isHidden = model.rating == 0
            self.btnRatings.setTitle("\(model.rating!)", for: .normal)

            self.btnReviews.isHidden = model.reviews.reviewTotal == "0"
            self.btnReviews.setTitle("\(model.reviews.reviewTotal!) \(localize(string: "Reviews"))", for: .normal)

            self.btnAddToCartHtConstr.constant = 50
            self.arrOptions = NSMutableArray(array: model.options)
            self.tblViewOptions.reloadData()
            self.btnFavorite.isSelected = model.inWishlist
            self.btnFavorite.tintColor = model.inWishlist ? UIColor.appThemeColor : UIColor.lightGray

        }) { (errMsg) in
        }
    }
    
    func getSimilarProductsAPI()
    {
        let api = "\(SIMILAR_PRODUCT_LISTING_API)&id=\(productModel.productId!)"
        RemoteAPI().callPOSTApi(apiName: api , params: nil, apiType: .get, completion: { (responseData) in
            self.arrSimilarProducts = responseData as! NSArray
            if self.arrSimilarProducts.count > 0 {
                self.viewSimilarProductHtConstr.constant = 220
                DispatchQueue.main.async {
                    
                    self._collectionView.reloadData()
                }
            }
        
        }) { (errMsg) in
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func addProductToCart()
    {
        let param = NSMutableDictionary()
        param.setValue(productId, forKey: "product_id")
        param.setValue(btnSelQuantity.titleLabel!.text!, forKey: "quantity")
        if arrOptions.count > 0 {
            let optionDict = NSMutableDictionary()
            for mod in arrOptions {
                let model = mod as! Option
                optionDict.setValue(getSelectedOptionID(optionValue: model.optionValue), forKey: "\(model.productOptionId!)")
            }
            param.setObject(optionDict, forKey: "option" as NSCopying)
        }
       
        btnAddToCart.showLoader()
        RemoteAPI().callPOSTApi(apiName: ADD_PRODUCT_TO_CART, params: param as? [String : AnyObject] , apiType: .post, completion: { (responseData) in
            self.btnAddToCart.hideLoader()
            self.view.makeToast(localize(string:"Product added to Cart successfully"))
            let model = responseData as! AddToCartModel
            self.showProceedToCheckoutView(model: model)
            var cartCount = Preferences.getCartCount()!
            let quantity = Int(self.btnSelQuantity.titleLabel!.text!)!
            cartCount = cartCount + quantity
            Preferences.saveCartCount(count: cartCount)
            self.addItemToCart()
        }) { (errMsg) in
            self.btnAddToCart.hideLoader()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func addProductToWishlist(isRemove : Bool = false)
    {
        let url = "\(MY_WISHLIST_API)/\(productId)"
        favoriteLoader.startAnimating()
        RemoteAPI().callPOSTApi(apiName: url, params: nil, apiType: isRemove ? .delete : .post, completion: { (response) in
            self.favoriteLoader.stopAnimating()
            if !isRemove {
                VIEWMANAGER.showToast(localize(string: "Added to wish list successfully"))
                self.btnFavorite.tintColor = UIColor.appThemeColor
            }
            else {
                self.btnFavorite.tintColor = UIColor.lightGray
            }
        }) { (errMsg) in
            self.favoriteLoader.stopAnimating()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func getShippingDetails() {
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: GET_ZONE_SHIPPING_API, params: nil, apiType: .get, completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            self.showZoneShippingView(data: response as! NSArray)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSimilarProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionCell", for: indexPath) as! ProductCollectionCell
        let model = Preferences.getAppLanguage() == ENGLISH ?  arrSimilarProducts[indexPath.row] as! ProductListingModel : arrSimilarProducts[arrSimilarProducts.count - 1 - indexPath.row] as! ProductListingModel
        cell.lblProductName.text = model.name
        cell.imageViewProduct.sd_setImage(with: URL(string:VIEWMANAGER.getProperImageUrl(strUrl: model.thumb) ?? VIEWMANAGER.getProperImageUrl(strUrl: model.originalImage) ?? ""), placeholderImage: UIImage(named:"placeholder_img"))
        cell.lblProductPrice.text = model.priceFormated
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = Preferences.getAppLanguage() == ENGLISH ?  arrSimilarProducts[indexPath.row] as! ProductListingModel : arrSimilarProducts[arrSimilarProducts.count - 1 - indexPath.row] as! ProductListingModel
        
       self.pushToProductDetailsController(productModel: model)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsOptionCell") as! DetailsOptionCell
        let model = arrOptions[indexPath.row] as! Option
        cell.lblTitle.text = "\(model.name!):"
        cell.lblValue.text = "\(getSelectedOption(optionValue: model.optionValue!))"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrOptions[indexPath.row] as! Option
        let detailsOptionsVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailsOptionsController") as! DetailsOptionsController
        detailsOptionsVC.arrValues = NSMutableArray(array: model.optionValue)
        self.navigationController?.pushViewController(detailsOptionsVC, animated: true)
        
        detailsOptionsVC.onOptionSelecton { (arrOptions) in
            model.optionValue = arrOptions as? [OptionValue]
            self.arrOptions.replaceObject(at: indexPath.row, with: model)
            self.tblViewOptions.reloadData()
        }
    }
    
    func pushToProductDetailsController(productModel:ProductListingModel)
    {
        let details = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ProductDetailsController") as! ProductDetailsController
        details.productModel = productModel
        self.navigationController?.pushViewController(details, animated: true)
    }
    
    func getSelectedOption(optionValue:[OptionValue]) -> String {
        let arr = optionValue.filter { $0.isSelected == true}
        if arr.count > 0 {
            let model = arr[0]
            if model.price == 0 {
                return model.name
            }
            return "\(model.name!)(\(model.pricePrefix!)\(model.priceFormated!))"
        }
        return ""
    }
    
    func getSelectedOptionID(optionValue:[OptionValue]) -> String {
        let arr = optionValue.filter { $0.isSelected == true}
        if arr.count > 0 {
            let model = arr[0]
            return "\(model.optionValueId!)"
        }
        return "0"
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblViewOptionsHtConstr.constant = tblViewOptions.contentSize.height
    }
    
    @IBAction func btnAddToCartClicked(_ sender: Any) {
        self.addProductToCart()
    }
    
    @IBAction func btnAddToWishlistClicked(_ sender: UIButton) {
        if VIEWMANAGER.currentUser == nil {
            VIEWMANAGER.showToast(localize(string: "You must login or create an account to save item to your wishlist"))
            return
        }
        sender.isSelected = !sender.isSelected
        addProductToWishlist(isRemove: !sender.isSelected)
    }
    
    
    @IBAction func btnShippingClicked(_ sender: Any) {
        getShippingDetails()
    }
    
    @IBAction func btnSelectQuantityClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.isSelected ? showProductQuantityView() : viewSelQuantity.hide()
    }
    
    @IBAction func btnReviewsClicked(_ sender: Any) {
        
        if detailsModel == nil || detailsModel?.reviews.reviews.count == 0 {
            return
        }
        let reviewsVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ReviewsViewController") as! ReviewsViewController
        reviewsVC.arrList = detailsModel!.reviews.reviews
        self.navigationController?.pushViewController(reviewsVC, animated: true)
    }
    
    @IBAction func btnPostQuestionClicked(_ sender: Any) {
        let postQuestionVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "PostQuestionController") as! PostQuestionController
        self.present(postQuestionVC, animated: true, completion: nil)
    }
    
    
    func showProductQuantityView()
    {
        viewSelQuantity = ProductQuantityView(frame: CGRect(x: 0, y: self.view.frame.maxY, width: self.view.frame.size.width, height: 120))
        self.view.addSubview(viewSelQuantity)
        viewSelQuantity.show()
        viewSelQuantity.selIndex = Int(btnSelQuantity.titleLabel!.text!)! - 1
        viewSelQuantity.onCloseClicked {(selQuantity) in
            self.btnSelQuantity.isSelected = false
            self.btnSelQuantity.setTitle(String(format: "%02d", selQuantity), for: .normal)
            self.viewSelQuantity.hide()
        }
    }
    
    func showProceedToCheckoutView(model:AddToCartModel)
    {
        let viewProceed = Bundle.main.loadNibNamed("ProceedCheckoutView", owner: self, options: nil)?.first as! ProceedCheckoutView
        self.view.addSubview(viewProceed)
        viewProceed.enableAutoLayout()
        viewProceed.leadingMargin(pixels: 0)
        viewProceed.trailingMargin(pixels: 0)
        viewProceed.topMargin(pixels: 0)
        viewProceed.bottomMargin(pixels: 0)
        
        viewProceed.lblPrice.text = productModel.priceFormated
        let pModel = model.product as ProductAddCart
        viewProceed.lblProductName.text = " "
        viewProceed.lblProductBrand.text = pModel.name
        viewProceed.onProceedToCheckout {
            self.btnCartClicked()
        }
        
        viewProceed.onContinueToShopping {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
        
    func showImageViewerController(_ index : Int)
    {
        let imgViewer = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "ImageViewerController") as! ImageViewerController
        let arrData = bannerView.bannerData.copy() as! NSArray
        imgViewer.imgData = NSMutableArray(array: arrData)
        imgViewer.selIdx = index
        self.present(imgViewer, animated: true, completion: nil)
    }
    
    func setHTMLFromString(htmlText: String) -> NSAttributedString {
        let modifiedFont = String(format:"<span style=\"font-family: 'Raleway'; font-size: \(lblDescription.font!.pointSize)\">%@</span>", htmlText)

        //process collection values
        let attrStr = try! NSAttributedString(
            data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue],
            documentAttributes: nil)
        
        return attrStr
    }
    
    func showZoneShippingView(data:NSArray) {
        let shippingView = ShippingDetailsView(frame: UIScreen.main.bounds, shippingData: data)
        UIApplication.shared.keyWindow?.addSubview(shippingView)
        shippingView.enableAutoLayout()
        shippingView.leadingMargin(pixels: 0)
        shippingView.trailingMargin(pixels: 0)
        shippingView.topMargin(pixels: 0)
        shippingView.bottomMargin(pixels: 0)
    }
    
    deinit {
        tblViewOptions.removeObserver(self, forKeyPath: "contentSize")
    }
}
