//
//  AddNewAddressController.swift
//  Souq
//
//  Created by Pawan Ramteke on 18/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class AddNewAddressController: BaseViewController {
    
    var addAddrSuccess : (()->())?
    @IBOutlet weak var txtFieldFirstName: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var txtFieldCountry: CustomTextField!
    @IBOutlet weak var txtFieldZone: CustomTextField!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var txtFieldDistrict: UITextField!
    @IBOutlet weak var txtFieldStreetNo: UITextField!
    @IBOutlet weak var txtFieldHouseNo: UITextField!
    @IBOutlet weak var txtFieldAddress1: UITextField!
    @IBOutlet weak var txtFieldAddress2: UITextField!
    @IBOutlet weak var txtFieldPostalCode: UITextField!
    @IBOutlet weak var btnSetAsDefYes: UIButton!
    @IBOutlet weak var btnSetAsDefNo: UIButton!
    @IBOutlet weak var btnAddressLableYes: UIButton!
    @IBOutlet weak var btnAddressLableNo: UIButton!
    
    var arrCountries = NSMutableArray()
    var currentIdx : Int = 0
    var countryId = Int()
    var zoneId : String = ""
    var arrZones = NSMutableArray()
    var accountModel : AddressModel?
    
    var zoneModel : ZoneModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = localize(string: "ADD ADDRESS")

        self.setBorder(btn: btnSetAsDefYes)
        self.setBorder(btn: btnSetAsDefNo)
        self.setBorder(btn: btnAddressLableYes)
        self.setBorder(btn: btnAddressLableNo)
        
        txtFieldCountry.setTextFieldTag(tag: .TAG_ACTION_SHEET)
        txtFieldZone.setTextFieldTag(tag: .TAG_DROP_DOWN)
        
        btnSetAsDefClicked(btnSetAsDefYes)
        btnAddressLableClicked(btnAddressLableYes)
        txtFieldFirstName.text = VIEWMANAGER.currentUser?.firstname
        txtFieldLastName.text = VIEWMANAGER.currentUser?.lastname

        if accountModel != nil {
            setupData()
        }
        self.getCountries()
    }
    
    func setupData()
    {
        txtFieldFirstName.text = accountModel?.firstname
        txtFieldLastName.text = accountModel?.lastname
        txtFieldCountry.text = accountModel?.country
        countryId = Int(accountModel!.countryId!) ?? 0
        zoneId = accountModel!.zoneId
        getZoneService(countryId: countryId,showLoader: false)
        txtFieldCity.text = accountModel!.city
        txtFieldDistrict.text = accountModel!.customField.two
        txtFieldHouseNo.text = accountModel!.customField.five
        txtFieldStreetNo.text = accountModel!.customField.four
        txtFieldAddress1.text = accountModel!.address1
        txtFieldAddress2.text = accountModel!.address2
        txtFieldPostalCode.text = accountModel!.postcode
        btnSetAsDefClicked(accountModel!.defaultField ? btnSetAsDefYes : btnSetAsDefNo)
       
    }
    
    @IBAction func btnSetAsDefClicked(_ sender:UIButton) {
        reSetDefaultAddress()
        sender.isSelected = true
        sender.backgroundColor = UIColor.appThemeColor
    }
    
    @IBAction func btnAddressLableClicked(_ sender:UIButton){
        reSetAddressLabel()
        sender.isSelected = true
        sender.backgroundColor = UIColor.appThemeColor
    }
    
    func reSetDefaultAddress()
    {
        btnSetAsDefYes.backgroundColor = UIColor.clear
        btnSetAsDefNo.backgroundColor = UIColor.clear
        btnSetAsDefYes.isSelected = false
        btnSetAsDefNo.isSelected = false
    }
    
    func reSetAddressLabel()
    {
        btnAddressLableYes.backgroundColor = UIColor.clear
        btnAddressLableNo.backgroundColor = UIColor.clear
        btnAddressLableYes.isSelected = false
        btnAddressLableNo.isSelected = false
    }
    
    func setBorder(btn:UIButton){
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    func getCountries(){
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_COUNTRIES, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrCountries = responseData as! NSMutableArray
            if self.arrCountries.count > 0 {
                self.setupCountryData()
            }
            }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    func setupCountryData(){
        let arrList = NSMutableArray()
        if self.arrCountries.count > 0 {
            for str in self.arrCountries as [AnyObject]{
                let model  = str as! CountriesModel
                arrList.add(model.name)
             }
        }
        self.txtFieldCountry.actionSheetData = arrList

        txtFieldCountry.onDataSelectionSuccess { (data) in
            let arr = self.arrCountries.filter {
                (($0 as! CountriesModel).name == data)
            }
            if arr.count > 0 {
                let model = arr[0] as! CountriesModel
                self.countryId = model.countryId
                VIEWMANAGER.hideActivityIndicator()
                self.getZoneService(countryId: model.countryId)
            }
        }
    }
    
    func getZoneService(countryId:Int,showLoader : Bool = true){
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        let api = "\(GET_ZONE)\(countryId)"
        RemoteAPI().callPOSTApi(apiName: api, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.zoneModel = responseData as? ZoneModel
            for zModel in self.zoneModel.zone{
                self.arrZones.add(zModel.name)
            }
            
            if self.zoneId.count > 0 {
                self.txtFieldZone.text = self.getZoneByZoneId(zoneId: self.accountModel!.zoneId)
            }

            self.txtFieldZone.dropDownData = self.arrZones
            
            self.txtFieldZone.onDropDownSelectionSuccess(success: { (data) in
                let arr = self.zoneModel.zone.filter {
                    (($0).name == data)
                }
                if arr.count > 0 {
                    let model = arr[0]
                    self.zoneId = model.zoneId
                }
            })
            
            }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    func getZoneByZoneId(zoneId : String) -> String {
        let arr = self.zoneModel.zone.filter {
            (($0).zoneId == zoneId)
        }
        if arr.count > 0 {
            let model = arr[0]
            return model.name
        }
        return ""
    }
    
    func addAddressService()
        
    {
        let request = [
            "firstname": txtFieldFirstName.text!,
            "lastname": txtFieldLastName.text!,
            "city": txtFieldCity.text!,
            "address_1": txtFieldAddress1.text!,
            "address_2": txtFieldAddress2.text!,
            "country_id": self.countryId,
            "postcode": txtFieldPostalCode.text!,
            "zone_id": zoneId,
            "custom_field": [
                "address": [
                    "2": txtFieldDistrict.text!,
                    "3": txtFieldZone.text!,
                    "4": txtFieldStreetNo.text!,
                    "5": txtFieldHouseNo.text!,
                ]
            ],
            "default": NSNumber(booleanLiteral: btnSetAsDefYes.isSelected)
    ] as [String : Any]
        
        let param = NSMutableDictionary(dictionary: request)
        var api = ADD_ACCOUNT_ADDRESS
        if accountModel != nil  {
            api = "\(GET_ACCOUNT_ADDRESS_LIST)/\(accountModel!.addressId!)"
        }
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: api, params: param as? [String : AnyObject], apiType:accountModel != nil ? .put : .post, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.view.makeToast(localize(string:"Address saved successfuly."))
            if self.addAddrSuccess != nil {
                self.addAddrSuccess!()
            }
            self.navigationController?.popViewController(animated: true)
        }) { (errMsg) in
            VIEWMANAGER.showToast(errMsg)
            
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    @IBAction func btnSaveAddressClicked(_ sender: Any) {
        if validate(){
            addAddressService()
        }
    }
    
    func validate() -> Bool {
        if txtFieldFirstName.text!.count == 0 {
            self.view.makeToast(localize(string:"Please enter your first name."))
            return false
        }
        
        else if txtFieldFirstName.text!.count > 32 {
            self.view.makeToast(localize(string:"First Name must be between 1 and 32 characters!"))
            return false
        }
        
        if txtFieldLastName.text!.count == 0 {
            self.view.makeToast(localize(string:"Please enter your last name."))
            return false
        }
        
        else if txtFieldLastName.text!.count > 32 {
            self.view.makeToast(localize(string:"Last Name must be between 1 and 32 characters!"))
            return false
        }
        if txtFieldCountry.text!.count == 0 {
            self.view.makeToast(localize(string:"Please select country"))
            return false
        }
        if txtFieldZone.text!.count == 0 {
            self.view.makeToast(localize(string:"Please select zone"))
            return false
        }
        else if txtFieldCity.text!.count == 0 {
            self.view.makeToast(localize(string:"Please enter your city"))
            return false
        }
        else if txtFieldDistrict.text!.count == 0 {
            self.view.makeToast(localize(string:"Please enter your district"))
            return false
        }
        else if txtFieldStreetNo.text!.count == 0 {
            self.view.makeToast(localize(string:"Please enter your Street No."))
            return false
        }
        else if txtFieldHouseNo.text!.count == 0 {
            self.view.makeToast(localize(string:"Please enter House No."))
            return false
        }
        else if txtFieldAddress1.text!.count == 0 {
            self.view.makeToast(localize(string:"Please enter address line 1"))
            return false
        }
//        else if txtFieldAddress2.text!.count == 0 {
//            self.view.makeToast(localize(string:"Please enter address line 2"))
//            return false
//        }
        return true
    }
    
    func onAddressAddedSuccess(closure:@escaping ()->())
    {
        addAddrSuccess = closure
    }

}
