//
//  SlidingMenuController.swift
//  Souq
//
//  Created by iLoma on 18/01/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

extension UIApplication {
    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
}

class SlidingMenuController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var arrCategoryMenu = NSArray()
    
    var arrMenu = [String]()
    var menuImgs = [String]()
    @IBOutlet weak var tblViewHtConstr: NSLayoutConstraint!
    
    @IBOutlet weak var topLogo: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblHome: UILabel!
    
    @IBOutlet weak var tableViewSliding: UITableView!
    
    @IBOutlet weak var menuBeforeLogin: UIView!
    @IBOutlet weak var menuAfterLogin: UIView!
    
    @IBOutlet weak var lblCartCount: UILabel!
    
    @IBOutlet weak var viewDetailsHtConstr: NSLayoutConstraint!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserEmail: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if VIEWMANAGER.currentUser != nil {
            arrMenu = ["LANGUAGE","CONTACT US","HELP","RATE US","LOGOUT","SELL ON SOUQ366"]
            menuImgs = ["ic_language","ic_contact_us","ic_help","ic_blank_star","ic_signout",""]
            menuBeforeLogin.isHidden = true
            menuAfterLogin.isHidden = false
            viewDetailsHtConstr.constant = 40
            lblUserName.text = "\(localize(string: "Hello")) \(VIEWMANAGER.currentUser!.firstname!) \(VIEWMANAGER.currentUser!.lastname!)"
            lblUserEmail.text = VIEWMANAGER.currentUser?.email
        }
        else {
            arrMenu = ["LANGUAGE","CONTACT US","HELP","RATE US","SELL ON SOUQ366"]
            menuImgs = ["ic_language","ic_contact_us","ic_help","ic_blank_star",""]
            menuBeforeLogin.isHidden = false
            menuAfterLogin.isHidden = true
            viewDetailsHtConstr.constant = 0
        }
        
        if VIEWMANAGER.arrCategoriesData.count > 0 {
            arrCategoryMenu = VIEWMANAGER.arrCategoriesData
            tableViewSliding.reloadData()
        }
        if Preferences.getCartCount() != 0{
            let cartCount = Preferences.getCartCount()
            lblCartCount.text = "\(cartCount!)"
            lblCartCount.isHidden = false
        }else{
            lblCartCount.isHidden = true
        }
        lblCartCount.textAlignment = .center
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        topLogo.image = UIImage(named: "logo")?.withRenderingMode(.alwaysTemplate)
        topLogo.tintColor = UIColor.appThemeColor
        
       // tableViewSliding.register(UITableViewCell.self, forCellReuseIdentifier: "DrawerCell")
        tableViewSliding.delegate = self
        tableViewSliding.dataSource  = self
        tableViewSliding.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblViewHtConstr.constant = tableViewSliding.contentSize.height
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? arrCategoryMenu.count : arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 1))
        footer.backgroundColor = UIColor.white
        return footer
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "DrawerCell")
        if cell == nil {
            cell = UITableViewCell(style: .value1, reuseIdentifier: "DrawerCell")
        }
        cell?.textLabel?.font = UIFont.appRegularFont(size: 15)
        cell?.detailTextLabel?.font = UIFont.appRegularFont(size: 14)
        if indexPath.section == 0 {
            let model = arrCategoryMenu[indexPath.row] as! CategoriesModel
            cell?.textLabel?.text = model.name
            cell?.imageView?.image = nil
            cell?.accessoryType = .disclosureIndicator
        }
        else{
            
            cell?.detailTextLabel?.text = indexPath.row == 0 ? (Preferences.getAppLanguage() == ENGLISH ? "عربى" : "English") : ""
            cell?.textLabel?.text = localize(string: arrMenu[indexPath.row])
            cell?.imageView?.image = UIImage(named: menuImgs[indexPath.row])
            cell?.accessoryType = .none
        }
    
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = self.parent as! KYDrawerController
        controller.setDrawerState(.closed, animated: true)
        
        if indexPath.section == 1 && indexPath.row == 0 {
            if Preferences.getAppLanguage() == ENGLISH {
                Preferences.setAppLanguage(lang: ARABIC)
            }
            else {
                Preferences.setAppLanguage(lang: ENGLISH)
            }
            app_del.setSplashAsRoot()
        }
        else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: DRAWER_SELECTION_NOTIFICATION), object: indexPath)
        }
    }
    
    
    
    @IBAction func btnLogoClicked(_ sender: Any) {
        btnCloseDrawerClicked(nil)
        
        if let topVC = UIApplication.getTopMostViewController() {
            print(topVC)
            
            if topVC is KYDrawerController{
                let drawer = topVC as! KYDrawerController
                print(drawer.mainViewController)
                if drawer.mainViewController is UINavigationController{
                    let nav = drawer.mainViewController as! UINavigationController
                    nav.popToRootViewController(animated: true)
                }
            }
            
        }
    }
    

    @IBAction func btnSignInClicked(_ sender: Any) {
        btnCloseDrawerClicked(nil)
        let loginVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        VIEWMANAGER.topMostController().present(loginVC, animated: true, completion: nil)
    }
    
    @IBAction func btnSignupClicked(_ sender: Any) {
        btnCloseDrawerClicked(nil)
        let signupVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "SignUpController") as! SignUpController
        let navVC = UINavigationController(rootViewController: signupVC)
        VIEWMANAGER.topMostController().present(navVC, animated: true, completion: nil)
    }
    
    @IBAction func btnCartClicked(_ sender: Any) {
        btnCloseDrawerClicked(nil)
        let cartVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        VIEWMANAGER.topMostController().navigationController?.pushViewController(cartVC, animated: true)
    }
    
    @IBAction func btnHomeClicked(_ sender: Any) {
        btnCloseDrawerClicked(nil)
        
        
        if let topVC = UIApplication.getTopMostViewController() {
            print(topVC)
            
            if topVC is KYDrawerController{
                let drawer = topVC as! KYDrawerController
                 print(drawer.mainViewController)
                if drawer.mainViewController is UINavigationController{
                    let nav = drawer.mainViewController as! UINavigationController
                    nav.popToRootViewController(animated: true)
                }
            }
            
        }
        
    }
    
    @IBAction func btnCloseDrawerClicked(_ sender: Any?) {
        let controller = self.parent as! KYDrawerController
        controller.setDrawerState(.closed, animated: true)
    }
    
    
    @IBAction func btnAccountClicked(_ sender: Any) {
        btnCloseDrawerClicked(nil)
        let accountVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
        VIEWMANAGER.topMostController().navigationController?.pushViewController(accountVC, animated: true)
    }
    
    @IBAction func btnWishlistClicked(_ sender: Any) {
        btnCloseDrawerClicked(nil)
        let wishlistVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "MyWishlistController") as! MyWishlistController
        VIEWMANAGER.topMostController().navigationController?.pushViewController(wishlistVC, animated: true)
    }
    
    @IBAction func btnTermsOfUseClicked(_ sender: Any) {
        
        showWebViewController(title: "Terms and Conditions")
    }
  
    @IBAction func btnReturnPolicyClicked(_ sender: Any) {
        showWebViewController(title:"Return Policy")
    }
    
  
    @IBAction func btnPrivacyPolicyClicked(_ sender: Any) {
        
        showWebViewController(title:  "Privacy Policy")
    }
    
    @IBAction func btnWarrantyPolicyClicked(_ sender: Any) {
        showWebViewController(title: "Warranty Policy")
    }
    
    @IBAction func btnDEliveryInfoClicked(_ sender: Any) {
        showWebViewController(title: "Delivery Information")
    }
    
    @IBAction func btnFAQsClicked(_ sender: Any) {
        showWebViewController(title:  "FAQ's")
    }
    
    @IBAction func btnFacebookClicked(_ sender: Any) {
        guard let url = URL(string: FACEBOOK_LINK) else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func btnInstgramClicked(_ sender: Any) {
        guard let url = URL(string: INSTA_LINK) else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func btnYOutubeClicked(_ sender: Any) {
        guard let url = URL(string: YOUTUBE_LINK) else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func btnSnapchatClicked(_ sender: Any) {
        guard let url = URL(string: SNAPCHAT_LINK) else { return }
        UIApplication.shared.open(url)
    }
    
    func showWebViewController(title:String)
    {
        btnCloseDrawerClicked(nil)
        let webVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webVC.title = title
        VIEWMANAGER.topMostController().navigationController?.pushViewController(webVC, animated: true)
    }
    deinit {
        tableViewSliding.removeObserver(self, forKeyPath: "contentSize")
    }

}
