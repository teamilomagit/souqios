//
//  ViewController.swift
//  Souq
//
//  Created by iLoma Technology on 16/01/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import FBSDKCoreKit
import GoogleSignIn
class LoginController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate {
    
    var loginSuccess : (()->())?
    
    @IBOutlet weak var topLogo: UIImageView!
    @IBOutlet weak var txtFieldLogin: CustomTextField!
    @IBOutlet weak var txtFieldPassword: CustomTextField!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnGoogle: GIDSignInButton!
    //Facebook
    var strFbAccessToken : String!
    var strFbFirstName : String!
    var strFbLastName : String!
    var strFbEmail : String!
    //Google
    var strGoogleAccessToken : String!
    var strGoogleFirstName : String!
    var strGoogleLastName : String!
    var strGoogleEmail : String!
    
    

    
    
    var fromSignup : Bool = false
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        topLogo.image = UIImage(named: "logo")?.withRenderingMode(.alwaysTemplate)
        topLogo.tintColor = UIColor.appThemeColor

        txtFieldPassword.setTextFieldTag(tag: .TAG_PASSWORD)
       // txtFieldLogin.text = "mayur@gmail.com"
       // txtFieldPassword.text = "123456"
        btnFacebook.addTarget(self, action: #selector(self.btnFacebookClicked), for: .touchUpInside)
        btnGoogle.addTarget(self, action: #selector(btnGoogleClicked(_ :)), for: .touchUpInside)
    }

    func loginService() {
        let param = ["email": txtFieldLogin.text,
                     "password": txtFieldPassword.text,
                     ] as [String : AnyObject]
        VIEWMANAGER.showActivityIndicator(hardLoader: true)
        RemoteAPI().callPOSTApi(apiName:LOGIN_URL, params: param, completion: { (responseData) in
            let model: LoginModel? = responseData as? LoginModel
            VIEWMANAGER.currentUser = model
           // app_del.setDashboardAsRoot()
            Preferences.saveCartCount(count: model!.cartCountProducts)

            self.btnCloseClicked(nil)
            if self.loginSuccess != nil {
                self.loginSuccess!()
            }
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    @IBAction func btnLoginClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            loginService()
        }
    }
    
    func validate() -> Bool
    {
        if txtFieldLogin.text?.trimString().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter registered email address"))
            return false
        }
        if !txtFieldLogin.text!.isValidEmail() {
            VIEWMANAGER.showToast(localize(string: "Please enter valid email address"))
            return false
        }
        if txtFieldPassword.text!.count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter password"))
            return false
        }
        return true
    }

    @IBAction func btnSignUpClicked(_ sender: Any) {
        if fromSignup {
            self.dismiss(animated: true, completion: nil)
            return
        }
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let signUP = storyboard.instantiateViewController(withIdentifier: "SignUpController") as! SignUpController
        signUP.fromSignIn = true
        let navVC = UINavigationController(rootViewController: signUP)
        self.present(navVC, animated: true) {
        }
    }
        
    @IBAction func btnForgotPasswordClicked(_ sender: Any) {
        let forgotVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ForgotPasswordController") as! ForgotPasswordController
        forgotVC.modalTransitionStyle = .crossDissolve
        self.present(forgotVC, animated: true, completion: nil)
    }
    
    @IBAction func btnCloseClicked(_ sender: Any?) {
        app_del.window?.rootViewController?.dismiss(animated: true, completion: nil)

       // self.dismiss(animated: true, completion: nil)
    }
    
    func onLoginSuccess(closure:@escaping ()->()) {
        loginSuccess = closure
    }
  
    
    @objc func btnFacebookClicked() {
        let loginManager = LoginManager()
        loginManager.loginBehavior = .native
        loginManager.logIn(readPermissions: [.publicProfile,.email], viewController: self) { (result) in
                switch result {
                case .failed(let error):
                    
                    print(error)
                case .cancelled:
                    print("User cancelled login.")
                case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                    self.strFbAccessToken = accessToken.authenticationToken
                    print("Logged in!")
                    self.getFBUserInfo(accessToken: self.strFbAccessToken)
                   
            }
        
        }
        }
        
    func getFBUserInfo(accessToken:String) {
            VIEWMANAGER.showActivityIndicator(hardLoader: true)
           let request = GraphRequest(graphPath: "me", parameters: ["fields":"email,first_name,last_name"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: FacebookCore.GraphAPIVersion.defaultVersion)
            request.start { (response, result) in
                VIEWMANAGER.hideActivityIndicator()
switch result {
                 case .success(let value):
                    print(value.dictionaryValue!)
                    self.strFbEmail = (value.dictionaryValue!["email"] as! String)
                    self.strFbFirstName = (value.dictionaryValue!["first_name"] as! String)
                    self.strFbLastName = (value.dictionaryValue!["last_name"] as! String)
                    self.socialLoginService(provider: "facebook")
    
                    case .failed(let error):
                    VIEWMANAGER.hideActivityIndicator()
                    print(error)
                }
            }
        }

    
    func socialLoginService(provider:String) {
        var param = [String : AnyObject]()
        if provider == "facebook"{
            param = ["access_token": strFbAccessToken,
                     "email": strFbEmail,
                     "provider": provider,
                     "firstname": strFbFirstName,
                     "lastname": strFbLastName,
                     ] as [String : AnyObject]
        }else{
            param = ["access_token": strGoogleAccessToken,
                     "email": strGoogleEmail,
                     "provider": provider,
                     "firstname": strGoogleFirstName,
                     "lastname": strGoogleLastName,
                     ] as [String : AnyObject]
        }
        
        VIEWMANAGER.showActivityIndicator(hardLoader: true)
        RemoteAPI().callPOSTApi(apiName:SOCIAL_LOGIN_URL, params: param, completion: { (responseData) in
            let model: LoginModel? = responseData as? LoginModel
            VIEWMANAGER.currentUser = model
            //app_del.setDashboardAsRoot()
            //Preferences.saveCartCount(count: model!.cartCountProducts)
            
            self.btnCloseClicked(nil)
            if self.loginSuccess != nil {
                self.loginSuccess!()
            }
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    @objc func btnGoogleClicked(_ sender: UIButton){
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error{
            print("We have error sign in user == \(error.localizedDescription)")
        }else{
            if let gmailUser = user{
                strGoogleEmail = gmailUser.profile.email
                //let userId = user.userID
                strGoogleAccessToken = user.authentication.idToken
                let fullName = user.profile.name
                var components = fullName!.components(separatedBy: " ")
                if(components.count > 0)
                {
                    strGoogleFirstName = components.removeFirst()
                    strGoogleLastName = components.joined(separator: " ")
                   
                }
                
                //let givenName = user.profile.givenName
                //let familyName = user.profile.familyName
                //let email = user.profile.email
                self.socialLoginService(provider: "google")

            }
        }
       
    }
}

