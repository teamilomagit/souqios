//
//  AccoundAddressListController.swift
//  Souq
//
//  Created by Pawan Ramteke on 16/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class AddressListController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblViewAddressList: UITableView!
    var arrAddressList = NSMutableArray()
    var arrCustomFields = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = localize(string: "ADDRESS")
        tblViewAddressList.register(UINib(nibName: "AccountAddressListCell", bundle: nil), forCellReuseIdentifier: "AccountAddressListCell")
        getAccountAddressListService()
    }
    
    func getAccountAddressListService(){
                
        VIEWMANAGER.showActivityIndicator(self)
        RemoteAPI().callPOSTApi(apiName: GET_ACCOUNT_ADDRESS_LIST, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrAddressList = NSMutableArray(array: responseData as! NSArray)
            self.tblViewAddressList.backgroundView = self.arrAddressList.count > 0 ? nil : self.tableBGView()

            self.tblViewAddressList.reloadData()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
        }
    }
    
    func deleteAddreeService(indexPath : IndexPath) {
        let addressModel = self.arrAddressList[indexPath.row] as! AddressModel
        let api = "\(GET_ACCOUNT_ADDRESS_LIST)/\(addressModel.addressId!)"
        
        VIEWMANAGER.showActivityIndicator(hardLoader:true)
        RemoteAPI().callPOSTApi(apiName: api, params: nil, apiType: .delete, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.arrAddressList.removeObject(at: indexPath.row)
            self.tblViewAddressList.reloadData()
            self.tblViewAddressList.backgroundView = self.arrAddressList.count > 0 ? nil : self.tableBGView()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func tableBGView() -> UIView
    {
        let viewBG = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: tblViewAddressList.frame.size.height))
        viewBG.backgroundColor = UIColor.clear
        
        
        let imgView = UIImageView()
        imgView.image = UIImage(named: "")
        viewBG.addSubview(imgView)
        imgView.enableAutoLayout()
        imgView.centerX()
        imgView.topMargin(pixels: 10)
        imgView.fixedWidth(pixels: SCREEN_HEIGHT * 0.4)
        imgView.fixedHeight(pixels: SCREEN_HEIGHT * 0.4)
        
        let lblEmptyTitle = UILabel()
        lblEmptyTitle.font = UIFont.appBoldFont(size: 16)
        lblEmptyTitle.text = localize(string: localize(string: "Your address book looks empty"))
        lblEmptyTitle.textAlignment = .center
        viewBG.addSubview(lblEmptyTitle)
        lblEmptyTitle.enableAutoLayout()
        lblEmptyTitle.leadingMargin(pixels: 10)
        lblEmptyTitle.trailingMargin(pixels: 10)
        lblEmptyTitle.belowToView(view: imgView, pixels: 10)
        
        let lblSubTitle = UILabel()
        lblSubTitle.font = UIFont.appRegularFont(size: 14)
        lblSubTitle.textColor = UIColor.lightGray
        lblSubTitle.text = localize(string: localize(string: localize(string: "What are you waiting for?")))
        lblSubTitle.textAlignment = .center
        viewBG.addSubview(lblSubTitle)
        lblSubTitle.enableAutoLayout()
        lblSubTitle.leadingMargin(pixels: 10)
        lblSubTitle.trailingMargin(pixels: 10)
        lblSubTitle.belowToView(view: lblEmptyTitle, pixels: 10)
        
        return viewBG
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountAddressListCell") as! AccountAddressListCell
        let addressModel = arrAddressList[indexPath.row] as! AddressModel
        cell.lblZone.text = addressModel.zone!
        cell.lblAddress.text = "\(addressModel.address1!)" + " " + "\(addressModel.address2!)"
       // cell.lblContactNumber.text = addressModel.postcode
        cell.indexPath = indexPath
        
        cell.onEditAddressClicked { (indexPath) in
            let addressModel = self.arrAddressList[indexPath.row] as! AddressModel
            self.pushToAddNewAddressVC(addressModel)
        }
        
        cell.onDeleteAddressClicked { (indexPath) in
            CustomAlertView.showAlertWithYesNo(title: "", messsage: localize(string: "Are you sure want to delete?"), completion: {
                self.deleteAddreeService(indexPath: indexPath)
            })
        }
        return cell
    }

    @IBAction func btnAddNewAddressClicked(_ sender: Any) {
        pushToAddNewAddressVC(nil)
    }
    
    func pushToAddNewAddressVC(_ model : AddressModel?) {
        let addAddressVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "AddNewAddressController") as! AddNewAddressController
        addAddressVC.accountModel = model
        self.navigationController?.pushViewController(addAddressVC, animated: true)
        addAddressVC.onAddressAddedSuccess {
            self.getAccountAddressListService()
        }
    }
}
