//
//  AppDelegate.swift
//  Souq
//
//  Created by iLoma Technology on 16/01/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FacebookCore
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = GOOGLE_CLIENT_ID

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = localize(string: "Done")
        let dict = Preferences.getLoginResponse()
        if dict != nil {
            let model = LoginModel(fromDictionary: dict as! [String : Any])
            VIEWMANAGER.currentUser = model
        }
        setupNavigationBar()
        setSplashAsRoot()
        return true
    }
    
    func setOnboardControllerAsRoot()
    {
        let navVC = UINavigationController(rootViewController: OnboardingController())
        UIView.transition(with: window!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = navVC
        }, completion: { completed in
        })
        window?.makeKeyAndVisible()
    }
    
    func setSplashAsRoot()
    {
        
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = localize(string: "Done")

        performOnLanguageChange()

        let splashVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "SplashViewController") as! SplashViewController
        
        UIView.transition(with: window!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = splashVC
        }, completion: { completed in
        })
        window?.makeKeyAndVisible()
        
    }
    
    func setDashboardAsRoot()
    {
        
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = localize(string: "Done")

        let homepageVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "HompageController") as! HompageController
        let drawerViewController = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "SlidingMenuController")
        
        let drawerController = KYDrawerController(drawerDirection: Preferences.getAppLanguage() == ENGLISH ? KYDrawerController.DrawerDirection.left : KYDrawerController.DrawerDirection.right, drawerWidth: SCREEN_WIDTH)
        drawerController.mainViewController = UINavigationController(rootViewController: homepageVC)
        drawerController.drawerViewController = drawerViewController
        
        UIView.transition(with: window!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.window?.rootViewController = drawerController
        }, completion: { completed in
        })
        window?.makeKeyAndVisible()
    }
    
    func setProductListingasRoot()
    {
        let listingVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ProductListingController") as! ProductListingController
        window?.rootViewController = listingVC
        window?.makeKeyAndVisible()
    }
    
    func performOnLanguageChange()
    {
        if Preferences.getAppLanguage() == ARABIC {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().textAlignment = .right
            if UILabel.appearance().textAlignment != .center {
                UILabel.appearance().textAlignment = .right
            }
            UITextView.appearance().textAlignment = .right
        }
        else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UITextField.appearance().textAlignment = .left
            if UILabel.appearance().textAlignment != .center {
                UILabel.appearance().textAlignment = .left
            }
            UITextView.appearance().textAlignment = .left
        }
    }
    
    func setupNavigationBar()
    {
        let navigationBar = UINavigationBar.appearance()
        navigationBar.isTranslucent = false
        navigationBar.barStyle = .default
        navigationBar.tintColor = UIColor.white
        navigationBar.barTintColor = UIColor.appThemeColor
        
        navigationBar.tintColor = UIColor.white
        navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.font : UIFont.appMediumFont(size: 18)]
        
        let back = UIImage(named:"ic_back")
        navigationBar.backIndicatorImage = back;
        navigationBar.backIndicatorTransitionMaskImage = back;
        
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.clear], for: .normal)
        
    }
    
    func getProductCategoriesAPI(completion:(()->())?)
    {
        RemoteAPI().callPOSTApi(apiName: PRODUCT_CATEGORIES_API, params: nil, apiType: .get, completion: { (responseData) in
            VIEWMANAGER.arrCategoriesData = responseData as! NSArray
            if completion != nil {
                completion!()
            }
        }) { (errMsg) in
            
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url as URL?,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

