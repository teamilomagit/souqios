//
//  Preferences.swift
//  FitnessMaa
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import UIKit
let USER_DEFAULTS  = UserDefaults.standard
class Preferences
{
    class func saveLoginResponse(response: NSDictionary) {
        let data = NSKeyedArchiver.archivedData(withRootObject: response)
        USER_DEFAULTS.set(data, forKey: "LoginResponse")
        USER_DEFAULTS.synchronize()
    }
    
    class func getLoginResponse() -> NSDictionary? {
        let data = USER_DEFAULTS.object(forKey: "LoginResponse")
        if data == nil {
            return nil
        }
        let loginDict = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
        return loginDict as? NSDictionary
    }
    
    class func clearLoginData()
    {
        USER_DEFAULTS.removeObject(forKey: "LoginResponse")
        USER_DEFAULTS.removeObject(forKey: "CountKey")
        USER_DEFAULTS.synchronize()
    }
    
    class func saveSessionKey(response: String) {
        USER_DEFAULTS.set(response, forKey: "SessionKey")
        USER_DEFAULTS.synchronize()
    }
    
    class func getSessionKey() -> String? {
        
        let sessionKey = USER_DEFAULTS.object(forKey: "SessionKey")
        return sessionKey as? String
    }
    class func saveCartCount(count: Int) {
        USER_DEFAULTS.set(count, forKey: "CountKey")
        USER_DEFAULTS.synchronize()
    }
    
    class func getCartCount() -> Int? {
        let countKey = USER_DEFAULTS.integer(forKey: "CountKey")
        return countKey
    }
    
    class func setAppLanguage(lang:String) {
        USER_DEFAULTS.set(lang, forKey: "AppLanguage")
        USER_DEFAULTS.synchronize()
    }
    
    class func getAppLanguage() -> String
    {
        let lang = USER_DEFAULTS.value(forKey: "AppLanguage")
        if lang == nil {
            return ENGLISH
        }
        return lang as! String
    }
    
    class func saveOnbording(response: Bool) {
        USER_DEFAULTS.set(response, forKey: "OnboardingScreen")
        USER_DEFAULTS.synchronize()
    }
    
    class func getOnbording() -> Bool {
        
        let sessionKey = USER_DEFAULTS.bool(forKey: "OnboardingScreen")
        return sessionKey
    }
}
