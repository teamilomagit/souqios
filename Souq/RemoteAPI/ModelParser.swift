//
//  ModelParser.swift
//  FitnessMaa
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import UIKit
class ModelParser {
    
    func modelParserWithData(apiURL:String,responseData:AnyObject)->AnyObject
    {
        if apiURL == GET_SESSION_API   {
            return parseSessionKeyData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == LOGIN_URL || apiURL == REGISTRATION_API || apiURL == SOCIAL_LOGIN_URL {
            return parseLoginData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_BANNER_API  {
            return parseAppBannersData(responseData: responseData as! NSArray) as AnyObject
        }
        if apiURL.contains(GET_BANNER_IMAGES_API)  {
            return parseBannerImagesData(responseData: responseData as! NSArray) as AnyObject
        }
        
        if apiURL.contains(GET_PRODUCT_DETAILS_API) {
            return parseProductDetailsData(responseData: responseData as! NSDictionary) as AnyObject
        }
        
        if apiURL.contains(GET_PRODUCT_LISTING_API) || apiURL.contains(DEALS_OF_THE_DAY_API) || apiURL.contains(SIMILAR_PRODUCT_LISTING_API)  {
            return parseProductListingData(responseData: responseData as! NSArray) as AnyObject
        }
 
        if apiURL == PRODUCT_CATEGORIES_API {
            return parseProductCategoriesData(responseData: responseData as! NSArray) as AnyObject
        }
        if apiURL == GET_ACCOUNT_ADDRESS_LIST  {
            if responseData is NSArray {
                return [] as AnyObject
            }
            return parseAccountAddressListData(responseData: responseData["addresses"] as! NSArray) as AnyObject
        }
        if apiURL == ADD_PRODUCT_TO_CART  {
            if responseData is [String : Any] {
                return parseAddProductToListData(responseData: responseData as! NSDictionary) as AnyObject
            }
        }
        if apiURL == GET_CART_PRODUCTS  {
    
            return parseProductCartListData(responseData:responseData is NSArray ? NSDictionary() : responseData as! NSDictionary) as AnyObject
        }
        
        if apiURL == REMOVE_ITEM_FROM_CART {
            return parseRemoveCartListData(responseData:responseData is NSArray ? NSDictionary() : responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_COUNTRIES  {
            return parseCountriesListData(responseData: responseData as! NSArray) as AnyObject
        }
        if apiURL.contains(GET_ZONE)  {
            return parseZoneListData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == MY_WISHLIST_API {
            return parseMyWishlistData(responseData: responseData as! NSArray) as AnyObject
        }
        if apiURL == GET_SHIPPING_METHODS_API {
            if responseData is NSArray {
                return [] as AnyObject
            }
            return parseShippingMethodData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_PAYMENT_METHODS_API {
            if responseData is NSArray {
                return [] as AnyObject
            }
            return parsePaymentMethodData(responseData: responseData as! NSDictionary) as AnyObject
        }
        
        if apiURL == PLACE_ORDER_API {
            if let response = responseData as? NSDictionary {
                return response.value(forKey: "order_id") as AnyObject
            }
        }
        if apiURL.contains(ORDER_HISTORY_DETAILS_API) {
            return parseOrderHistoryDetailsData(responseData: responseData as! NSDictionary) as AnyObject
        }
        
        if apiURL.contains(ORDER_HISTORY_API) {
            return parseOrderHistoryListData(responseData: responseData as! NSArray) as AnyObject
        }
        
        if apiURL == UPDATE_ACCOUNT {
            if responseData is NSArray{
                return [] as AnyObject
            }
            return parseAccountDetailsData(responseData: responseData as! NSDictionary) as AnyObject
        }
        
        if apiURL == GET_ZONE_SHIPPING_API {
            return parseZoneShippingData(responseData: responseData as! NSArray) as AnyObject
        }
        
        
        if apiURL.contains(GET_INFORMATION_TYPES_API) {
            
            if responseData is NSDictionary{
                return parseInfomationTypeDetailData(responseData: responseData as! NSDictionary) as AnyObject
            }
            else{
                return parseInfomationTypeData(responseData: responseData as! NSArray) as AnyObject
            }
        }
        
        return [] as AnyObject
    }
    
    func parseSessionKeyData(responseData: NSDictionary) -> Any? {
        let model = SessionKeyModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    func parseLoginData(responseData: NSDictionary) -> Any? {
        Preferences.saveLoginResponse(response: responseData)
        let model = LoginModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    func parseAppBannersData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = BannerTypeModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseBannerImagesData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = BannerImagesModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
        
    }
    func parseProductListingData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = ProductListingModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseProductCategoriesData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = CategoriesModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    func parseProductDetailsData(responseData:NSDictionary) -> Any?
    {
        let model = ProductDetailsModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    func parseAccountAddressListData(responseData:NSArray) -> NSArray?
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = AddressModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    func parseAddProductToListData(responseData:NSDictionary) -> Any
    {
        let model = AddToCartModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    func parseProductCartListData(responseData:NSDictionary) -> Any
    {
        let model = ViewCartProductModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    func parseRemoveCartListData(responseData:NSDictionary) -> Any
    {
        let model = RemoveCartModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    func parseCountriesListData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = CountriesModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    func parseZoneListData(responseData:NSDictionary) -> Any
    {
        let model = ZoneModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    func parseMyWishlistData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = WishlistModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseShippingMethodData(responseData:NSDictionary) -> ShippingMethodModel
    {
        let model = ShippingMethodModel.init(fromDictionary:responseData["shipping_methods"] as! [String : Any])
        return model
    }
    func parsePaymentMethodData(responseData:NSDictionary) -> PaymentMethodModel
    {
        let model = PaymentMethodModel.init(fromDictionary:responseData["payment_methods"] as! [String : Any])
        return model
    }
    func parseOrderHistoryListData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = OrderHistoryModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    func parseOrderHistoryDetailsData(responseData:NSDictionary) -> Any?
    {
        let model = OrderHistoryDetailsModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    func parseAccountDetailsData(responseData:NSDictionary) -> Any?
    {
        let model = AccountDetailModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    func parseZoneShippingData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = ZoneShippingModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    
    func parseInfomationTypeData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = InformationType(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseInfomationTypeDetailData(responseData:NSDictionary) -> InformationType
    {
    
        let model = InformationType(fromDictionary: responseData as! [String : Any])
        return model
    }
}
