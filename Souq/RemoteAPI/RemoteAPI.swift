//
//  RemoteAPI.swift
//  FitnessMaa
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import Alamofire

class RemoteAPI{
    
    var api = String()
    
    func callPOSTApi(apiName:String,params:[String: AnyObject]?,apiType:HTTPMethod = .post ,completion: @escaping (AnyObject) -> Void,failed:@escaping (_ errorMsg:String?) -> Void)
    {

        var headers: HTTPHeaders = [
            "X-Oc-Merchant-Id": MERCHANT_ID,
            "X-Oc-Merchant-Language" : Preferences.getAppLanguage() == ENGLISH ? "en-gb" : ARABIC
        ]
        if (Preferences.getSessionKey() != nil){
            let strSessionKey = Preferences.getSessionKey()
            headers = [
                "X-Oc-Merchant-Id": MERCHANT_ID,
                "X-Oc-Session": strSessionKey!,
                "X-Oc-Merchant-Language" : Preferences.getAppLanguage() == ENGLISH ? "en-gb" : ARABIC
            ]
            
        }

        var fullAPI = "\(BASE_URL)\(apiName)"
        
        if apiName.contains(GET_PRODUCT_DETAILS_API){
             fullAPI = "\(BASE_URL)\(apiName.replacingOccurrences(of: "_details", with: ""))"
        }
        else if apiName == (GET_CART_PRODUCTS){
            fullAPI = "\(BASE_URL)\(apiName.replacingOccurrences(of: "_details", with: ""))"
        }
        else if apiName == (REMOVE_ITEM_FROM_CART){
            fullAPI = "\(BASE_URL)\(apiName.replacingOccurrences(of: "_remove", with: ""))"
        }
        else if apiName == (ADD_ACCOUNT_ADDRESS){
            fullAPI = "\(BASE_URL)\(apiName.replacingOccurrences(of: "_add", with: ""))"
        }
        
        else if apiName.contains(FILTER_PRODUCT_LISTING_API) || apiName.contains(SIMILAR_PRODUCT_LISTING_API) {
            fullAPI = "\(BASE_URL.replacingOccurrences(of: "api/rest/", with: ""))\(apiName)"
        }
        else if apiName.contains(ORDER_HISTORY_DETAILS_API) {
            fullAPI = "\(BASE_URL)\(apiName.replacingOccurrences(of: "_details", with: ""))"
        }
        

       Alamofire.request(fullAPI, method: apiType, parameters: params, encoding: JSONEncoding.default,headers:headers).responseJSON{ response in

           // debugPrint(response)
            if let ERR = response.result.error
            {
                print(ERR);
                failed("Something is Wrong")
                return
            }

            if let JSON = response.result.value
            {
                let serverResponse = JSON as? NSDictionary
                
                
                if serverResponse?.value(forKey: "success") as! Int64 == 0
                {
                    let errData = serverResponse?.value(forKey: "error")
                    if errData is NSArray{
                        let data = errData as! NSArray
                        if data.count > 0 {
                            let strErr = data[0] as! String
                            failed(strErr)
                        }
                        return
                    }
                    failed(serverResponse?.value(forKey: "error") as? String)
                    
                        //completion(serverResponse?.value(forKey: "error") as AnyObject)
                    
                    return
                }else if serverResponse?.value(forKey: "success") as! Int64 == 1{
                    let obj = ModelParser().modelParserWithData(apiURL: apiName , responseData: serverResponse?.value(forKey: "data") as AnyObject)
                    completion(obj)
                }
            }
        }
    }
    }
    func uploadImageToServer(apiName:String,params:[String: AnyObject],completion: @escaping (AnyObject) -> Void,failed:@escaping (_ errorMsg:String?) -> Void)
    {
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            ]
        let fullAPI = "\(BASE_URL)\(apiName)"
        
        let url = URL(string: fullAPI)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append((params["gym_id"] as! String).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: "gym_id")
            let fileUrl = URL(fileURLWithPath:params["file_path"] as! String)
            multipartFormData.append(fileUrl, withName: "file_path")
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (encodingResult) in
            switch encodingResult
            {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    if let serverResponse = response.result.value as? NSDictionary {
                        if serverResponse.value(forKey: "status") as! String == "ERROR"
                        {
                            failed(serverResponse.value(forKey: "message") as? String)
                            return
                        }
                        let obj = ModelParser().modelParserWithData(apiURL: apiName, responseData: serverResponse.value(forKey: "response") as AnyObject)
                        completion(obj)
                    }
                    else{
                        failed("Something is wrong")
                        return
                        
                    }
                })
                break
                
            case .failure(let error):
                failed(error.localizedDescription)
                break
            }
        
    }
    
    func stopAllRequests()
    {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
}

