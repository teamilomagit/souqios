//
//  ViewCartProductModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 19, 2019

import Foundation


class ViewCartProductModel : NSObject, NSCoding{

    var coupon : String!
    var couponStatus : String!
    var currency : Currency!
    var hasDownload : Int!
    var hasRecurringProducts : Int!
    var hasShipping : Int!
    var products : [Product]!
    var reward : String!
    var rewardStatus : Bool!
    var total : String!
    var totalProductCount : Int!
    var totalRaw : Float!
    var totals : [Total]!
    var voucher : String!
    var voucherStatus : String!
    var vouchers : [AnyObject]!
    var weight : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        coupon = dictionary["coupon"] as? String
        couponStatus = dictionary["coupon_status"] as? String
        hasDownload = dictionary["has_download"] as? Int
        hasRecurringProducts = dictionary["has_recurring_products"] as? Int
        hasShipping = dictionary["has_shipping"] as? Int
        reward = dictionary["reward"] as? String
        rewardStatus = dictionary["reward_status"] as? Bool
        total = dictionary["total"] as? String
        totalProductCount = dictionary["total_product_count"] as? Int
        totalRaw = dictionary["total_raw"] as? Float
        voucher = dictionary["voucher"] as? String
        voucherStatus = dictionary["voucher_status"] as? String
        weight = dictionary["weight"] as? String
        if let currencyData = dictionary["currency"] as? [String:Any]{
            currency = Currency(fromDictionary: currencyData)
        }
        products = [Product]()
        if let productsArray = dictionary["products"] as? [[String:Any]]{
            for dic in productsArray{
                let value = Product(fromDictionary: dic)
                products.append(value)
            }
        }
        totals = [Total]()
        if let totalsArray = dictionary["totals"] as? [[String:Any]]{
            for dic in totalsArray{
                let value = Total(fromDictionary: dic)
                totals.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if coupon != nil{
            dictionary["coupon"] = coupon
        }
        if couponStatus != nil{
            dictionary["coupon_status"] = couponStatus
        }
        if hasDownload != nil{
            dictionary["has_download"] = hasDownload
        }
        if hasRecurringProducts != nil{
            dictionary["has_recurring_products"] = hasRecurringProducts
        }
        if hasShipping != nil{
            dictionary["has_shipping"] = hasShipping
        }
        if reward != nil{
            dictionary["reward"] = reward
        }
        if rewardStatus != nil{
            dictionary["reward_status"] = rewardStatus
        }
        if total != nil{
            dictionary["total"] = total
        }
        if totalProductCount != nil{
            dictionary["total_product_count"] = totalProductCount
        }
        if totalRaw != nil{
            dictionary["total_raw"] = totalRaw
        }
        if voucher != nil{
            dictionary["voucher"] = voucher
        }
        if voucherStatus != nil{
            dictionary["voucher_status"] = voucherStatus
        }
        if weight != nil{
            dictionary["weight"] = weight
        }
        if currency != nil{
            dictionary["currency"] = currency.toDictionary()
        }
        if products != nil{
            var dictionaryElements = [[String:Any]]()
            for productsElement in products {
                dictionaryElements.append(productsElement.toDictionary())
            }
            dictionary["products"] = dictionaryElements
        }
        if totals != nil{
            var dictionaryElements = [[String:Any]]()
            for totalsElement in totals {
                dictionaryElements.append(totalsElement.toDictionary())
            }
            dictionary["totals"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        coupon = aDecoder.decodeObject(forKey: "coupon") as? String
        couponStatus = aDecoder.decodeObject(forKey: "coupon_status") as? String
        currency = aDecoder.decodeObject(forKey: "currency") as? Currency
        hasDownload = aDecoder.decodeObject(forKey: "has_download") as? Int
        hasRecurringProducts = aDecoder.decodeObject(forKey: "has_recurring_products") as? Int
        hasShipping = aDecoder.decodeObject(forKey: "has_shipping") as? Int
        products = aDecoder.decodeObject(forKey: "products") as? [Product]
        reward = aDecoder.decodeObject(forKey: "reward") as? String
        rewardStatus = aDecoder.decodeObject(forKey: "reward_status") as? Bool
        total = aDecoder.decodeObject(forKey: "total") as? String
        totalProductCount = aDecoder.decodeObject(forKey: "total_product_count") as? Int
        totalRaw = aDecoder.decodeObject(forKey: "total_raw") as? Float
        totals = aDecoder.decodeObject(forKey: "totals") as? [Total]
        voucher = aDecoder.decodeObject(forKey: "voucher") as? String
        voucherStatus = aDecoder.decodeObject(forKey: "voucher_status") as? String
        vouchers = aDecoder.decodeObject(forKey: "vouchers") as? [AnyObject]
        weight = aDecoder.decodeObject(forKey: "weight") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if coupon != nil{
            aCoder.encode(coupon, forKey: "coupon")
        }
        if couponStatus != nil{
            aCoder.encode(couponStatus, forKey: "coupon_status")
        }
        if currency != nil{
            aCoder.encode(currency, forKey: "currency")
        }
        if hasDownload != nil{
            aCoder.encode(hasDownload, forKey: "has_download")
        }
        if hasRecurringProducts != nil{
            aCoder.encode(hasRecurringProducts, forKey: "has_recurring_products")
        }
        if hasShipping != nil{
            aCoder.encode(hasShipping, forKey: "has_shipping")
        }
        if products != nil{
            aCoder.encode(products, forKey: "products")
        }
        if reward != nil{
            aCoder.encode(reward, forKey: "reward")
        }
        if rewardStatus != nil{
            aCoder.encode(rewardStatus, forKey: "reward_status")
        }
        if total != nil{
            aCoder.encode(total, forKey: "total")
        }
        if totalProductCount != nil{
            aCoder.encode(totalProductCount, forKey: "total_product_count")
        }
        if totalRaw != nil{
            aCoder.encode(totalRaw, forKey: "total_raw")
        }
        if totals != nil{
            aCoder.encode(totals, forKey: "totals")
        }
        if voucher != nil{
            aCoder.encode(voucher, forKey: "voucher")
        }
        if voucherStatus != nil{
            aCoder.encode(voucherStatus, forKey: "voucher_status")
        }
        if vouchers != nil{
            aCoder.encode(vouchers, forKey: "vouchers")
        }
        if weight != nil{
            aCoder.encode(weight, forKey: "weight")
        }
    }
}
