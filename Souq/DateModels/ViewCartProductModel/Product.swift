//
//  Product.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 19, 2019

import Foundation


class Product : NSObject, NSCoding{

    var key : String!
    var model : String!
    var name : String!
    var option : [AnyObject]!
    var points : Int!
    var price : String!
    var priceRaw : Float!
    var productId : String!
    var quantity : String!
    var recurring : String!
    var reward : String!
    var stock : Bool!
    var thumb : String!
    var total : String!
    var totalRaw : Float!
    var isSelected : Bool = false
    var image : String!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        key = dictionary["key"] as? String
        model = dictionary["model"] as? String
        name = dictionary["name"] as? String
        points = dictionary["points"] as? Int
        price = dictionary["price"] as? String
        priceRaw = dictionary["price_raw"] as? Float
        productId = dictionary["product_id"] as? String
        quantity = dictionary["quantity"] as? String
        recurring = dictionary["recurring"] as? String
        reward = dictionary["reward"] as? String
        stock = dictionary["stock"] as? Bool
        thumb = dictionary["thumb"] as? String
        total = dictionary["total"] as? String
        totalRaw = dictionary["total_raw"] as? Float
        image = dictionary["image"] as? String

    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if key != nil{
            dictionary["key"] = key
        }
        if model != nil{
            dictionary["model"] = model
        }
        if name != nil{
            dictionary["name"] = name
        }
        if points != nil{
            dictionary["points"] = points
        }
        if price != nil{
            dictionary["price"] = price
        }
        if priceRaw != nil{
            dictionary["price_raw"] = priceRaw
        }
        if productId != nil{
            dictionary["product_id"] = productId
        }
        if quantity != nil{
            dictionary["quantity"] = quantity
        }
        if recurring != nil{
            dictionary["recurring"] = recurring
        }
        if reward != nil{
            dictionary["reward"] = reward
        }
        if stock != nil{
            dictionary["stock"] = stock
        }
        if thumb != nil{
            dictionary["thumb"] = thumb
        }
        if total != nil{
            dictionary["total"] = total
        }
        if totalRaw != nil{
            dictionary["total_raw"] = totalRaw
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        key = aDecoder.decodeObject(forKey: "key") as? String
        model = aDecoder.decodeObject(forKey: "model") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        option = aDecoder.decodeObject(forKey: "option") as? [AnyObject]
        points = aDecoder.decodeObject(forKey: "points") as? Int
        price = aDecoder.decodeObject(forKey: "price") as? String
        priceRaw = aDecoder.decodeObject(forKey: "price_raw") as? Float
        productId = aDecoder.decodeObject(forKey: "product_id") as? String
        quantity = aDecoder.decodeObject(forKey: "quantity") as? String
        recurring = aDecoder.decodeObject(forKey: "recurring") as? String
        reward = aDecoder.decodeObject(forKey: "reward") as? String
        stock = aDecoder.decodeObject(forKey: "stock") as? Bool
        thumb = aDecoder.decodeObject(forKey: "thumb") as? String
        total = aDecoder.decodeObject(forKey: "total") as? String
        totalRaw = aDecoder.decodeObject(forKey: "total_raw") as? Float
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if key != nil{
            aCoder.encode(key, forKey: "key")
        }
        if model != nil{
            aCoder.encode(model, forKey: "model")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if option != nil{
            aCoder.encode(option, forKey: "option")
        }
        if points != nil{
            aCoder.encode(points, forKey: "points")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if priceRaw != nil{
            aCoder.encode(priceRaw, forKey: "price_raw")
        }
        if productId != nil{
            aCoder.encode(productId, forKey: "product_id")
        }
        if quantity != nil{
            aCoder.encode(quantity, forKey: "quantity")
        }
        if recurring != nil{
            aCoder.encode(recurring, forKey: "recurring")
        }
        if reward != nil{
            aCoder.encode(reward, forKey: "reward")
        }
        if stock != nil{
            aCoder.encode(stock, forKey: "stock")
        }
        if thumb != nil{
            aCoder.encode(thumb, forKey: "thumb")
        }
        if total != nil{
            aCoder.encode(total, forKey: "total")
        }
        if totalRaw != nil{
            aCoder.encode(totalRaw, forKey: "total_raw")
        }
    }
}
