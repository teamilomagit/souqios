//
//  Total.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 19, 2019

import Foundation


class Total : NSObject, NSCoding{

    var text : String!
    var title : String!
    var value : Float!
    var strValue : String!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        text = dictionary["text"] as? String
        title = dictionary["title"] as? String
        value = dictionary["value"] as? Float
        strValue = dictionary["value"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if text != nil{
            dictionary["text"] = text
        }
        if title != nil{
            dictionary["title"] = title
        }
        if value != nil{
            dictionary["value"] = value
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        text = aDecoder.decodeObject(forKey: "text") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        value = aDecoder.decodeObject(forKey: "value") as? Float
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if text != nil{
            aCoder.encode(text, forKey: "text")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if value != nil{
            aCoder.encode(value, forKey: "value")
        }
    }
}
