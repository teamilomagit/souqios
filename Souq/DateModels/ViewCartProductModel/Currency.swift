//
//  Currency.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 19, 2019

import Foundation


class Currency : NSObject, NSCoding{

    var currencyId : String!
    var decimalPlace : String!
    var symbolLeft : String!
    var symbolRight : String!
    var value : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        currencyId = dictionary["currency_id"] as? String
        decimalPlace = dictionary["decimal_place"] as? String
        symbolLeft = dictionary["symbol_left"] as? String
        symbolRight = dictionary["symbol_right"] as? String
        value = dictionary["value"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if currencyId != nil{
            dictionary["currency_id"] = currencyId
        }
        if decimalPlace != nil{
            dictionary["decimal_place"] = decimalPlace
        }
        if symbolLeft != nil{
            dictionary["symbol_left"] = symbolLeft
        }
        if symbolRight != nil{
            dictionary["symbol_right"] = symbolRight
        }
        if value != nil{
            dictionary["value"] = value
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        currencyId = aDecoder.decodeObject(forKey: "currency_id") as? String
        decimalPlace = aDecoder.decodeObject(forKey: "decimal_place") as? String
        symbolLeft = aDecoder.decodeObject(forKey: "symbol_left") as? String
        symbolRight = aDecoder.decodeObject(forKey: "symbol_right") as? String
        value = aDecoder.decodeObject(forKey: "value") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if currencyId != nil{
            aCoder.encode(currencyId, forKey: "currency_id")
        }
        if decimalPlace != nil{
            aCoder.encode(decimalPlace, forKey: "decimal_place")
        }
        if symbolLeft != nil{
            aCoder.encode(symbolLeft, forKey: "symbol_left")
        }
        if symbolRight != nil{
            aCoder.encode(symbolRight, forKey: "symbol_right")
        }
        if value != nil{
            aCoder.encode(value, forKey: "value")
        }
    }
}