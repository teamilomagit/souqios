//
//  ImageBannerModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 31, 2019

import Foundation


class BannerTypeModel : NSObject, NSCoding{

    var bannerId : String!
    var checksum : String!
    var name : String!
    var status : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        bannerId = dictionary["banner_id"] as? String
        checksum = dictionary["checksum"] as? String
        name = dictionary["name"] as? String
        status = dictionary["status"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if bannerId != nil{
            dictionary["banner_id"] = bannerId
        }
        if checksum != nil{
            dictionary["checksum"] = checksum
        }
        if name != nil{
            dictionary["name"] = name
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        bannerId = aDecoder.decodeObject(forKey: "banner_id") as? String
        checksum = aDecoder.decodeObject(forKey: "checksum") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if bannerId != nil{
            aCoder.encode(bannerId, forKey: "banner_id")
        }
        if checksum != nil{
            aCoder.encode(checksum, forKey: "checksum")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
    }
}
