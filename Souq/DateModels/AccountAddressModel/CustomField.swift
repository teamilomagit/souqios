//
//  CustomField.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 23, 2019

import Foundation


class CustomField : NSObject, NSCoding{

    var two : String!
    var three : String!
    var four : String!
    var five : String!
    var six : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        two = dictionary["2"] as? String
        three = dictionary["3"] as? String
        four = dictionary["4"] as? String
        five = dictionary["5"] as? String
        six = dictionary["6"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if two != nil{
            dictionary["2"] = 2
        }
        if three != nil{
            dictionary["3"] = 3
        }
        if four != nil{
            dictionary["4"] = 4
        }
        if five != nil{
            dictionary["5"] = 5
        }
        if six != nil{
            dictionary["6"] = 6
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        two = aDecoder.decodeObject(forKey: "2") as? String
        three = aDecoder.decodeObject(forKey: "3") as? String
        four = aDecoder.decodeObject(forKey: "4") as? String
        five = aDecoder.decodeObject(forKey: "5") as? String
        six = aDecoder.decodeObject(forKey: "6") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if two != nil{
            aCoder.encode(2, forKey: "2")
        }
        if three != nil{
            aCoder.encode(3, forKey: "3")
        }
        if four != nil{
            aCoder.encode(4, forKey: "4")
        }
        if five != nil{
            aCoder.encode(5, forKey: "5")
        }
        if six != nil{
            aCoder.encode(6, forKey: "6")
        }
    }
}
