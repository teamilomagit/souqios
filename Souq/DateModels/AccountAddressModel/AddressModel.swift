//
//  AddressModel.swift
//  Souq
//
//  Created by Pawan Ramteke on 28/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import Foundation


class AddressModel : NSObject, NSCoding{
    
    var address1 : String!
    var address2 : String!
    var addressFormat : String!
    var addressId : String!
    var city : String!
    var company : String!
    var country : String!
    var countryId : String!
    var customField : CustomField!
    var defaultField : Bool!
    var firstname : String!
    var isoCode2 : String!
    var isoCode3 : String!
    var lastname : String!
    var postcode : String!
    var zone : String!
    var zoneCode : String!
    var zoneId : String!
    var isSelected : Bool = false
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    
    override init() {
        
    }
    
    init(fromDictionary dictionary: [String:Any]){
        address1 = dictionary["address_1"] as? String
        address2 = dictionary["address_2"] as? String
        addressFormat = dictionary["address_format"] as? String
        addressId = dictionary["address_id"] as? String
        city = dictionary["city"] as? String
        company = dictionary["company"] as? String
        country = dictionary["country"] as? String
        countryId = dictionary["country_id"] as? String
        defaultField = dictionary["default"] as? Bool
        firstname = dictionary["firstname"] as? String
        isoCode2 = dictionary["iso_code_2"] as? String
        isoCode3 = dictionary["iso_code_3"] as? String
        lastname = dictionary["lastname"] as? String
        postcode = dictionary["postcode"] as? String
        zone = dictionary["zone"] as? String
        zoneCode = dictionary["zone_code"] as? String
        zoneId = dictionary["zone_id"] as? String
        if let customFieldData = dictionary["custom_field"] as? [String:Any]{
            customField = CustomField(fromDictionary: customFieldData)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address1 != nil{
            dictionary["address_1"] = address1
        }
        if address2 != nil{
            dictionary["address_2"] = address2
        }
        if addressFormat != nil{
            dictionary["address_format"] = addressFormat
        }
        if addressId != nil{
            dictionary["address_id"] = addressId
        }
        if city != nil{
            dictionary["city"] = city
        }
        if company != nil{
            dictionary["company"] = company
        }
        if country != nil{
            dictionary["country"] = country
        }
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if defaultField != nil{
            dictionary["default"] = defaultField
        }
        if firstname != nil{
            dictionary["firstname"] = firstname
        }
        if isoCode2 != nil{
            dictionary["iso_code_2"] = isoCode2
        }
        if isoCode3 != nil{
            dictionary["iso_code_3"] = isoCode3
        }
        if lastname != nil{
            dictionary["lastname"] = lastname
        }
        if postcode != nil{
            dictionary["postcode"] = postcode
        }
        if zone != nil{
            dictionary["zone"] = zone
        }
        if zoneCode != nil{
            dictionary["zone_code"] = zoneCode
        }
        if zoneId != nil{
            dictionary["zone_id"] = zoneId
        }
        if customField != nil{
            dictionary["customField"] = customField.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address1 = aDecoder.decodeObject(forKey: "address_1") as? String
        address2 = aDecoder.decodeObject(forKey: "address_2") as? String
        addressFormat = aDecoder.decodeObject(forKey: "address_format") as? String
        addressId = aDecoder.decodeObject(forKey: "address_id") as? String
        city = aDecoder.decodeObject(forKey: "city") as? String
        company = aDecoder.decodeObject(forKey: "company") as? String
        country = aDecoder.decodeObject(forKey: "country") as? String
        countryId = aDecoder.decodeObject(forKey: "country_id") as? String
        customField = aDecoder.decodeObject(forKey: "custom_field") as? CustomField
        defaultField = aDecoder.decodeObject(forKey: "default") as? Bool
        firstname = aDecoder.decodeObject(forKey: "firstname") as? String
        isoCode2 = aDecoder.decodeObject(forKey: "iso_code_2") as? String
        isoCode3 = aDecoder.decodeObject(forKey: "iso_code_3") as? String
        lastname = aDecoder.decodeObject(forKey: "lastname") as? String
        postcode = aDecoder.decodeObject(forKey: "postcode") as? String
        zone = aDecoder.decodeObject(forKey: "zone") as? String
        zoneCode = aDecoder.decodeObject(forKey: "zone_code") as? String
        zoneId = aDecoder.decodeObject(forKey: "zone_id") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address1 != nil{
            aCoder.encode(address1, forKey: "address_1")
        }
        if address2 != nil{
            aCoder.encode(address2, forKey: "address_2")
        }
        if addressFormat != nil{
            aCoder.encode(addressFormat, forKey: "address_format")
        }
        if addressId != nil{
            aCoder.encode(addressId, forKey: "address_id")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if company != nil{
            aCoder.encode(company, forKey: "company")
        }
        if country != nil{
            aCoder.encode(country, forKey: "country")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if customField != nil{
            aCoder.encode(customField, forKey: "custom_field")
        }
        if defaultField != nil{
            aCoder.encode(defaultField, forKey: "default")
        }
        if firstname != nil{
            aCoder.encode(firstname, forKey: "firstname")
        }
        if isoCode2 != nil{
            aCoder.encode(isoCode2, forKey: "iso_code_2")
        }
        if isoCode3 != nil{
            aCoder.encode(isoCode3, forKey: "iso_code_3")
        }
        if lastname != nil{
            aCoder.encode(lastname, forKey: "lastname")
        }
        if postcode != nil{
            aCoder.encode(postcode, forKey: "postcode")
        }
        if zone != nil{
            aCoder.encode(zone, forKey: "zone")
        }
        if zoneCode != nil{
            aCoder.encode(zoneCode, forKey: "zone_code")
        }
        if zoneId != nil{
            aCoder.encode(zoneId, forKey: "zone_id")
        }
    }
}
