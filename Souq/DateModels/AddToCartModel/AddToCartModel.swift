//
//  AddToCartModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 19, 2019

import Foundation


class AddToCartModel : NSObject, NSCoding{

    var product : ProductAddCart!
    var total : String!
    var totalPrice : String!
    var totalProductCount : Int!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        total = dictionary["total"] as? String
        totalPrice = dictionary["total_price"] as? String
        totalProductCount = dictionary["total_product_count"] as? Int
        if let productData = dictionary["product"] as? [String:Any]{
            product = ProductAddCart(fromDictionary: productData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if total != nil{
            dictionary["total"] = total
        }
        if totalPrice != nil{
            dictionary["total_price"] = totalPrice
        }
        if totalProductCount != nil{
            dictionary["total_product_count"] = totalProductCount
        }
        if product != nil{
            dictionary["product"] = product.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        product = aDecoder.decodeObject(forKey: "product") as? ProductAddCart
        total = aDecoder.decodeObject(forKey: "total") as? String
        totalPrice = aDecoder.decodeObject(forKey: "total_price") as? String
        totalProductCount = aDecoder.decodeObject(forKey: "total_product_count") as? Int
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if product != nil{
            aCoder.encode(product, forKey: "product")
        }
        if total != nil{
            aCoder.encode(total, forKey: "total")
        }
        if totalPrice != nil{
            aCoder.encode(totalPrice, forKey: "total_price")
        }
        if totalProductCount != nil{
            aCoder.encode(totalProductCount, forKey: "total_product_count")
        }
    }
}
