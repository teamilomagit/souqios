//
//  ZoneShippingModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 25, 2019

import Foundation


class ZoneShippingModel : NSObject, NSCoding{

    var countryId : String!
    var name : String!
    var shippingFlatCost : String!
    var zoneId : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        countryId = dictionary["country_id"] as? String
        name = dictionary["name"] as? String
        shippingFlatCost = dictionary["shipping_flat_cost"] as? String
        zoneId = dictionary["zone_id"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if name != nil{
            dictionary["name"] = name
        }
        if shippingFlatCost != nil{
            dictionary["shipping_flat_cost"] = shippingFlatCost
        }
        if zoneId != nil{
            dictionary["zone_id"] = zoneId
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        countryId = aDecoder.decodeObject(forKey: "country_id") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        shippingFlatCost = aDecoder.decodeObject(forKey: "shipping_flat_cost") as? String
        zoneId = aDecoder.decodeObject(forKey: "zone_id") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if shippingFlatCost != nil{
            aCoder.encode(shippingFlatCost, forKey: "shipping_flat_cost")
        }
        if zoneId != nil{
            aCoder.encode(zoneId, forKey: "zone_id")
        }
    }
}