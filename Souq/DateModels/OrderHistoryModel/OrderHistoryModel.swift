//
//  OrderHistoryModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 11, 2019

import Foundation


class OrderHistoryModel : NSObject, NSCoding{

    var currency : Currency!
    var currencyCode : String!
    var currencyValue : String!
    var dateAdded : String!
    var name : String!
    var orderId : String!
    var products : Int!
    var status : String!
    var timestamp : Int!
    var total : String!
    var totalRaw : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        currencyCode = dictionary["currency_code"] as? String
        currencyValue = dictionary["currency_value"] as? String
        dateAdded = dictionary["date_added"] as? String
        name = dictionary["name"] as? String
        orderId = dictionary["order_id"] as? String
        products = dictionary["products"] as? Int
        status = dictionary["status"] as? String
        timestamp = dictionary["timestamp"] as? Int
        total = dictionary["total"] as? String
        totalRaw = dictionary["total_raw"] as? String
        if let currencyData = dictionary["currency"] as? [String:Any]{
            currency = Currency(fromDictionary: currencyData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if currencyCode != nil{
            dictionary["currency_code"] = currencyCode
        }
        if currencyValue != nil{
            dictionary["currency_value"] = currencyValue
        }
        if dateAdded != nil{
            dictionary["date_added"] = dateAdded
        }
        if name != nil{
            dictionary["name"] = name
        }
        if orderId != nil{
            dictionary["order_id"] = orderId
        }
        if products != nil{
            dictionary["products"] = products
        }
        if status != nil{
            dictionary["status"] = status
        }
        if timestamp != nil{
            dictionary["timestamp"] = timestamp
        }
        if total != nil{
            dictionary["total"] = total
        }
        if totalRaw != nil{
            dictionary["total_raw"] = totalRaw
        }
        if currency != nil{
            dictionary["currency"] = currency.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        currency = aDecoder.decodeObject(forKey: "currency") as? Currency
        currencyCode = aDecoder.decodeObject(forKey: "currency_code") as? String
        currencyValue = aDecoder.decodeObject(forKey: "currency_value") as? String
        dateAdded = aDecoder.decodeObject(forKey: "date_added") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        orderId = aDecoder.decodeObject(forKey: "order_id") as? String
        products = aDecoder.decodeObject(forKey: "products") as? Int
        status = aDecoder.decodeObject(forKey: "status") as? String
        timestamp = aDecoder.decodeObject(forKey: "timestamp") as? Int
        total = aDecoder.decodeObject(forKey: "total") as? String
        totalRaw = aDecoder.decodeObject(forKey: "total_raw") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if currency != nil{
            aCoder.encode(currency, forKey: "currency")
        }
        if currencyCode != nil{
            aCoder.encode(currencyCode, forKey: "currency_code")
        }
        if currencyValue != nil{
            aCoder.encode(currencyValue, forKey: "currency_value")
        }
        if dateAdded != nil{
            aCoder.encode(dateAdded, forKey: "date_added")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if orderId != nil{
            aCoder.encode(orderId, forKey: "order_id")
        }
        if products != nil{
            aCoder.encode(products, forKey: "products")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if timestamp != nil{
            aCoder.encode(timestamp, forKey: "timestamp")
        }
        if total != nil{
            aCoder.encode(total, forKey: "total")
        }
        if totalRaw != nil{
            aCoder.encode(totalRaw, forKey: "total_raw")
        }
    }
}