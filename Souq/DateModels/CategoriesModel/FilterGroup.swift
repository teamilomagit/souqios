//
//  FilterGroup.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 5, 2019

import Foundation


class FilterGroup : NSObject, NSCoding{

    var filterOptions : [FilterOptions]!
    var filterGroupId : Int!
    var name : String!
    var selFilters : String = ""
    var selFilterIds : String = ""
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        filterGroupId = dictionary["filter_group_id"] as? Int
        name = dictionary["name"] as? String
        filterOptions = [FilterOptions]()
        if let filterArray = dictionary["filter"] as? [[String:Any]]{
            for dic in filterArray{
                let value = FilterOptions(fromDictionary: dic)
                filterOptions.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if filterGroupId != nil{
            dictionary["filter_group_id"] = filterGroupId
        }
        if name != nil{
            dictionary["name"] = name
        }
        if filterOptions != nil{
            var dictionaryElements = [[String:Any]]()
            for filterElement in filterOptions {
                dictionaryElements.append(filterElement.toDictionary())
            }
            dictionary["filter"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        filterOptions = aDecoder.decodeObject(forKey: "filter") as? [FilterOptions]
        filterGroupId = aDecoder.decodeObject(forKey: "filter_group_id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if filterOptions != nil{
            aCoder.encode(filterOptions, forKey: "filter")
        }
        if filterGroupId != nil{
            aCoder.encode(filterGroupId, forKey: "filter_group_id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
    }
    
   
}
