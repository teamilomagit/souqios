//
//  Filter.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 5, 2019

import Foundation


class Filter : NSObject, NSCoding{

    var filterGroups : [FilterGroup]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        filterGroups = [FilterGroup]()
        if let filterGroupsArray = dictionary["filter_groups"] as? [[String:Any]]{
            for dic in filterGroupsArray{
                let value = FilterGroup(fromDictionary: dic)
                filterGroups.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if filterGroups != nil{
            var dictionaryElements = [[String:Any]]()
            for filterGroupsElement in filterGroups {
                dictionaryElements.append(filterGroupsElement.toDictionary())
            }
            dictionary["filter_groups"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        filterGroups = aDecoder.decodeObject(forKey: "filter_groups") as? [FilterGroup]
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if filterGroups != nil{
            aCoder.encode(filterGroups, forKey: "filter_groups")
        }
    }

}
