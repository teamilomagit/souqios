//
//  FilterOptions.swift
//  Souq
//
//  Created by Pawan Ramteke on 07/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class FilterOptions: NSObject {
    var filterId : String!
    var name : String!
    var isSelected : Bool = false
    init(fromDictionary dictionary: [String:Any]){
        filterId = dictionary["filter_id"] as? String
        name = dictionary["name"] as? String
    }
    
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if filterId != nil{
            dictionary["filter_id"] = filterId
        }
        if name != nil{
            dictionary["name"] = name
        }
        
        return dictionary
    }
}
