//
//  CategoriesModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 5, 2019

import Foundation


class CategoriesModel : NSObject, NSCoding{

    var categories : [AnyObject]!
    var categoryId : Int!
    var filters : Filter!
    var image : String!
    var name : String!
    var originalImage : String!
    var parentId : Int!
    var seoUrl : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        categoryId = dictionary["category_id"] as? Int
        image = dictionary["image"] as? String
        name = dictionary["name"] as? String
        originalImage = dictionary["original_image"] as? String
        parentId = dictionary["parent_id"] as? Int
        seoUrl = dictionary["seo_url"] as? String
        if let filtersData = dictionary["filters"] as? [String:Any]{
            filters = Filter(fromDictionary: filtersData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if categoryId != nil{
            dictionary["category_id"] = categoryId
        }
        if image != nil{
            dictionary["image"] = image
        }
        if name != nil{
            dictionary["name"] = name
        }
        if originalImage != nil{
            dictionary["original_image"] = originalImage
        }
        if parentId != nil{
            dictionary["parent_id"] = parentId
        }
        if seoUrl != nil{
            dictionary["seo_url"] = seoUrl
        }
        if filters != nil{
            dictionary["filters"] = filters.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        categories = aDecoder.decodeObject(forKey: "categories") as? [AnyObject]
        categoryId = aDecoder.decodeObject(forKey: "category_id") as? Int
        filters = aDecoder.decodeObject(forKey: "filters") as? Filter
        image = aDecoder.decodeObject(forKey: "image") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        originalImage = aDecoder.decodeObject(forKey: "original_image") as? String
        parentId = aDecoder.decodeObject(forKey: "parent_id") as? Int
        seoUrl = aDecoder.decodeObject(forKey: "seo_url") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if categories != nil{
            aCoder.encode(categories, forKey: "categories")
        }
        if categoryId != nil{
            aCoder.encode(categoryId, forKey: "category_id")
        }
        if filters != nil{
            aCoder.encode(filters, forKey: "filters")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if originalImage != nil{
            aCoder.encode(originalImage, forKey: "original_image")
        }
        if parentId != nil{
            aCoder.encode(parentId, forKey: "parent_id")
        }
        if seoUrl != nil{
            aCoder.encode(seoUrl, forKey: "seo_url")
        }
    }
}