//
//  CountriesModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 23, 2019

import Foundation


class CountriesModel : NSObject, NSCoding{

    var addressFormat : String!
    var countryId : Int!
    var isoCode2 : String!
    var isoCode3 : String!
    var name : String!
    var postcodeRequired : String!
    var status : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        addressFormat = dictionary["address_format"] as? String
        countryId = dictionary["country_id"] as? Int
        isoCode2 = dictionary["iso_code_2"] as? String
        isoCode3 = dictionary["iso_code_3"] as? String
        name = dictionary["name"] as? String
        postcodeRequired = dictionary["postcode_required"] as? String
        status = dictionary["status"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if addressFormat != nil{
            dictionary["address_format"] = addressFormat
        }
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if isoCode2 != nil{
            dictionary["iso_code_2"] = isoCode2
        }
        if isoCode3 != nil{
            dictionary["iso_code_3"] = isoCode3
        }
        if name != nil{
            dictionary["name"] = name
        }
        if postcodeRequired != nil{
            dictionary["postcode_required"] = postcodeRequired
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        addressFormat = aDecoder.decodeObject(forKey: "address_format") as? String
        countryId = aDecoder.decodeObject(forKey: "country_id") as? Int
        isoCode2 = aDecoder.decodeObject(forKey: "iso_code_2") as? String
        isoCode3 = aDecoder.decodeObject(forKey: "iso_code_3") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        postcodeRequired = aDecoder.decodeObject(forKey: "postcode_required") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if addressFormat != nil{
            aCoder.encode(addressFormat, forKey: "address_format")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if isoCode2 != nil{
            aCoder.encode(isoCode2, forKey: "iso_code_2")
        }
        if isoCode3 != nil{
            aCoder.encode(isoCode3, forKey: "iso_code_3")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if postcodeRequired != nil{
            aCoder.encode(postcodeRequired, forKey: "postcode_required")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
    }
}