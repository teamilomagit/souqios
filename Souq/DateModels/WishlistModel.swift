//
//  WishlistModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 2, 2019

import Foundation


class WishlistModel : NSObject, NSCoding{

    var model : String!
    var name : String!
    var price : String!
    var productId : String!
    var productSeoUrl : String!
    var special : Int!
    var stock : String!
    var thumb : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        model = dictionary["model"] as? String
        name = dictionary["name"] as? String
        price = dictionary["price"] as? String
        productId = dictionary["product_id"] as? String
        productSeoUrl = dictionary["product_seo_url"] as? String
        special = dictionary["special"] as? Int
        stock = dictionary["stock"] as? String
        thumb = dictionary["thumb"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if model != nil{
            dictionary["model"] = model
        }
        if name != nil{
            dictionary["name"] = name
        }
        if price != nil{
            dictionary["price"] = price
        }
        if productId != nil{
            dictionary["product_id"] = productId
        }
        if productSeoUrl != nil{
            dictionary["product_seo_url"] = productSeoUrl
        }
        if special != nil{
            dictionary["special"] = special
        }
        if stock != nil{
            dictionary["stock"] = stock
        }
        if thumb != nil{
            dictionary["thumb"] = thumb
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        model = aDecoder.decodeObject(forKey: "model") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        price = aDecoder.decodeObject(forKey: "price") as? String
        productId = aDecoder.decodeObject(forKey: "product_id") as? String
        productSeoUrl = aDecoder.decodeObject(forKey: "product_seo_url") as? String
        special = aDecoder.decodeObject(forKey: "special") as? Int
        stock = aDecoder.decodeObject(forKey: "stock") as? String
        thumb = aDecoder.decodeObject(forKey: "thumb") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if model != nil{
            aCoder.encode(model, forKey: "model")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if productId != nil{
            aCoder.encode(productId, forKey: "product_id")
        }
        if productSeoUrl != nil{
            aCoder.encode(productSeoUrl, forKey: "product_seo_url")
        }
        if special != nil{
            aCoder.encode(special, forKey: "special")
        }
        if stock != nil{
            aCoder.encode(stock, forKey: "stock")
        }
        if thumb != nil{
            aCoder.encode(thumb, forKey: "thumb")
        }
    }
}