//
//  Quote.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 9, 2019

import Foundation


class Quote : NSObject, NSCoding{

    var flat : Flat!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let flatData = dictionary["flat"] as? [String:Any]{
            flat = Flat(fromDictionary: flatData as NSDictionary)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if flat != nil{
            dictionary["flat"] = flat.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        flat = aDecoder.decodeObject(forKey: "flat") as? Flat
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if flat != nil{
            aCoder.encode(flat, forKey: "flat")
        }
    }
}
