//
//  Free.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 9, 2019

import Foundation


class Free : NSObject, NSCoding{

    var code : String!
    var cost : Int!
    var taxClassId : Int!
    var text : String!
    var title : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        code = dictionary["code"] as? String
        cost = dictionary["cost"] as? Int
        taxClassId = dictionary["tax_class_id"] as? Int
        text = dictionary["text"] as? String
        title = dictionary["title"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if code != nil{
            dictionary["code"] = code
        }
        if cost != nil{
            dictionary["cost"] = cost
        }
        if taxClassId != nil{
            dictionary["tax_class_id"] = taxClassId
        }
        if text != nil{
            dictionary["text"] = text
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        code = aDecoder.decodeObject(forKey: "code") as? String
        cost = aDecoder.decodeObject(forKey: "cost") as? Int
        taxClassId = aDecoder.decodeObject(forKey: "tax_class_id") as? Int
        text = aDecoder.decodeObject(forKey: "text") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if code != nil{
            aCoder.encode(code, forKey: "code")
        }
        if cost != nil{
            aCoder.encode(cost, forKey: "cost")
        }
        if taxClassId != nil{
            aCoder.encode(taxClassId, forKey: "tax_class_id")
        }
        if text != nil{
            aCoder.encode(text, forKey: "text")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
    }
}