//
//  ShippingMethodModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 9, 2019

import Foundation


class ShippingMethodModel : NSObject, NSCoding{

    var flat : Flat!
    var free : Free!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let flatData = dictionary["flat"] as? NSDictionary{
            flat = Flat(fromDictionary: flatData.value(forKeyPath: "quote.flat") as! NSDictionary)
        }
        if let freeData = dictionary["free"] as? NSDictionary{
            free = Free(fromDictionary: freeData.value(forKeyPath: "quote.free") as! [String : Any])
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if flat != nil{
            dictionary["flat"] = flat.toDictionary()
        }
        if free != nil{
            dictionary["free"] = free.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        flat = aDecoder.decodeObject(forKey: "flat") as? Flat
        free = aDecoder.decodeObject(forKey: "free") as? Free
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if flat != nil{
            aCoder.encode(flat, forKey: "flat")
        }
        if free != nil{
            aCoder.encode(free, forKey: "free")
        }
    }
}
