//
//  LoginModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 2, 2019

import Foundation


class LoginModel : NSObject, NSCoding{

    var addressId : String!
    var cartCountProducts : Int!
    var code : String!
    var company : AnyObject!
    var customerGroupId : String!
    var customerId : String!
    var dateAdded : String!
    var email : String!
    var fax : String!
    var firstname : String!
    var ip : String!
    var languageId : String!
    var lastname : String!
    var newsletter : String!
    var safe : String!
    var seller : String!
    var status : String!
    var storeId : String!
    var telephone : String!
    var wishlist : [String]!
    var wishlistTotal : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        addressId = dictionary["address_id"] as? String
        cartCountProducts = dictionary["cart_count_products"] as? Int
        code = dictionary["code"] as? String
        company = dictionary["company"] as? AnyObject
        customerGroupId = dictionary["customer_group_id"] as? String
        customerId = dictionary["customer_id"] as? String
        dateAdded = dictionary["date_added"] as? String
        email = dictionary["email"] as? String
        fax = dictionary["fax"] as? String
        firstname = dictionary["firstname"] as? String
        ip = dictionary["ip"] as? String
        languageId = dictionary["language_id"] as? String
        lastname = dictionary["lastname"] as? String
        newsletter = dictionary["newsletter"] as? String
        safe = dictionary["safe"] as? String
        seller = dictionary["seller"] as? String
        status = dictionary["status"] as? String
        storeId = dictionary["store_id"] as? String
        telephone = dictionary["telephone"] as? String
        wishlistTotal = dictionary["wishlist_total"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if addressId != nil{
            dictionary["address_id"] = addressId
        }
        if cartCountProducts != nil{
            dictionary["cart_count_products"] = cartCountProducts
        }
        if code != nil{
            dictionary["code"] = code
        }
        if company != nil{
            dictionary["company"] = company
        }
        if customerGroupId != nil{
            dictionary["customer_group_id"] = customerGroupId
        }
        if customerId != nil{
            dictionary["customer_id"] = customerId
        }
        if dateAdded != nil{
            dictionary["date_added"] = dateAdded
        }
        if email != nil{
            dictionary["email"] = email
        }
        if fax != nil{
            dictionary["fax"] = fax
        }
        if firstname != nil{
            dictionary["firstname"] = firstname
        }
        if ip != nil{
            dictionary["ip"] = ip
        }
        if languageId != nil{
            dictionary["language_id"] = languageId
        }
        if lastname != nil{
            dictionary["lastname"] = lastname
        }
        if newsletter != nil{
            dictionary["newsletter"] = newsletter
        }
        if safe != nil{
            dictionary["safe"] = safe
        }
        if seller != nil{
            dictionary["seller"] = seller
        }
        if status != nil{
            dictionary["status"] = status
        }
        if storeId != nil{
            dictionary["store_id"] = storeId
        }
        if telephone != nil{
            dictionary["telephone"] = telephone
        }
        if wishlistTotal != nil{
            dictionary["wishlist_total"] = wishlistTotal
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        addressId = aDecoder.decodeObject(forKey: "address_id") as? String
        cartCountProducts = aDecoder.decodeObject(forKey: "cart_count_products") as? Int
        code = aDecoder.decodeObject(forKey: "code") as? String
        company = aDecoder.decodeObject(forKey: "company") as? AnyObject
        customerGroupId = aDecoder.decodeObject(forKey: "customer_group_id") as? String
        customerId = aDecoder.decodeObject(forKey: "customer_id") as? String
        dateAdded = aDecoder.decodeObject(forKey: "date_added") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        fax = aDecoder.decodeObject(forKey: "fax") as? String
        firstname = aDecoder.decodeObject(forKey: "firstname") as? String
        ip = aDecoder.decodeObject(forKey: "ip") as? String
        languageId = aDecoder.decodeObject(forKey: "language_id") as? String
        lastname = aDecoder.decodeObject(forKey: "lastname") as? String
        newsletter = aDecoder.decodeObject(forKey: "newsletter") as? String
        safe = aDecoder.decodeObject(forKey: "safe") as? String
        seller = aDecoder.decodeObject(forKey: "seller") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        storeId = aDecoder.decodeObject(forKey: "store_id") as? String
        telephone = aDecoder.decodeObject(forKey: "telephone") as? String
        wishlist = aDecoder.decodeObject(forKey: "wishlist") as? [String]
        wishlistTotal = aDecoder.decodeObject(forKey: "wishlist_total") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if addressId != nil{
            aCoder.encode(addressId, forKey: "address_id")
        }
        if cartCountProducts != nil{
            aCoder.encode(cartCountProducts, forKey: "cart_count_products")
        }
        if code != nil{
            aCoder.encode(code, forKey: "code")
        }
        if company != nil{
            aCoder.encode(company, forKey: "company")
        }
        
        if customerGroupId != nil{
            aCoder.encode(customerGroupId, forKey: "customer_group_id")
        }
        if customerId != nil{
            aCoder.encode(customerId, forKey: "customer_id")
        }
        if dateAdded != nil{
            aCoder.encode(dateAdded, forKey: "date_added")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if fax != nil{
            aCoder.encode(fax, forKey: "fax")
        }
        if firstname != nil{
            aCoder.encode(firstname, forKey: "firstname")
        }
        if ip != nil{
            aCoder.encode(ip, forKey: "ip")
        }
        if languageId != nil{
            aCoder.encode(languageId, forKey: "language_id")
        }
        if lastname != nil{
            aCoder.encode(lastname, forKey: "lastname")
        }
        if newsletter != nil{
            aCoder.encode(newsletter, forKey: "newsletter")
        }
        if safe != nil{
            aCoder.encode(safe, forKey: "safe")
        }
        if seller != nil{
            aCoder.encode(seller, forKey: "seller")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if storeId != nil{
            aCoder.encode(storeId, forKey: "store_id")
        }
        if telephone != nil{
            aCoder.encode(telephone, forKey: "telephone")
        }
        if wishlist != nil{
            aCoder.encode(wishlist, forKey: "wishlist")
        }
        if wishlistTotal != nil{
            aCoder.encode(wishlistTotal, forKey: "wishlist_total")
        }
    }
}
