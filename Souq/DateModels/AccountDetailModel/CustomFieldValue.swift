//
//  CustomFieldValue.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 14, 2019

import Foundation


class CustomFieldValue : NSObject, NSCoding{

    var customFieldValueId : String!
    var name : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        customFieldValueId = dictionary["custom_field_value_id"] as? String
        name = dictionary["name"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if customFieldValueId != nil{
            dictionary["custom_field_value_id"] = customFieldValueId
        }
        if name != nil{
            dictionary["name"] = name
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        customFieldValueId = aDecoder.decodeObject(forKey: "custom_field_value_id") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if customFieldValueId != nil{
            aCoder.encode(customFieldValueId, forKey: "custom_field_value_id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
    }
}