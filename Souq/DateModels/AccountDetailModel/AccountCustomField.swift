//
//  AccountCustomField.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 14, 2019

import Foundation


class AccountCustomField : NSObject, NSCoding{

    var one : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        one = dictionary["1"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if one != nil{
            dictionary["1"] = 1
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        one = aDecoder.decodeObject(forKey: "1") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if one != nil{
            aCoder.encode(1, forKey: "1")
        }
    }
}
