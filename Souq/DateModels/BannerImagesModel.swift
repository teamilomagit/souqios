//
//  BannerImagesModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 4, 2019

import Foundation


class BannerImagesModel : NSObject, NSCoding{

    var image : String!
    var link : String!
    var title : String!
    var isSelected : Bool = false

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        image = dictionary["image"] as? String
        link = dictionary["link"] as? String
        title = dictionary["title"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if image != nil{
            dictionary["image"] = image
        }
        if link != nil{
            dictionary["link"] = link
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        image = aDecoder.decodeObject(forKey: "image") as? String
        link = aDecoder.decodeObject(forKey: "link") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if link != nil{
            aCoder.encode(link, forKey: "link")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
    }
}
