//
//  OrderHistoryDetailsModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 11, 2019

import Foundation


class OrderHistoryDetailsModel : NSObject, NSCoding{

    var comment : String!
    var currency : Currency!
    var currencyCode : String!
    var currencyId : String!
    var currencyValue : String!
    var customerId : String!
    var dateAdded : String!
    var dateModified : String!
    var email : String!
    var firstname : String!
    var histories : [History]!
    var invoiceNo : String!
    var invoicePrefix : String!
    var ip : String!
    var languageId : String!
    var lastname : String!
    var orderId : String!
    var orderStatusId : String!
    var paymentAddress : String!
    var paymentAddress1 : String!
    var paymentAddress2 : String!
    var paymentAddressFormat : String!
    var paymentCity : String!
    var paymentCompany : String!
    var paymentCountry : String!
    var paymentCountryId : String!
    var paymentFirstname : String!
    var paymentIsoCode2 : String!
    var paymentIsoCode3 : String!
    var paymentLastname : String!
    var paymentMethod : String!
    var paymentPostcode : String!
    var paymentZone : String!
    var paymentZoneCode : String!
    var paymentZoneId : String!
    var products : [Product]!
    var shippingAddress : String!
    var shippingAddress1 : String!
    var shippingAddress2 : String!
    var shippingAddressFormat : String!
    var shippingCity : String!
    var shippingCompany : String!
    var shippingCountry : String!
    var shippingCountryId : String!
    var shippingFirstname : String!
    var shippingIsoCode2 : String!
    var shippingIsoCode3 : String!
    var shippingLastname : String!
    var shippingMethod : String!
    var shippingPostcode : String!
    var shippingZone : String!
    var shippingZoneCode : String!
    var shippingZoneId : String!
    var storeId : String!
    var storeName : String!
    var storeUrl : String!
    var telephone : String!
    var timestamp : Int!
    var total : String!
    var totals : [Total]!
    var vouchers : [AnyObject]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        comment = dictionary["comment"] as? String
        currencyCode = dictionary["currency_code"] as? String
        currencyId = dictionary["currency_id"] as? String
        currencyValue = dictionary["currency_value"] as? String
        customerId = dictionary["customer_id"] as? String
        dateAdded = dictionary["date_added"] as? String
        dateModified = dictionary["date_modified"] as? String
        email = dictionary["email"] as? String
        firstname = dictionary["firstname"] as? String
        invoiceNo = dictionary["invoice_no"] as? String
        invoicePrefix = dictionary["invoice_prefix"] as? String
        ip = dictionary["ip"] as? String
        languageId = dictionary["language_id"] as? String
        lastname = dictionary["lastname"] as? String
        orderId = dictionary["order_id"] as? String
        orderStatusId = dictionary["order_status_id"] as? String
        paymentAddress = dictionary["payment_address"] as? String
        paymentAddress1 = dictionary["payment_address_1"] as? String
        paymentAddress2 = dictionary["payment_address_2"] as? String
        paymentAddressFormat = dictionary["payment_address_format"] as? String
        paymentCity = dictionary["payment_city"] as? String
        paymentCompany = dictionary["payment_company"] as? String
        paymentCountry = dictionary["payment_country"] as? String
        paymentCountryId = dictionary["payment_country_id"] as? String
        paymentFirstname = dictionary["payment_firstname"] as? String
        paymentIsoCode2 = dictionary["payment_iso_code_2"] as? String
        paymentIsoCode3 = dictionary["payment_iso_code_3"] as? String
        paymentLastname = dictionary["payment_lastname"] as? String
        paymentMethod = dictionary["payment_method"] as? String
        paymentPostcode = dictionary["payment_postcode"] as? String
        paymentZone = dictionary["payment_zone"] as? String
        paymentZoneCode = dictionary["payment_zone_code"] as? String
        paymentZoneId = dictionary["payment_zone_id"] as? String
        shippingAddress = dictionary["shipping_address"] as? String
        shippingAddress1 = dictionary["shipping_address_1"] as? String
        shippingAddress2 = dictionary["shipping_address_2"] as? String
        shippingAddressFormat = dictionary["shipping_address_format"] as? String
        shippingCity = dictionary["shipping_city"] as? String
        shippingCompany = dictionary["shipping_company"] as? String
        shippingCountry = dictionary["shipping_country"] as? String
        shippingCountryId = dictionary["shipping_country_id"] as? String
        shippingFirstname = dictionary["shipping_firstname"] as? String
        shippingIsoCode2 = dictionary["shipping_iso_code_2"] as? String
        shippingIsoCode3 = dictionary["shipping_iso_code_3"] as? String
        shippingLastname = dictionary["shipping_lastname"] as? String
        shippingMethod = dictionary["shipping_method"] as? String
        shippingPostcode = dictionary["shipping_postcode"] as? String
        shippingZone = dictionary["shipping_zone"] as? String
        shippingZoneCode = dictionary["shipping_zone_code"] as? String
        shippingZoneId = dictionary["shipping_zone_id"] as? String
        storeId = dictionary["store_id"] as? String
        storeName = dictionary["store_name"] as? String
        storeUrl = dictionary["store_url"] as? String
        telephone = dictionary["telephone"] as? String
        timestamp = dictionary["timestamp"] as? Int
        total = dictionary["total"] as? String
        if let currencyData = dictionary["currency"] as? [String:Any]{
            currency = Currency(fromDictionary: currencyData)
        }
        histories = [History]()
        if let historiesArray = dictionary["histories"] as? [[String:Any]]{
            for dic in historiesArray{
                let value = History(fromDictionary: dic)
                histories.append(value)
            }
        }
        products = [Product]()
        if let productsArray = dictionary["products"] as? [[String:Any]]{
            for dic in productsArray{
                let value = Product(fromDictionary: dic)
                products.append(value)
            }
        }
        totals = [Total]()
        if let totalsArray = dictionary["totals"] as? [[String:Any]]{
            for dic in totalsArray{
                let value = Total(fromDictionary: dic)
                totals.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if comment != nil{
            dictionary["comment"] = comment
        }
        if currencyCode != nil{
            dictionary["currency_code"] = currencyCode
        }
        if currencyId != nil{
            dictionary["currency_id"] = currencyId
        }
        if currencyValue != nil{
            dictionary["currency_value"] = currencyValue
        }
        if customerId != nil{
            dictionary["customer_id"] = customerId
        }
        if dateAdded != nil{
            dictionary["date_added"] = dateAdded
        }
        if dateModified != nil{
            dictionary["date_modified"] = dateModified
        }
        if email != nil{
            dictionary["email"] = email
        }
        if firstname != nil{
            dictionary["firstname"] = firstname
        }
        if invoiceNo != nil{
            dictionary["invoice_no"] = invoiceNo
        }
        if invoicePrefix != nil{
            dictionary["invoice_prefix"] = invoicePrefix
        }
        if ip != nil{
            dictionary["ip"] = ip
        }
        if languageId != nil{
            dictionary["language_id"] = languageId
        }
        if lastname != nil{
            dictionary["lastname"] = lastname
        }
        if orderId != nil{
            dictionary["order_id"] = orderId
        }
        if orderStatusId != nil{
            dictionary["order_status_id"] = orderStatusId
        }
        if paymentAddress != nil{
            dictionary["payment_address"] = paymentAddress
        }
        if paymentAddress1 != nil{
            dictionary["payment_address_1"] = paymentAddress1
        }
        if paymentAddress2 != nil{
            dictionary["payment_address_2"] = paymentAddress2
        }
        if paymentAddressFormat != nil{
            dictionary["payment_address_format"] = paymentAddressFormat
        }
        if paymentCity != nil{
            dictionary["payment_city"] = paymentCity
        }
        if paymentCompany != nil{
            dictionary["payment_company"] = paymentCompany
        }
        if paymentCountry != nil{
            dictionary["payment_country"] = paymentCountry
        }
        if paymentCountryId != nil{
            dictionary["payment_country_id"] = paymentCountryId
        }
        if paymentFirstname != nil{
            dictionary["payment_firstname"] = paymentFirstname
        }
        if paymentIsoCode2 != nil{
            dictionary["payment_iso_code_2"] = paymentIsoCode2
        }
        if paymentIsoCode3 != nil{
            dictionary["payment_iso_code_3"] = paymentIsoCode3
        }
        if paymentLastname != nil{
            dictionary["payment_lastname"] = paymentLastname
        }
        if paymentMethod != nil{
            dictionary["payment_method"] = paymentMethod
        }
        if paymentPostcode != nil{
            dictionary["payment_postcode"] = paymentPostcode
        }
        if paymentZone != nil{
            dictionary["payment_zone"] = paymentZone
        }
        if paymentZoneCode != nil{
            dictionary["payment_zone_code"] = paymentZoneCode
        }
        if paymentZoneId != nil{
            dictionary["payment_zone_id"] = paymentZoneId
        }
        if shippingAddress != nil{
            dictionary["shipping_address"] = shippingAddress
        }
        if shippingAddress1 != nil{
            dictionary["shipping_address_1"] = shippingAddress1
        }
        if shippingAddress2 != nil{
            dictionary["shipping_address_2"] = shippingAddress2
        }
        if shippingAddressFormat != nil{
            dictionary["shipping_address_format"] = shippingAddressFormat
        }
        if shippingCity != nil{
            dictionary["shipping_city"] = shippingCity
        }
        if shippingCompany != nil{
            dictionary["shipping_company"] = shippingCompany
        }
        if shippingCountry != nil{
            dictionary["shipping_country"] = shippingCountry
        }
        if shippingCountryId != nil{
            dictionary["shipping_country_id"] = shippingCountryId
        }
        if shippingFirstname != nil{
            dictionary["shipping_firstname"] = shippingFirstname
        }
        if shippingIsoCode2 != nil{
            dictionary["shipping_iso_code_2"] = shippingIsoCode2
        }
        if shippingIsoCode3 != nil{
            dictionary["shipping_iso_code_3"] = shippingIsoCode3
        }
        if shippingLastname != nil{
            dictionary["shipping_lastname"] = shippingLastname
        }
        if shippingMethod != nil{
            dictionary["shipping_method"] = shippingMethod
        }
        if shippingPostcode != nil{
            dictionary["shipping_postcode"] = shippingPostcode
        }
        if shippingZone != nil{
            dictionary["shipping_zone"] = shippingZone
        }
        if shippingZoneCode != nil{
            dictionary["shipping_zone_code"] = shippingZoneCode
        }
        if shippingZoneId != nil{
            dictionary["shipping_zone_id"] = shippingZoneId
        }
        if storeId != nil{
            dictionary["store_id"] = storeId
        }
        if storeName != nil{
            dictionary["store_name"] = storeName
        }
        if storeUrl != nil{
            dictionary["store_url"] = storeUrl
        }
        if telephone != nil{
            dictionary["telephone"] = telephone
        }
        if timestamp != nil{
            dictionary["timestamp"] = timestamp
        }
        if total != nil{
            dictionary["total"] = total
        }
        if currency != nil{
            dictionary["currency"] = currency.toDictionary()
        }
        if histories != nil{
            var dictionaryElements = [[String:Any]]()
            for historiesElement in histories {
                dictionaryElements.append(historiesElement.toDictionary())
            }
            dictionary["histories"] = dictionaryElements
        }
        if products != nil{
            var dictionaryElements = [[String:Any]]()
            for productsElement in products {
                dictionaryElements.append(productsElement.toDictionary())
            }
            dictionary["products"] = dictionaryElements
        }
        if totals != nil{
            var dictionaryElements = [[String:Any]]()
            for totalsElement in totals {
                dictionaryElements.append(totalsElement.toDictionary())
            }
            dictionary["totals"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        comment = aDecoder.decodeObject(forKey: "comment") as? String
        currency = aDecoder.decodeObject(forKey: "currency") as? Currency
        currencyCode = aDecoder.decodeObject(forKey: "currency_code") as? String
        currencyId = aDecoder.decodeObject(forKey: "currency_id") as? String
        currencyValue = aDecoder.decodeObject(forKey: "currency_value") as? String
        customerId = aDecoder.decodeObject(forKey: "customer_id") as? String
        dateAdded = aDecoder.decodeObject(forKey: "date_added") as? String
        dateModified = aDecoder.decodeObject(forKey: "date_modified") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        firstname = aDecoder.decodeObject(forKey: "firstname") as? String
        histories = aDecoder.decodeObject(forKey: "histories") as? [History]
        invoiceNo = aDecoder.decodeObject(forKey: "invoice_no") as? String
        invoicePrefix = aDecoder.decodeObject(forKey: "invoice_prefix") as? String
        ip = aDecoder.decodeObject(forKey: "ip") as? String
        languageId = aDecoder.decodeObject(forKey: "language_id") as? String
        lastname = aDecoder.decodeObject(forKey: "lastname") as? String
        orderId = aDecoder.decodeObject(forKey: "order_id") as? String
        orderStatusId = aDecoder.decodeObject(forKey: "order_status_id") as? String
        paymentAddress = aDecoder.decodeObject(forKey: "payment_address") as? String
        paymentAddress1 = aDecoder.decodeObject(forKey: "payment_address_1") as? String
        paymentAddress2 = aDecoder.decodeObject(forKey: "payment_address_2") as? String
        paymentAddressFormat = aDecoder.decodeObject(forKey: "payment_address_format") as? String
        paymentCity = aDecoder.decodeObject(forKey: "payment_city") as? String
        paymentCompany = aDecoder.decodeObject(forKey: "payment_company") as? String
        paymentCountry = aDecoder.decodeObject(forKey: "payment_country") as? String
        paymentCountryId = aDecoder.decodeObject(forKey: "payment_country_id") as? String
        paymentFirstname = aDecoder.decodeObject(forKey: "payment_firstname") as? String
        paymentIsoCode2 = aDecoder.decodeObject(forKey: "payment_iso_code_2") as? String
        paymentIsoCode3 = aDecoder.decodeObject(forKey: "payment_iso_code_3") as? String
        paymentLastname = aDecoder.decodeObject(forKey: "payment_lastname") as? String
        paymentMethod = aDecoder.decodeObject(forKey: "payment_method") as? String
        paymentPostcode = aDecoder.decodeObject(forKey: "payment_postcode") as? String
        paymentZone = aDecoder.decodeObject(forKey: "payment_zone") as? String
        paymentZoneCode = aDecoder.decodeObject(forKey: "payment_zone_code") as? String
        paymentZoneId = aDecoder.decodeObject(forKey: "payment_zone_id") as? String
        products = aDecoder.decodeObject(forKey: "products") as? [Product]
        shippingAddress = aDecoder.decodeObject(forKey: "shipping_address") as? String
        shippingAddress1 = aDecoder.decodeObject(forKey: "shipping_address_1") as? String
        shippingAddress2 = aDecoder.decodeObject(forKey: "shipping_address_2") as? String
        shippingAddressFormat = aDecoder.decodeObject(forKey: "shipping_address_format") as? String
        shippingCity = aDecoder.decodeObject(forKey: "shipping_city") as? String
        shippingCompany = aDecoder.decodeObject(forKey: "shipping_company") as? String
        shippingCountry = aDecoder.decodeObject(forKey: "shipping_country") as? String
        shippingCountryId = aDecoder.decodeObject(forKey: "shipping_country_id") as? String
        shippingFirstname = aDecoder.decodeObject(forKey: "shipping_firstname") as? String
        shippingIsoCode2 = aDecoder.decodeObject(forKey: "shipping_iso_code_2") as? String
        shippingIsoCode3 = aDecoder.decodeObject(forKey: "shipping_iso_code_3") as? String
        shippingLastname = aDecoder.decodeObject(forKey: "shipping_lastname") as? String
        shippingMethod = aDecoder.decodeObject(forKey: "shipping_method") as? String
        shippingPostcode = aDecoder.decodeObject(forKey: "shipping_postcode") as? String
        shippingZone = aDecoder.decodeObject(forKey: "shipping_zone") as? String
        shippingZoneCode = aDecoder.decodeObject(forKey: "shipping_zone_code") as? String
        shippingZoneId = aDecoder.decodeObject(forKey: "shipping_zone_id") as? String
        storeId = aDecoder.decodeObject(forKey: "store_id") as? String
        storeName = aDecoder.decodeObject(forKey: "store_name") as? String
        storeUrl = aDecoder.decodeObject(forKey: "store_url") as? String
        telephone = aDecoder.decodeObject(forKey: "telephone") as? String
        timestamp = aDecoder.decodeObject(forKey: "timestamp") as? Int
        total = aDecoder.decodeObject(forKey: "total") as? String
        totals = aDecoder.decodeObject(forKey: "totals") as? [Total]
        vouchers = aDecoder.decodeObject(forKey: "vouchers") as? [AnyObject]
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if comment != nil{
            aCoder.encode(comment, forKey: "comment")
        }
        if currency != nil{
            aCoder.encode(currency, forKey: "currency")
        }
        if currencyCode != nil{
            aCoder.encode(currencyCode, forKey: "currency_code")
        }
        if currencyId != nil{
            aCoder.encode(currencyId, forKey: "currency_id")
        }
        if currencyValue != nil{
            aCoder.encode(currencyValue, forKey: "currency_value")
        }
        if customerId != nil{
            aCoder.encode(customerId, forKey: "customer_id")
        }
        if dateAdded != nil{
            aCoder.encode(dateAdded, forKey: "date_added")
        }
        if dateModified != nil{
            aCoder.encode(dateModified, forKey: "date_modified")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if firstname != nil{
            aCoder.encode(firstname, forKey: "firstname")
        }
        if histories != nil{
            aCoder.encode(histories, forKey: "histories")
        }
        if invoiceNo != nil{
            aCoder.encode(invoiceNo, forKey: "invoice_no")
        }
        if invoicePrefix != nil{
            aCoder.encode(invoicePrefix, forKey: "invoice_prefix")
        }
        if ip != nil{
            aCoder.encode(ip, forKey: "ip")
        }
        if languageId != nil{
            aCoder.encode(languageId, forKey: "language_id")
        }
        if lastname != nil{
            aCoder.encode(lastname, forKey: "lastname")
        }
        if orderId != nil{
            aCoder.encode(orderId, forKey: "order_id")
        }
        if orderStatusId != nil{
            aCoder.encode(orderStatusId, forKey: "order_status_id")
        }
        if paymentAddress != nil{
            aCoder.encode(paymentAddress, forKey: "payment_address")
        }
        if paymentAddress1 != nil{
            aCoder.encode(paymentAddress1, forKey: "payment_address_1")
        }
        if paymentAddress2 != nil{
            aCoder.encode(paymentAddress2, forKey: "payment_address_2")
        }
        if paymentAddressFormat != nil{
            aCoder.encode(paymentAddressFormat, forKey: "payment_address_format")
        }
        if paymentCity != nil{
            aCoder.encode(paymentCity, forKey: "payment_city")
        }
        if paymentCompany != nil{
            aCoder.encode(paymentCompany, forKey: "payment_company")
        }
        if paymentCountry != nil{
            aCoder.encode(paymentCountry, forKey: "payment_country")
        }
        if paymentCountryId != nil{
            aCoder.encode(paymentCountryId, forKey: "payment_country_id")
        }
        if paymentFirstname != nil{
            aCoder.encode(paymentFirstname, forKey: "payment_firstname")
        }
        if paymentIsoCode2 != nil{
            aCoder.encode(paymentIsoCode2, forKey: "payment_iso_code_2")
        }
        if paymentIsoCode3 != nil{
            aCoder.encode(paymentIsoCode3, forKey: "payment_iso_code_3")
        }
        if paymentLastname != nil{
            aCoder.encode(paymentLastname, forKey: "payment_lastname")
        }
        if paymentMethod != nil{
            aCoder.encode(paymentMethod, forKey: "payment_method")
        }
        if paymentPostcode != nil{
            aCoder.encode(paymentPostcode, forKey: "payment_postcode")
        }
        if paymentZone != nil{
            aCoder.encode(paymentZone, forKey: "payment_zone")
        }
        if paymentZoneCode != nil{
            aCoder.encode(paymentZoneCode, forKey: "payment_zone_code")
        }
        if paymentZoneId != nil{
            aCoder.encode(paymentZoneId, forKey: "payment_zone_id")
        }
        if products != nil{
            aCoder.encode(products, forKey: "products")
        }
        if shippingAddress != nil{
            aCoder.encode(shippingAddress, forKey: "shipping_address")
        }
        if shippingAddress1 != nil{
            aCoder.encode(shippingAddress1, forKey: "shipping_address_1")
        }
        if shippingAddress2 != nil{
            aCoder.encode(shippingAddress2, forKey: "shipping_address_2")
        }
        if shippingAddressFormat != nil{
            aCoder.encode(shippingAddressFormat, forKey: "shipping_address_format")
        }
        if shippingCity != nil{
            aCoder.encode(shippingCity, forKey: "shipping_city")
        }
        if shippingCompany != nil{
            aCoder.encode(shippingCompany, forKey: "shipping_company")
        }
        if shippingCountry != nil{
            aCoder.encode(shippingCountry, forKey: "shipping_country")
        }
        if shippingCountryId != nil{
            aCoder.encode(shippingCountryId, forKey: "shipping_country_id")
        }
        if shippingFirstname != nil{
            aCoder.encode(shippingFirstname, forKey: "shipping_firstname")
        }
        if shippingIsoCode2 != nil{
            aCoder.encode(shippingIsoCode2, forKey: "shipping_iso_code_2")
        }
        if shippingIsoCode3 != nil{
            aCoder.encode(shippingIsoCode3, forKey: "shipping_iso_code_3")
        }
        if shippingLastname != nil{
            aCoder.encode(shippingLastname, forKey: "shipping_lastname")
        }
        if shippingMethod != nil{
            aCoder.encode(shippingMethod, forKey: "shipping_method")
        }
        if shippingPostcode != nil{
            aCoder.encode(shippingPostcode, forKey: "shipping_postcode")
        }
        if shippingZone != nil{
            aCoder.encode(shippingZone, forKey: "shipping_zone")
        }
        if shippingZoneCode != nil{
            aCoder.encode(shippingZoneCode, forKey: "shipping_zone_code")
        }
        if shippingZoneId != nil{
            aCoder.encode(shippingZoneId, forKey: "shipping_zone_id")
        }
        if storeId != nil{
            aCoder.encode(storeId, forKey: "store_id")
        }
        if storeName != nil{
            aCoder.encode(storeName, forKey: "store_name")
        }
        if storeUrl != nil{
            aCoder.encode(storeUrl, forKey: "store_url")
        }
        if telephone != nil{
            aCoder.encode(telephone, forKey: "telephone")
        }
        if timestamp != nil{
            aCoder.encode(timestamp, forKey: "timestamp")
        }
        if total != nil{
            aCoder.encode(total, forKey: "total")
        }
        if totals != nil{
            aCoder.encode(totals, forKey: "totals")
        }
        if vouchers != nil{
            aCoder.encode(vouchers, forKey: "vouchers")
        }
    }
}