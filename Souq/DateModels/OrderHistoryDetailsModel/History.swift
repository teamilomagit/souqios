//
//  History.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 11, 2019

import Foundation


class History : NSObject, NSCoding{

    var comment : String!
    var dateAdded : String!
    var status : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        comment = dictionary["comment"] as? String
        dateAdded = dictionary["date_added"] as? String
        status = dictionary["status"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if comment != nil{
            dictionary["comment"] = comment
        }
        if dateAdded != nil{
            dictionary["date_added"] = dateAdded
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        comment = aDecoder.decodeObject(forKey: "comment") as? String
        dateAdded = aDecoder.decodeObject(forKey: "date_added") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if comment != nil{
            aCoder.encode(comment, forKey: "comment")
        }
        if dateAdded != nil{
            aCoder.encode(dateAdded, forKey: "date_added")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
    }
}