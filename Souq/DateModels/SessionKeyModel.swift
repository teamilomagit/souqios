//
//  SessionKeyModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 24, 2019

import Foundation


class SessionKeyModel : NSObject, NSCoding{

    var session : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        session = dictionary["session"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if session != nil{
            dictionary["session"] = session
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        session = aDecoder.decodeObject(forKey: "session") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if session != nil{
            aCoder.encode(session, forKey: "session")
        }
    }
}