//
//  PaymentMethodModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 9, 2019

import Foundation


class PaymentMethodModel : NSObject, NSCoding{

    var cod : Cod!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let codData = dictionary["cod"] as? [String:Any]{
            cod = Cod(fromDictionary: codData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cod != nil{
            dictionary["cod"] = cod.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cod = aDecoder.decodeObject(forKey: "cod") as? Cod
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if cod != nil{
            aCoder.encode(cod, forKey: "cod")
        }
    }
}