//
//  Cod.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 9, 2019

import Foundation


class Cod : NSObject, NSCoding{

    var code : String!
    var sortOrder : Int!
    var terms : String!
    var title : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        code = dictionary["code"] as? String
        sortOrder = dictionary["sort_order"] as? Int
        terms = dictionary["terms"] as? String
        title = dictionary["title"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if code != nil{
            dictionary["code"] = code
        }
        if sortOrder != nil{
            dictionary["sort_order"] = sortOrder
        }
        if terms != nil{
            dictionary["terms"] = terms
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        code = aDecoder.decodeObject(forKey: "code") as? String
        sortOrder = aDecoder.decodeObject(forKey: "sort_order") as? Int
        terms = aDecoder.decodeObject(forKey: "terms") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if code != nil{
            aCoder.encode(code, forKey: "code")
        }
        if sortOrder != nil{
            aCoder.encode(sortOrder, forKey: "sort_order")
        }
        if terms != nil{
            aCoder.encode(terms, forKey: "terms")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
    }
}