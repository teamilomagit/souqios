//
//  QuantityModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 25, 2019

import Foundation


class QuantityModel : NSObject, NSCoding{

    var quantity : String!
    var isSelected : Bool = false



    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        quantity = dictionary["quantity"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if quantity != nil{
            dictionary["quantity"] = quantity
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        quantity = aDecoder.decodeObject(forKey: "quantity") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if quantity != nil{
            aCoder.encode(quantity, forKey: "quantity")
        }
    }
}
