//
//  Attribute.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 12, 2019

import Foundation


class Attribute : NSObject, NSCoding{

    var attributeId : String!
    var name : String!
    var text : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        attributeId = dictionary["attribute_id"] as? String
        name = dictionary["name"] as? String
        text = dictionary["text"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if attributeId != nil{
            dictionary["attribute_id"] = attributeId
        }
        if name != nil{
            dictionary["name"] = name
        }
        if text != nil{
            dictionary["text"] = text
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        attributeId = aDecoder.decodeObject(forKey: "attribute_id") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        text = aDecoder.decodeObject(forKey: "text") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if attributeId != nil{
            aCoder.encode(attributeId, forKey: "attribute_id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if text != nil{
            aCoder.encode(text, forKey: "text")
        }
    }
}