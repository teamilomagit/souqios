//
//  AttributeGroup.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 12, 2019

import Foundation


class AttributeGroup : NSObject, NSCoding{

    var attribute : [Attribute]!
    var attributeGroupId : String!
    var name : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        attributeGroupId = dictionary["attribute_group_id"] as? String
        name = dictionary["name"] as? String
        attribute = [Attribute]()
        if let attributeArray = dictionary["attribute"] as? [[String:Any]]{
            for dic in attributeArray{
                let value = Attribute(fromDictionary: dic)
                attribute.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if attributeGroupId != nil{
            dictionary["attribute_group_id"] = attributeGroupId
        }
        if name != nil{
            dictionary["name"] = name
        }
        if attribute != nil{
            var dictionaryElements = [[String:Any]]()
            for attributeElement in attribute {
                dictionaryElements.append(attributeElement.toDictionary())
            }
            dictionary["attribute"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        attribute = aDecoder.decodeObject(forKey: "attribute") as? [Attribute]
        attributeGroupId = aDecoder.decodeObject(forKey: "attribute_group_id") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if attribute != nil{
            aCoder.encode(attribute, forKey: "attribute")
        }
        if attributeGroupId != nil{
            aCoder.encode(attributeGroupId, forKey: "attribute_group_id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
    }
}