//
//  Reviews.swift
//  Souq
//
//  Created by Pawan Ramteke on 13/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import UIKit

class Reviews: NSObject {
    var author : String!
    var dateAdded : String!
    var rating : Int!
    var text : String!
    
    init(fromDictionary dictionary: [String:Any]){
        author = dictionary["author"] as? String
        dateAdded = dictionary["date_added"] as? String
        rating = dictionary["rating"] as? Int
        text = dictionary["text"] as? String
    }

}


