//
//  Option.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 12, 2019

import Foundation


class Option : NSObject, NSCoding{

    var name : String!
    var optionId : Int!
    var optionValue : [OptionValue]!
    var productOptionId : Int!
    var required : String!
    var type : String!
    var value : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        name = dictionary["name"] as? String
        optionId = dictionary["option_id"] as? Int
        productOptionId = dictionary["product_option_id"] as? Int
        required = dictionary["required"] as? String
        type = dictionary["type"] as? String
        value = dictionary["value"] as? String
        optionValue = [OptionValue]()
        if let optionValueArray = dictionary["option_value"] as? [[String:Any]]{
            for i in 0 ..< optionValueArray.count {
                let dic = optionValueArray[i]
                let value = OptionValue(fromDictionary: dic)
                if i == 0 {
                    value.isSelected = true
                }
                optionValue.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if name != nil{
            dictionary["name"] = name
        }
        if optionId != nil{
            dictionary["option_id"] = optionId
        }
        if productOptionId != nil{
            dictionary["product_option_id"] = productOptionId
        }
        if required != nil{
            dictionary["required"] = required
        }
        if type != nil{
            dictionary["type"] = type
        }
        if value != nil{
            dictionary["value"] = value
        }
        if optionValue != nil{
            var dictionaryElements = [[String:Any]]()
            for optionValueElement in optionValue {
                dictionaryElements.append(optionValueElement.toDictionary())
            }
            dictionary["optionValue"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        name = aDecoder.decodeObject(forKey: "name") as? String
        optionId = aDecoder.decodeObject(forKey: "option_id") as? Int
        optionValue = aDecoder.decodeObject(forKey: "option_value") as? [OptionValue]
        productOptionId = aDecoder.decodeObject(forKey: "product_option_id") as? Int
        required = aDecoder.decodeObject(forKey: "required") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        value = aDecoder.decodeObject(forKey: "value") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if optionId != nil{
            aCoder.encode(optionId, forKey: "option_id")
        }
        if optionValue != nil{
            aCoder.encode(optionValue, forKey: "option_value")
        }
        if productOptionId != nil{
            aCoder.encode(productOptionId, forKey: "product_option_id")
        }
        if required != nil{
            aCoder.encode(required, forKey: "required")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if value != nil{
            aCoder.encode(value, forKey: "value")
        }
    }
}
