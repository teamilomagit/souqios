//
//  ProductDetailsModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 12, 2019

import Foundation


class ProductDetailsModel : NSObject, NSCoding{

    var attributeGroups : [AttributeGroup]!
    var category : [Category]!
    var dateAdded : String!
    var dateAvailable : String!
    var dateModified : String!
    var descriptionField : String!
    var discounts : [AnyObject]!
    var ean : String!
    var height : String!
    var id : Int!
    var image : String!
    var images : [AnyObject]!
    var inWishlist : Bool!
    var isbn : String!
    var jan : String!
    var length : String!
    var lengthClass : String!
    var lengthClassId : Int!
    var location : String!
    var manufacturer : AnyObject!
    var manufacturerId : Int!
    var metaDescription : String!
    var metaKeyword : String!
    var metaTitle : String!
    var minimum : String!
    var model : String!
    var mpn : String!
    var name : String!
    var options : [Option]!
    var originalImage : String!
    var originalImages : [AnyObject]!
    var points : String!
    var price : Int!
    var priceExcludingTax : Int!
    var priceExcludingTaxFormated : String!
    var priceFormated : String!
    var productId : Int!
    var quantity : Int!
    var rating : Int!
    var recurrings : [AnyObject]!
    var reviews : Review!
    var reward : AnyObject!
    var seoUrl : String!
    var shipping : String!
    var sku : String!
    var sortOrder : String!
    var special : Int!
    var specialEndDate : String!
    var specialExcludingTax : Int!
    var specialExcludingTaxFormated : Int!
    var specialFormated : Int!
    var specialStartDate : String!
    var status : String!
    var stockStatus : String!
    var stockStatusId : Int!
    var subtract : String!
    var tag : String!
    var taxClassId : Int!
    var upc : String!
    var viewed : String!
    var weight : String!
    var weightClass : String!
    var weightClassId : Int!
    var width : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        dateAdded = dictionary["date_added"] as? String
        dateAvailable = dictionary["date_available"] as? String
        dateModified = dictionary["date_modified"] as? String
        descriptionField = dictionary["description"] as? String
        ean = dictionary["ean"] as? String
        height = dictionary["height"] as? String
        id = dictionary["id"] as? Int
        image = dictionary["image"] as? String
        inWishlist = dictionary["in_wishlist"] as? Bool
        isbn = dictionary["isbn"] as? String
        jan = dictionary["jan"] as? String
        length = dictionary["length"] as? String
        lengthClass = dictionary["length_class"] as? String
        lengthClassId = dictionary["length_class_id"] as? Int
        location = dictionary["location"] as? String
        manufacturer = dictionary["manufacturer"] as? AnyObject
        manufacturerId = dictionary["manufacturer_id"] as? Int
        metaDescription = dictionary["meta_description"] as? String
        metaKeyword = dictionary["meta_keyword"] as? String
        metaTitle = dictionary["meta_title"] as? String
        minimum = dictionary["minimum"] as? String
        model = dictionary["model"] as? String
        mpn = dictionary["mpn"] as? String
        name = dictionary["name"] as? String
        originalImage = dictionary["original_image"] as? String
        points = dictionary["points"] as? String
        price = dictionary["price"] as? Int
        priceExcludingTax = dictionary["price_excluding_tax"] as? Int
        priceExcludingTaxFormated = dictionary["price_excluding_tax_formated"] as? String
        priceFormated = dictionary["price_formated"] as? String
        productId = dictionary["product_id"] as? Int
        quantity = dictionary["quantity"] as? Int
        rating = dictionary["rating"] as? Int
        reward = dictionary["reward"] as? AnyObject
        seoUrl = dictionary["seo_url"] as? String
        shipping = dictionary["shipping"] as? String
        sku = dictionary["sku"] as? String
        sortOrder = dictionary["sort_order"] as? String
        special = dictionary["special"] as? Int
        specialEndDate = dictionary["special_end_date"] as? String
        specialExcludingTax = dictionary["special_excluding_tax"] as? Int
        specialExcludingTaxFormated = dictionary["special_excluding_tax_formated"] as? Int
        specialFormated = dictionary["special_formated"] as? Int
        specialStartDate = dictionary["special_start_date"] as? String
        status = dictionary["status"] as? String
        stockStatus = dictionary["stock_status"] as? String
        stockStatusId = dictionary["stock_status_id"] as? Int
        subtract = dictionary["subtract"] as? String
        tag = dictionary["tag"] as? String
        taxClassId = dictionary["tax_class_id"] as? Int
        upc = dictionary["upc"] as? String
        viewed = dictionary["viewed"] as? String
        weight = dictionary["weight"] as? String
        weightClass = dictionary["weight_class"] as? String
        weightClassId = dictionary["weight_class_id"] as? Int
        width = dictionary["width"] as? String
        if let reviewsData = dictionary["reviews"] as? [String:Any]{
            reviews = Review(fromDictionary: reviewsData)
        }
        attributeGroups = [AttributeGroup]()
        if let attributeGroupsArray = dictionary["attribute_groups"] as? [[String:Any]]{
            for dic in attributeGroupsArray{
                let value = AttributeGroup(fromDictionary: dic)
                attributeGroups.append(value)
            }
        }
        category = [Category]()
        if let categoryArray = dictionary["category"] as? [[String:Any]]{
            for dic in categoryArray{
                let value = Category(fromDictionary: dic)
                category.append(value)
            }
        }
        options = [Option]()
        if let optionsArray = dictionary["options"] as? [[String:Any]]{
            for dic in optionsArray{
                let value = Option(fromDictionary: dic)
                options.append(value)
            }
        }
        let arrImg = dictionary["images"] as! NSArray
        if arrImg.count > 0 {
            let arrImages = NSMutableArray()
            for strImg in arrImg{
                let model = BannerImagesModel(fromDictionary: ["image":strImg])
                arrImages.add(model)
            }
            images = arrImages as! [BannerImagesModel]
        }
        else{
            let arrImages = NSMutableArray()
            let model = BannerImagesModel(fromDictionary: ["image":self.originalImage])
            arrImages.add(model)
            images = arrImages as! [BannerImagesModel]
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dateAdded != nil{
            dictionary["date_added"] = dateAdded
        }
        if dateAvailable != nil{
            dictionary["date_available"] = dateAvailable
        }
        if dateModified != nil{
            dictionary["date_modified"] = dateModified
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if ean != nil{
            dictionary["ean"] = ean
        }
        if height != nil{
            dictionary["height"] = height
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if inWishlist != nil{
            dictionary["in_wishlist"] = inWishlist
        }
        if isbn != nil{
            dictionary["isbn"] = isbn
        }
        if jan != nil{
            dictionary["jan"] = jan
        }
        if length != nil{
            dictionary["length"] = length
        }
        if lengthClass != nil{
            dictionary["length_class"] = lengthClass
        }
        if lengthClassId != nil{
            dictionary["length_class_id"] = lengthClassId
        }
        if location != nil{
            dictionary["location"] = location
        }
        if manufacturer != nil{
            dictionary["manufacturer"] = manufacturer
        }
        if manufacturerId != nil{
            dictionary["manufacturer_id"] = manufacturerId
        }
        if metaDescription != nil{
            dictionary["meta_description"] = metaDescription
        }
        if metaKeyword != nil{
            dictionary["meta_keyword"] = metaKeyword
        }
        if metaTitle != nil{
            dictionary["meta_title"] = metaTitle
        }
        if minimum != nil{
            dictionary["minimum"] = minimum
        }
        if model != nil{
            dictionary["model"] = model
        }
        if mpn != nil{
            dictionary["mpn"] = mpn
        }
        if name != nil{
            dictionary["name"] = name
        }
        if originalImage != nil{
            dictionary["original_image"] = originalImage
        }
        if points != nil{
            dictionary["points"] = points
        }
        if price != nil{
            dictionary["price"] = price
        }
        if priceExcludingTax != nil{
            dictionary["price_excluding_tax"] = priceExcludingTax
        }
        if priceExcludingTaxFormated != nil{
            dictionary["price_excluding_tax_formated"] = priceExcludingTaxFormated
        }
        if priceFormated != nil{
            dictionary["price_formated"] = priceFormated
        }
        if productId != nil{
            dictionary["product_id"] = productId
        }
        if quantity != nil{
            dictionary["quantity"] = quantity
        }
        if rating != nil{
            dictionary["rating"] = rating
        }
        if reward != nil{
            dictionary["reward"] = reward
        }
        if seoUrl != nil{
            dictionary["seo_url"] = seoUrl
        }
        if shipping != nil{
            dictionary["shipping"] = shipping
        }
        if sku != nil{
            dictionary["sku"] = sku
        }
        if sortOrder != nil{
            dictionary["sort_order"] = sortOrder
        }
        if special != nil{
            dictionary["special"] = special
        }
        if specialEndDate != nil{
            dictionary["special_end_date"] = specialEndDate
        }
        if specialExcludingTax != nil{
            dictionary["special_excluding_tax"] = specialExcludingTax
        }
        if specialExcludingTaxFormated != nil{
            dictionary["special_excluding_tax_formated"] = specialExcludingTaxFormated
        }
        if specialFormated != nil{
            dictionary["special_formated"] = specialFormated
        }
        if specialStartDate != nil{
            dictionary["special_start_date"] = specialStartDate
        }
        if status != nil{
            dictionary["status"] = status
        }
        if stockStatus != nil{
            dictionary["stock_status"] = stockStatus
        }
        if stockStatusId != nil{
            dictionary["stock_status_id"] = stockStatusId
        }
        if subtract != nil{
            dictionary["subtract"] = subtract
        }
        if tag != nil{
            dictionary["tag"] = tag
        }
        if taxClassId != nil{
            dictionary["tax_class_id"] = taxClassId
        }
        if upc != nil{
            dictionary["upc"] = upc
        }
        if viewed != nil{
            dictionary["viewed"] = viewed
        }
        if weight != nil{
            dictionary["weight"] = weight
        }
        if weightClass != nil{
            dictionary["weight_class"] = weightClass
        }
        if weightClassId != nil{
            dictionary["weight_class_id"] = weightClassId
        }
        if width != nil{
            dictionary["width"] = width
        }
        if reviews != nil{
            dictionary["reviews"] = reviews.toDictionary()
        }
        if attributeGroups != nil{
            var dictionaryElements = [[String:Any]]()
            for attributeGroupsElement in attributeGroups {
                dictionaryElements.append(attributeGroupsElement.toDictionary())
            }
            dictionary["attributeGroups"] = dictionaryElements
        }
        if category != nil{
            var dictionaryElements = [[String:Any]]()
            for categoryElement in category {
                dictionaryElements.append(categoryElement.toDictionary())
            }
            dictionary["category"] = dictionaryElements
        }
        if options != nil{
            var dictionaryElements = [[String:Any]]()
            for optionsElement in options {
                dictionaryElements.append(optionsElement.toDictionary())
            }
            dictionary["options"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        attributeGroups = aDecoder.decodeObject(forKey: "attribute_groups") as? [AttributeGroup]
        category = aDecoder.decodeObject(forKey: "category") as? [Category]
        dateAdded = aDecoder.decodeObject(forKey: "date_added") as? String
        dateAvailable = aDecoder.decodeObject(forKey: "date_available") as? String
        dateModified = aDecoder.decodeObject(forKey: "date_modified") as? String
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        discounts = aDecoder.decodeObject(forKey: "discounts") as? [AnyObject]
        ean = aDecoder.decodeObject(forKey: "ean") as? String
        height = aDecoder.decodeObject(forKey: "height") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        images = aDecoder.decodeObject(forKey: "images") as? [AnyObject]
        inWishlist = aDecoder.decodeObject(forKey: "in_wishlist") as? Bool
        isbn = aDecoder.decodeObject(forKey: "isbn") as? String
        jan = aDecoder.decodeObject(forKey: "jan") as? String
        length = aDecoder.decodeObject(forKey: "length") as? String
        lengthClass = aDecoder.decodeObject(forKey: "length_class") as? String
        lengthClassId = aDecoder.decodeObject(forKey: "length_class_id") as? Int
        location = aDecoder.decodeObject(forKey: "location") as? String
        manufacturer = aDecoder.decodeObject(forKey: "manufacturer") as? AnyObject
        manufacturerId = aDecoder.decodeObject(forKey: "manufacturer_id") as? Int
        metaDescription = aDecoder.decodeObject(forKey: "meta_description") as? String
        metaKeyword = aDecoder.decodeObject(forKey: "meta_keyword") as? String
        metaTitle = aDecoder.decodeObject(forKey: "meta_title") as? String
        minimum = aDecoder.decodeObject(forKey: "minimum") as? String
        model = aDecoder.decodeObject(forKey: "model") as? String
        mpn = aDecoder.decodeObject(forKey: "mpn") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        options = aDecoder.decodeObject(forKey: "options") as? [Option]
        originalImage = aDecoder.decodeObject(forKey: "original_image") as? String
        originalImages = aDecoder.decodeObject(forKey: "original_images") as? [AnyObject]
        points = aDecoder.decodeObject(forKey: "points") as? String
        price = aDecoder.decodeObject(forKey: "price") as? Int
        priceExcludingTax = aDecoder.decodeObject(forKey: "price_excluding_tax") as? Int
        priceExcludingTaxFormated = aDecoder.decodeObject(forKey: "price_excluding_tax_formated") as? String
        priceFormated = aDecoder.decodeObject(forKey: "price_formated") as? String
        productId = aDecoder.decodeObject(forKey: "product_id") as? Int
        quantity = aDecoder.decodeObject(forKey: "quantity") as? Int
        rating = aDecoder.decodeObject(forKey: "rating") as? Int
        recurrings = aDecoder.decodeObject(forKey: "recurrings") as? [AnyObject]
        reviews = aDecoder.decodeObject(forKey: "reviews") as? Review
        reward = aDecoder.decodeObject(forKey: "reward") as? AnyObject
        seoUrl = aDecoder.decodeObject(forKey: "seo_url") as? String
        shipping = aDecoder.decodeObject(forKey: "shipping") as? String
        sku = aDecoder.decodeObject(forKey: "sku") as? String
        sortOrder = aDecoder.decodeObject(forKey: "sort_order") as? String
        special = aDecoder.decodeObject(forKey: "special") as? Int
        specialEndDate = aDecoder.decodeObject(forKey: "special_end_date") as? String
        specialExcludingTax = aDecoder.decodeObject(forKey: "special_excluding_tax") as? Int
        specialExcludingTaxFormated = aDecoder.decodeObject(forKey: "special_excluding_tax_formated") as? Int
        specialFormated = aDecoder.decodeObject(forKey: "special_formated") as? Int
        specialStartDate = aDecoder.decodeObject(forKey: "special_start_date") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        stockStatus = aDecoder.decodeObject(forKey: "stock_status") as? String
        stockStatusId = aDecoder.decodeObject(forKey: "stock_status_id") as? Int
        subtract = aDecoder.decodeObject(forKey: "subtract") as? String
        tag = aDecoder.decodeObject(forKey: "tag") as? String
        taxClassId = aDecoder.decodeObject(forKey: "tax_class_id") as? Int
        upc = aDecoder.decodeObject(forKey: "upc") as? String
        viewed = aDecoder.decodeObject(forKey: "viewed") as? String
        weight = aDecoder.decodeObject(forKey: "weight") as? String
        weightClass = aDecoder.decodeObject(forKey: "weight_class") as? String
        weightClassId = aDecoder.decodeObject(forKey: "weight_class_id") as? Int
        width = aDecoder.decodeObject(forKey: "width") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if attributeGroups != nil{
            aCoder.encode(attributeGroups, forKey: "attribute_groups")
        }
        if category != nil{
            aCoder.encode(category, forKey: "category")
        }
        if dateAdded != nil{
            aCoder.encode(dateAdded, forKey: "date_added")
        }
        if dateAvailable != nil{
            aCoder.encode(dateAvailable, forKey: "date_available")
        }
        if dateModified != nil{
            aCoder.encode(dateModified, forKey: "date_modified")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if discounts != nil{
            aCoder.encode(discounts, forKey: "discounts")
        }
        if ean != nil{
            aCoder.encode(ean, forKey: "ean")
        }
        if height != nil{
            aCoder.encode(height, forKey: "height")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if images != nil{
            aCoder.encode(images, forKey: "images")
        }
        if inWishlist != nil{
            aCoder.encode(inWishlist, forKey: "in_wishlist")
        }
        if isbn != nil{
            aCoder.encode(isbn, forKey: "isbn")
        }
        if jan != nil{
            aCoder.encode(jan, forKey: "jan")
        }
        if length != nil{
            aCoder.encode(length, forKey: "length")
        }
        if lengthClass != nil{
            aCoder.encode(lengthClass, forKey: "length_class")
        }
        if lengthClassId != nil{
            aCoder.encode(lengthClassId, forKey: "length_class_id")
        }
        if location != nil{
            aCoder.encode(location, forKey: "location")
        }
        if manufacturer != nil{
            aCoder.encode(manufacturer, forKey: "manufacturer")
        }
        if manufacturerId != nil{
            aCoder.encode(manufacturerId, forKey: "manufacturer_id")
        }
        if metaDescription != nil{
            aCoder.encode(metaDescription, forKey: "meta_description")
        }
        if metaKeyword != nil{
            aCoder.encode(metaKeyword, forKey: "meta_keyword")
        }
        if metaTitle != nil{
            aCoder.encode(metaTitle, forKey: "meta_title")
        }
        if minimum != nil{
            aCoder.encode(minimum, forKey: "minimum")
        }
        if model != nil{
            aCoder.encode(model, forKey: "model")
        }
        if mpn != nil{
            aCoder.encode(mpn, forKey: "mpn")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if options != nil{
            aCoder.encode(options, forKey: "options")
        }
        if originalImage != nil{
            aCoder.encode(originalImage, forKey: "original_image")
        }
        if originalImages != nil{
            aCoder.encode(originalImages, forKey: "original_images")
        }
        if points != nil{
            aCoder.encode(points, forKey: "points")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if priceExcludingTax != nil{
            aCoder.encode(priceExcludingTax, forKey: "price_excluding_tax")
        }
        if priceExcludingTaxFormated != nil{
            aCoder.encode(priceExcludingTaxFormated, forKey: "price_excluding_tax_formated")
        }
        if priceFormated != nil{
            aCoder.encode(priceFormated, forKey: "price_formated")
        }
        if productId != nil{
            aCoder.encode(productId, forKey: "product_id")
        }
        if quantity != nil{
            aCoder.encode(quantity, forKey: "quantity")
        }
        if rating != nil{
            aCoder.encode(rating, forKey: "rating")
        }
        if recurrings != nil{
            aCoder.encode(recurrings, forKey: "recurrings")
        }
        if reviews != nil{
            aCoder.encode(reviews, forKey: "reviews")
        }
        if reward != nil{
            aCoder.encode(reward, forKey: "reward")
        }
        if seoUrl != nil{
            aCoder.encode(seoUrl, forKey: "seo_url")
        }
        if shipping != nil{
            aCoder.encode(shipping, forKey: "shipping")
        }
        if sku != nil{
            aCoder.encode(sku, forKey: "sku")
        }
        if sortOrder != nil{
            aCoder.encode(sortOrder, forKey: "sort_order")
        }
        if special != nil{
            aCoder.encode(special, forKey: "special")
        }
        if specialEndDate != nil{
            aCoder.encode(specialEndDate, forKey: "special_end_date")
        }
        if specialExcludingTax != nil{
            aCoder.encode(specialExcludingTax, forKey: "special_excluding_tax")
        }
        if specialExcludingTaxFormated != nil{
            aCoder.encode(specialExcludingTaxFormated, forKey: "special_excluding_tax_formated")
        }
        if specialFormated != nil{
            aCoder.encode(specialFormated, forKey: "special_formated")
        }
        if specialStartDate != nil{
            aCoder.encode(specialStartDate, forKey: "special_start_date")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if stockStatus != nil{
            aCoder.encode(stockStatus, forKey: "stock_status")
        }
        if stockStatusId != nil{
            aCoder.encode(stockStatusId, forKey: "stock_status_id")
        }
        if subtract != nil{
            aCoder.encode(subtract, forKey: "subtract")
        }
        if tag != nil{
            aCoder.encode(tag, forKey: "tag")
        }
        if taxClassId != nil{
            aCoder.encode(taxClassId, forKey: "tax_class_id")
        }
        if upc != nil{
            aCoder.encode(upc, forKey: "upc")
        }
        if viewed != nil{
            aCoder.encode(viewed, forKey: "viewed")
        }
        if weight != nil{
            aCoder.encode(weight, forKey: "weight")
        }
        if weightClass != nil{
            aCoder.encode(weightClass, forKey: "weight_class")
        }
        if weightClassId != nil{
            aCoder.encode(weightClassId, forKey: "weight_class_id")
        }
        if width != nil{
            aCoder.encode(width, forKey: "width")
        }
    }
}
