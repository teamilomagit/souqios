//
//  Review.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 12, 2019

import Foundation


class Review : NSObject, NSCoding{

    var reviewTotal : String!
    var reviews : [Reviews]!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        reviewTotal = dictionary["review_total"] as? String
        
        reviews = [Reviews]()
        if let categoryArray = dictionary["reviews"] as? [[String:Any]]{
            for dic in categoryArray{
                let value = Reviews(fromDictionary: dic)
                reviews.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if reviewTotal != nil{
            dictionary["review_total"] = reviewTotal
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        reviewTotal = aDecoder.decodeObject(forKey: "review_total") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if reviewTotal != nil{
            aCoder.encode(reviewTotal, forKey: "review_total")
        }
    }
}
