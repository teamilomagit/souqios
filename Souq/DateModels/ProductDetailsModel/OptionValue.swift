//
//  OptionValue.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 12, 2019

import Foundation


class OptionValue : NSObject, NSCoding{

    var image : AnyObject!
    var name : String!
    var optionValueId : Int!
    var price : Int!
    var priceExcludingTax : Int!
    var priceExcludingTaxFormated : String!
    var priceFormated : String!
    var pricePrefix : String!
    var productOptionValueId : Int!
    var quantity : Int!
    var isSelected : Bool = false

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        image = dictionary["image"] as? AnyObject
        name = dictionary["name"] as? String
        optionValueId = dictionary["option_value_id"] as? Int
        price = dictionary["price"] as? Int
        priceExcludingTax = dictionary["price_excluding_tax"] as? Int
        priceExcludingTaxFormated = dictionary["price_excluding_tax_formated"] as? String
        priceFormated = dictionary["price_formated"] as? String
        pricePrefix = dictionary["price_prefix"] as? String
        productOptionValueId = dictionary["product_option_value_id"] as? Int
        quantity = dictionary["quantity"] as? Int
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if image != nil{
            dictionary["image"] = image
        }
        if name != nil{
            dictionary["name"] = name
        }
        if optionValueId != nil{
            dictionary["option_value_id"] = optionValueId
        }
        if price != nil{
            dictionary["price"] = price
        }
        if priceExcludingTax != nil{
            dictionary["price_excluding_tax"] = priceExcludingTax
        }
        if priceExcludingTaxFormated != nil{
            dictionary["price_excluding_tax_formated"] = priceExcludingTaxFormated
        }
        if priceFormated != nil{
            dictionary["price_formated"] = priceFormated
        }
        if pricePrefix != nil{
            dictionary["price_prefix"] = pricePrefix
        }
        if productOptionValueId != nil{
            dictionary["product_option_value_id"] = productOptionValueId
        }
        if quantity != nil{
            dictionary["quantity"] = quantity
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        image = aDecoder.decodeObject(forKey: "image") as? AnyObject
        name = aDecoder.decodeObject(forKey: "name") as? String
        optionValueId = aDecoder.decodeObject(forKey: "option_value_id") as? Int
        price = aDecoder.decodeObject(forKey: "price") as? Int
        priceExcludingTax = aDecoder.decodeObject(forKey: "price_excluding_tax") as? Int
        priceExcludingTaxFormated = aDecoder.decodeObject(forKey: "price_excluding_tax_formated") as? String
        priceFormated = aDecoder.decodeObject(forKey: "price_formated") as? String
        pricePrefix = aDecoder.decodeObject(forKey: "price_prefix") as? String
        productOptionValueId = aDecoder.decodeObject(forKey: "product_option_value_id") as? Int
        quantity = aDecoder.decodeObject(forKey: "quantity") as? Int
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if optionValueId != nil{
            aCoder.encode(optionValueId, forKey: "option_value_id")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if priceExcludingTax != nil{
            aCoder.encode(priceExcludingTax, forKey: "price_excluding_tax")
        }
        if priceExcludingTaxFormated != nil{
            aCoder.encode(priceExcludingTaxFormated, forKey: "price_excluding_tax_formated")
        }
        if priceFormated != nil{
            aCoder.encode(priceFormated, forKey: "price_formated")
        }
        if pricePrefix != nil{
            aCoder.encode(pricePrefix, forKey: "price_prefix")
        }
        if productOptionValueId != nil{
            aCoder.encode(productOptionValueId, forKey: "product_option_value_id")
        }
        if quantity != nil{
            aCoder.encode(quantity, forKey: "quantity")
        }
    }
}
