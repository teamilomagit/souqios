//
//  ReviewsModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 4, 2019

import Foundation


class ReviewsModel : NSObject, NSCoding{

    var reviewTotal : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        reviewTotal = dictionary["review_total"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if reviewTotal != nil{
            dictionary["review_total"] = reviewTotal
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        reviewTotal = aDecoder.decodeObject(forKey: "review_total") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if reviewTotal != nil{
            aCoder.encode(reviewTotal, forKey: "review_total")
        }
    }
}