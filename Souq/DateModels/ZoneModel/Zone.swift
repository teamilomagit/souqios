//
//  Zone.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 26, 2019

import Foundation


class Zone : NSObject, NSCoding{

    var code : String!
    var countryId : String!
    var name : String!
    var status : String!
    var zoneId : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        code = dictionary["code"] as? String
        countryId = dictionary["country_id"] as? String
        name = dictionary["name"] as? String
        status = dictionary["status"] as? String
        zoneId = dictionary["zone_id"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if code != nil{
            dictionary["code"] = code
        }
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if name != nil{
            dictionary["name"] = name
        }
        if status != nil{
            dictionary["status"] = status
        }
        if zoneId != nil{
            dictionary["zone_id"] = zoneId
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        code = aDecoder.decodeObject(forKey: "code") as? String
        countryId = aDecoder.decodeObject(forKey: "country_id") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        zoneId = aDecoder.decodeObject(forKey: "zone_id") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if code != nil{
            aCoder.encode(code, forKey: "code")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if zoneId != nil{
            aCoder.encode(zoneId, forKey: "zone_id")
        }
    }
}