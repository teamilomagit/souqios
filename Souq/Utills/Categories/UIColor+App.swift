//
//  UIColor+App.swift
//  Souq
//
//  Created by Pawan Ramteke on 04/02/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    public class var appThemeColor: UIColor
    {
        return UIColor(red:0.16, green:0.29, blue:0.80, alpha:1.0)
    }
    public class var controllerBGColor : UIColor
    {
        return UIColor(red: 0.94, green: 0.94, blue: 0.94, alpha: 1.0)
    }
    
    public class var statusGreenColor : UIColor
    {
        return UIColor(red: 0.094, green: 0.82, blue: 0.45, alpha: 1.0)
    }
   
    public class var ratingGoldenColor : UIColor
    {
        return UIColor(red: 1, green: 0.84, blue: 0, alpha: 1.0)
    }
    
}
