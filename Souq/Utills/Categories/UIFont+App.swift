//
//  UIFont+App.swift
//  Odito
//
//  Created by Pawan Ramteke on 28/01/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit
extension UIFont
{
    class func appRegularFont(size:CGFloat)->UIFont
    {
        return UIFont(name: "Raleway", size: size)!
    }

    class func appBoldFont(size:CGFloat)->UIFont
    {
        return UIFont(name: "Raleway-Bold", size: size)!
    }
    
    class func appMediumFont(size:CGFloat)->UIFont
    {
        return UIFont(name: "Raleway-SemiBold", size: size)!
    }
}
