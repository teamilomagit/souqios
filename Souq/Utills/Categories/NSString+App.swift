//
//  NSString+App.swift
//  Odito
//
//  Created by Pawan Ramteke on 31/01/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit
extension String
{
    func trimString()->String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
