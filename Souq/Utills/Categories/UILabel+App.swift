//
//  UILabel+App.swift
//  Souq
//
//  Created by Pawan Ramteke on 24/03/19.
//  Copyright © 2019 iLoma Technology. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    @IBInspectable var localizeText: String? {
        get {
            return self.text
        }
        set {
            self.text = localize(string: localizeText!)
        }
    }
}

extension UIButton {
    @IBInspectable var localizeText: String? {
        get {
            return self.titleLabel?.text
        }
        set {
            self.setTitle(localize(string: localizeText!), for: .normal)
        }
    }
}

extension UITextField {
    @IBInspectable var localizePlaceholder: String? {
        get {
            return self.placeholder
        }
        set {
            self.placeholder = localize(string: localizePlaceholder!)
        }
    }
}

extension UITextView {
    @IBInspectable var localizePlaceholder: String? {
        get {
            return self.placeholder
        }
        set {
            self.self.placeholder = localize(string: localizePlaceholder!)
        }
    }
}
