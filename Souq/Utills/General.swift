//
//  Genaral.swift
//  FitnessMaa
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import UIKit
let SCREEN_WIDTH    =   UIScreen.main.bounds.size.width
let SCREEN_HEIGHT   =   UIScreen.main.bounds.size.height

let VIEWMANAGER = ViewManager.shared

let MAYUR_STORYBOARD = UIStoryboard(name: "Main", bundle: nil)
let PAWAN_STORYBOARD = UIStoryboard(name: "Pawan", bundle: nil)
let IS_IPHONE_5S     =  SCREEN_HEIGHT == 568.0

let app_del = UIApplication.shared.delegate as! AppDelegate

func localize(string:String) -> String {
    let path = Bundle.main.path(forResource: Preferences.getAppLanguage(), ofType: "lproj")
    let bundle = Bundle(path: path!)
    let localizedStr = NSLocalizedString(string, tableName: nil, bundle: bundle!, value: "", comment: "")
    return localizedStr
}


let ENGLISH =   "en"
let ARABIC  =   "ar"

let GOOGLE_CLIENT_ID = "36996357790-3rddkoj68kcmlbo4otbepfrqu4v8abmu.apps.googleusercontent.com"



let APP_NAME    =   Bundle.main.infoDictionary!["CFBundleName"] as! String


let MERCHANT_ID         =   "Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM"

//Test
//let BASE_URL                     =       "https://souq366.com/api/rest/"
let BASE_URL                       =       "http://dev.ravikatre.in/api/rest/"

let GET_SESSION_API                =       "session"


//Dreawer
let DRAWER_SELECTION_NOTIFICATION  =      "DRAWER_SELECTION_NOTIFICATION"

//Login URL
let LOGIN_URL                      =       "login"

let SOCIAL_LOGIN_URL               =       "sociallogin"

let GUEST_LOGIN_URL                =        "guest"
let GUEST_SHIPPING_URL             =        "guestshipping"
//Logout
let LOGOUT_URL                     =       "logout"

//Register
let REGISTRATION_API               =       "register2"

//Banner
let GET_BANNER_API                 =       "banners"
let GET_BANNER_IMAGES_API          =       "banners/"

//Categories
let PRODUCT_CATEGORIES_API         =       "categories"

//Forgot Password
let FORGOT_PASSWORD_API            =       "forgotten"

let UPDATE_ACCOUNT                 =        "account/account"

//Product Listing
let GET_PRODUCT_LISTING_API        =        "products"

let FILTER_PRODUCT_LISTING_API     =        "index.php?route=feed/rest_api/products"

let SIMILAR_PRODUCT_LISTING_API    =        "index.php?route=feed/rest_api/related"

let POST_YOUR_QUESTION_API         =        "contact2"
//Deals of the day
let DEALS_OF_THE_DAY_API           =        "latest"

//Product details
let GET_PRODUCT_DETAILS_API        =        "products_details"

//Account Adddress
let GET_ACCOUNT_ADDRESS_LIST       =        "account/address"

//Add Account Adddress
let ADD_ACCOUNT_ADDRESS            =    "account/address_add"

//Add to Cart
let ADD_PRODUCT_TO_CART            = "cart"

//View Cart
let GET_CART_PRODUCTS              = "cart_details"

let APPLY_COUPON_API                =   "coupon"

//Delete item from cart
let REMOVE_ITEM_FROM_CART             = "cart_remove"

//Get Countries
let GET_COUNTRIES                     = "countries"

//Get Zone
let GET_ZONE                     = "countries/"

let SEARCH_PRODUCT_API            =  "products/search/"

let CHANGE_PASSWORD_API         =   "/account/password"

let MY_WISHLIST_API             =   "wishlist"

let SET_PAYMENT_ADDRESS_API     =   "paymentaddress/existing"

let SET_SHIPPING_ADDRESS_API    =   "shippingaddress/existing"

let GET_SHIPPING_METHODS_API    =   "shippingmethods"

let GET_PAYMENT_METHODS_API     =   "paymentmethods"

let PLACE_ORDER_API             =   "confirm"

let ORDER_HISTORY_API           =   "customerorders/"

let ORDER_HISTORY_DETAILS_API           =   "customerorders/_details"

let GET_ZONE_SHIPPING_API       =   "zone_shipping"

let GET_INFORMATION_TYPES_API       =   "information"

let FACEBOOK_LINK       =      "https://www.facebook.com/souq366/"

let INSTA_LINK      =       "https://www.instagram.com/souq366/"

let YOUTUBE_LINK        =       "https://www.youtube.com/channel/UCqkHGugdzP2H4Dm23Eb_1tw"

let SNAPCHAT_LINK   =   "https://www.snapchat.com/add/souq366.com"


let CONTACT_NO       =   "91773695087"

