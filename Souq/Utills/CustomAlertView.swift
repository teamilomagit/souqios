//
//  CustomAlertView.swift
//  BTS
//
//  Created by Pawan Ramteke on 31/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit
class CustomAlertView
{
    class func showAlert(withTitle:String,messsage:String, completion:(()->Void)?)
    {
        let alertVC = UIAlertController(title: withTitle, message: messsage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: localize(string:"OK"), style: .default) { (action) in
            
            if completion != nil {
                completion!()
            }
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(okAction)
        
        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)
        
    }
    
    class func showAlertWithYesNo(title:String,messsage:String, completion:(()->Void)?)
    {
        let alertVC = UIAlertController(title: title, message: messsage, preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title:localize(string: "Yes"), style: .default) { (action) in
            
            if completion != nil {
                completion!()
            }
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
        }
        
        let noAction = UIAlertAction(title: localize(string:"No"), style: .default) { (action) in
            
            alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(yesAction)
        alertVC.addAction(noAction)

        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)
    }
    
    class func showPermissionEnableAlert(title:String,message:String, completion:(()->Void)?)
    {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title:localize(string: "Settings"), style: .default) { (action) in
            
            if completion != nil {
                completion!()
            }
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
        }
        
        let noAction = UIAlertAction(title:localize(string: "Not Now"), style: .default) { (action) in
            
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(noAction)
        alertVC.addAction(yesAction)
        
        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)

    }
}
