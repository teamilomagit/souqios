//
//  ViewManager.swift
//  BTS
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
class ViewManager : NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    static let shared = ViewManager()
    var currentUser: LoginModel?
    var isObserverd : Bool! = false
    var Onboarding : Bool! = true
    var imagePickerBlock : ((UIImage,String)->())?
    var arrBannersData : NSArray!
    var arrCategoriesData = NSArray()
    var loader : UIActivityIndicatorView!
    private override init(){
    }
    
    func topMostController() -> UIViewController
    {
        var topViewController = UIApplication.shared.keyWindow?.rootViewController
        while true {
            if topViewController?.presentedViewController != nil {
                topViewController = topViewController?.presentedViewController
            }
            else if (topViewController is KYDrawerController) {
                let kyDrawer = topViewController as? KYDrawerController
                if (kyDrawer?.mainViewController is UINavigationController)
                {
                    let vc = kyDrawer?.mainViewController as! UINavigationController
                    return vc.topViewController!
                }
                return (kyDrawer?.mainViewController)!
            }
            else if (topViewController is UINavigationController) {
                let nav = topViewController as? UINavigationController
                if (nav?.topViewController is KYDrawerController) {
                    let kyDrawer = nav?.topViewController as? KYDrawerController
                    return (kyDrawer?.mainViewController)!
                }
                topViewController = nav?.topViewController
            }
            
            else if (topViewController is UINavigationController) {
                let nav = topViewController as? UINavigationController
                
                topViewController = nav?.topViewController
            }
            else if (topViewController is UITabBarController) {
                let tab = topViewController as? UITabBarController
                topViewController = tab?.selectedViewController
                break
            }
            else {
                break
            }
        }
        return topViewController!
    }
    
    func showActionSheet(_ actionSheetData:NSArray,completion:@escaping (String) -> Void)
    {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        for i in actionSheetData {
            let alertAction = UIAlertAction(title: i as? String, style: .default, handler: { (action) in
                completion(action.title!)
            })
            alert.addAction(alertAction)
            
        }
        self.topMostController().present(alert, animated: true, completion: nil)
    }
    
    func showImagePicker(sourceType : UIImagePickerControllerSourceType,block : @escaping (UIImage,String) -> Void)
    {
        self.imagePickerBlock = block
        let imgPicker =  UIImagePickerController()
        imgPicker.sourceType = sourceType
        imgPicker.delegate = self
        self.topMostController().present(imgPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        do{
            let paths = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask)
            let documentsDirectory : URL = paths[0]
            let localPath = documentsDirectory.appendingPathComponent("Profile_pic.png")
            
            let data = UIImageJPEGRepresentation(image, 0.5)
            
            
            try data?.write(to: localPath)
            
            if self.imagePickerBlock != nil {
                self.imagePickerBlock!(image,localPath.path)
            }
        }
        catch{
            
        }
        self.topMostController().dismiss(animated: true, completion: nil)
    }
    
    func showActivityIndicator(_ controller:UIViewController = VIEWMANAGER.topMostController(),hardLoader:Bool = false)
    {
        if !hardLoader {
            loader = UIActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.midX - 10, y: UIScreen.main.bounds.midY - 10, width: 20, height: 20))
            loader.activityIndicatorViewStyle = .gray
            loader.color = UIColor.appThemeColor
            controller.view.addSubview(loader)
            loader.startAnimating()
        }
        else {
             SVProgressHUD.setDefaultMaskType(.gradient)
             SVProgressHUD.setForegroundColor(UIColor.appThemeColor)
             SVProgressHUD.setRingThickness(3)
             SVProgressHUD.setBackgroundColor(UIColor.clear)
             SVProgressHUD.show()
        }
    }
    
    func hideActivityIndicator()
    {
        if loader != nil {
            loader.stopAnimating()
            loader.removeFromSuperview()
        }
        SVProgressHUD.dismiss()
    }
    
    func showToast(_ message:String?) {
        UIApplication.shared.keyWindow?.makeToast(message)
    }
    
    func getProperImageUrl(strUrl:String?) -> String?
    {
        if strUrl == nil {
            return nil
        }
        return strUrl!.replacingOccurrences(of: " ", with: "%20")
    }
}
